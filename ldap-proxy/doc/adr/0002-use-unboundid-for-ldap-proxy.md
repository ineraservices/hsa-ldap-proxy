# 2. use unboundid for ldap proxy

Date: 2018-05-16

## Status

Accepted

## Context

We need a LDAP SDK for our LDAP proxy

## Decision

Use UnboundID since it supports all our LDAP requirements, easy of use, and exceptional in-house expertise

## Consequences

Lock-in, alternaitve would be Apache DS which has a similar but not the same SDK
