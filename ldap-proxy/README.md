## Known errors
target test will fail if you have old gradle version. update gradle to at least v. 4.6

## Start environment

Start/stop/restart the environment with docker-compose
```
$ docker-compose up -d
$ docker-compose stop
$ docker-compose restart
```

## Build
```
$ gradle build
```

## Deploy war-file to a running instance 

```
$ gradle deploy
```

## Cucumber integration tests

Requires that the docker compose suites for Org API and LDAP Proxy are running locally.
Results will be written to _reports/cucumber.xml_

```
$ gradle cucumber
```

Scenarios annotated with the @WIP-tag (Work In Progress) will not be run.

## Check that the proxy is up
```
$ docker exec -it hsa-ldap-proxy bash
$ curl http://127.0.0.1:9080/health
{"checks":[{"data":{},"name":"proxy","state":"UP"}],"outcome":"UP"}
``` 

## Run ldap commands
```
$ shelldap, openldap examples?
```

## view logs
```
$ docker logs -f hsa-ldap-proxy
```

# link to hsa information
https://www.inera.se/kundservice/dokument-och-lankar/tjanster/hsa/
