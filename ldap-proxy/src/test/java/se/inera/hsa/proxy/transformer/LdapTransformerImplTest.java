package se.inera.hsa.proxy.transformer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;

import se.inera.hsa.proxy.AttributeNames;
import se.inera.hsa.proxy.BaseTest;
import se.inera.hsa.proxy.ObjectClasses;

class LdapTransformerImplTest extends BaseTest {

    private LdapTransformerImpl ldapTransformer;

    @BeforeEach
    void setUp() {
//        MockitoAnnotations.initMocks(this);
        ldapTransformer = new LdapTransformerImpl();
        InputStream hsaSchemaStream= getClass().getClassLoader().getResourceAsStream("hsa-schema.ldif");
        try {
             ldapTransformer.setSchema(Schema.getSchema(hsaSchemaStream));
        } catch (LDIFException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testUnitEntryToJson() throws IOException, LDIFException, URISyntaxException {

        Entry unitEntry = buildEntry("unit_entry.ldif");

        JsonObject unit = stringToJsonObject(ldapTransformer.transformToJson(unitEntry.getDN(),
                unitEntry.getAttributes()));

        assertEquals("23", unit.getString("municipalityCode"));
        assertEquals("Unit", unit.getString("type"));

    }
    
    @Test
    void testUnitHsaHealthCareProviderEntryToJson() throws IOException, LDIFException, URISyntaxException {
        
        Entry unitEntry = buildEntry("unit_hsaHealthCareProvider.ldif");
        
        JsonObject unit = stringToJsonObject(ldapTransformer.transformToJson(unitEntry.getDN(),
                unitEntry.getAttributes()));
        
        assertEquals(JsonValue.TRUE, unit.get(ObjectClasses.hsaHealthCareProvider));
    }
    
    @Test
    void testUnitHsaHealthCareunitEntryToJson() throws IOException, LDIFException, URISyntaxException {
        
        Entry unitEntry = buildEntry("unit_hsaHealthCareUnit.ldif");
        
        JsonObject unit = stringToJsonObject(ldapTransformer.transformToJson(unitEntry.getDN(),
                unitEntry.getAttributes()));
        
        assertEquals(JsonValue.TRUE, unit.get(ObjectClasses.hsaHealthCareUnit));
    }
    
    @Test
    void testOrganizationHsaHealthCareProviderEntryToJson() throws IOException, LDIFException, URISyntaxException {
        
        Entry unitEntry = buildEntry("organization_hsaHealthCareProvider.ldif");
        
        JsonObject unit = stringToJsonObject(ldapTransformer.transformToJson(unitEntry.getDN(),
                unitEntry.getAttributes()));
        
        assertEquals(JsonValue.TRUE, unit.get(ObjectClasses.hsaHealthCareProvider));
    }
    
    @Test
    void testOrganizationHsaHealthCareunitEntryToJson() throws IOException, LDIFException, URISyntaxException {
        
        Entry unitEntry = buildEntry("organization_hsaHealthCareUnit.ldif");
        
        JsonObject unit = stringToJsonObject(ldapTransformer.transformToJson(unitEntry.getDN(),
                unitEntry.getAttributes()));
        
        assertEquals(JsonValue.TRUE, unit.get(ObjectClasses.hsaHealthCareUnit));
    }

    @Test
    void testOrganizationEntryToJson() throws IOException, LDIFException, URISyntaxException {

        Entry organizationEntry = buildEntry("organization_entry.ldif");

        JsonObject organization = stringToJsonObject(ldapTransformer.transformToJson(organizationEntry.getDN(),
                organizationEntry.getAttributes()));

        assertEquals("\"TEST123456789-0001\"", organization.get("hsaIdentity").toString());

    }
    
    @Test
    void testMultipleNamingAttrs() throws IOException, LDIFException, URISyntaxException {
        
        Entry unit = new Entry("ou=test1,o=testo,c=SE");
        unit.addAttribute(AttributeNames.ou, "test2", "test1");
        
        
        JsonObject organization = stringToJsonObject(ldapTransformer.transformToJson(unit.getDN(),
                unit.getAttributes()));
        
        assertEquals("test1", organization.getString("name"));
        
    }

    @Test
    void testTimeSpanToJson() {
        assertEquals("{\"timeSpan\":{\"fromDay\":\"1\",\"fromTime2\":\"10:00\",\"toTime2\":" +
                "\"10:30\",\"toDay\":\"1\",\"comment\":\"Ännu mindre öppet\"}}",
                ldapTransformer.timeSpanToJson("1-1#10:00#10:30#Ännu mindre öppet").toString());
        
        assertEquals("{\"timeSpan\":{\"fromDay\":\"5\",\"fromTime2\":\"21:00\",\"toTime2\":" +
                        "\"24:00\",\"toDay\":\"5\"}}",
                ldapTransformer.timeSpanToJson("5-5#21:00#24:00").toString());
        assertEquals("{\"timeSpan\":{\"fromDay\":\"3\",\"fromTime2\":\"08:00\",\"toTime2\":" +
                        "\"22:00\",\"toDay\":\"4\",\"comment\":\"Mellandagsöppet\",\"fromDate\":" +
                        "\"20171227\",\"toDate\":\"20171230\"}}",
                ldapTransformer.timeSpanToJson("3-4#08:00#22:00#Mellandagsöppet#20171227#20171230")
                        .toString());
        assertEquals("{\"timeSpan\":{\"fromDay\":\"3\",\"fromTime2\":\"08:00\",\"toTime2\":" +
                        "\"22:00\",\"toDay\":\"4\",\"comment\":\" , & ; -\",\"fromDate\":" +
                        "\"20171227\",\"toDate\":\"20171230\"}}",
                ldapTransformer.timeSpanToJson("3-4#08:00#22:00# , & ; -#20171227#20171230")
                        .toString());

    }

    @Test
    void testIsTimeSpan() {
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00"));
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00###"));
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00##20100101#20990101"));
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00##20100101#"));
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00#Comment#20100101#20990101"));
        assertTrue(ldapTransformer.isTimeSpan("1-5#08:00#10:00# , & ; -#20100101#20990101"));
        assertTrue(ldapTransformer.isTimeSpan("1-1#10:00#10:30#Ännu mindre öppet"));
        assertFalse(ldapTransformer.isTimeSpan("1-5#08-00#10:00"));
        assertFalse(ldapTransformer.isTimeSpan("#08:00#10:00"));
    }

    @Test
    void testTransformRT90CoordinatesToJson() {
        assertEquals("{\"coordinate\":{\"type\":\"rt90\",\"x\":\"6598840\",\"y\":\"1641424\"}}",
                ldapTransformer.transformRT90CoordinatesToJson("X:6598840,Y:1641424").toString());
    }

    @Test
    void testIsRT90Coordinates() {
        assertTrue(ldapTransformer.isRT90Coordinates("X:6598840,Y:1641424"));
    }

    @Test
    void testTransformSWEREF99CoordinatesToJson() {
        assertEquals("{\"coordinate\":{\"type\":\"sweref\",\"N\":\"6477155\",\"E\":\"536352\"}}",
                ldapTransformer.transformSweRefCoordinatesToJson("N:6477155,E:536352").toString());
    }

    @Test
    void testIsSWEREF99Coordinates() {
        assertTrue(ldapTransformer.isSweRefCoordinates("N:6477155,E:536352"));
    }


    @Test
    void testAddressLineToJson() {
        assertEquals("{\"addressLine\":[\"Box 64\",\"782 22 Malung\",\"Vårdcentral Malung\"]}",
                ldapTransformer.transformAddressLineToJson("Box 64$782 22 Malung$Vårdcentral Malung")
                        .toString());
    }

    @Test
    void testIsBinary() {
        assertTrue(ldapTransformer.isBinary(AttributeNames.hsaJpegLogotype + ";binary"));
    }

    @Test
    void testFilterObjectClasses() {
        String rdn = "ou=Sa Sjukh,o=Stockholms läns landsting,l=Stockholms län,c=SE"
                .substring(0, "ou=Sa Sjukh,o=Stockholms läns landsting,l=Stockholms län,c=SE".indexOf("="));
        assertEquals("Unit",
                ldapTransformer.filterObjectClasses(new String[]{"top", "organizationalUnit", "hiddenObject",
                        "hsaFeignedDataObject"}, rdn)[0]);
        assertNotEquals("Unit",
                ldapTransformer.filterObjectClasses(new String[]{"top", "organizationalUnit", "hsaPersonExtension",
                        "hsaOrganizationExtension"}, rdn)[0]);
        
        
        String personRdn = "cn";
        assertEquals("Employee",
                ldapTransformer.filterObjectClasses(new String[]{"hsapersonextension", "inetorgperson", "organizationalperson", "person", "pkiuser", "top"}, personRdn )[0]);
    }
    
    @Test
    void testTransformToUpdateUnit() throws LDIFException, IOException, URISyntaxException {
        Entry unitEntry = buildEntry("unit_entry.ldif");

        String transformToUpdateJson = ldapTransformer.transformToUpdateJson(unitEntry.getDN(),
                unitEntry.getAttributes());
        JsonObject update = stringToJsonObject(transformToUpdateJson);

        assertNull(update.getJsonObject("newPath"), "newPath should be null");
        assertEquals("MERGE", update.getString("modificationType"));
        assertEquals("Unit", update.getJsonObject("unit").getString("type"));
    }
    
    @Test
    void testTransformToUpdateUnitHsaHealthCareProvider() throws LDIFException, IOException, URISyntaxException {
        Entry unitEntry = buildEntry("unit_entry.ldif");
        unitEntry.addAttribute(AttributeNames.objectClass, ObjectClasses.hsaHealthCareUnit);
        String transformToUpdateJson = ldapTransformer.transformToUpdateJson(unitEntry.getDN(),
                unitEntry.getAttributes());
        JsonObject update = stringToJsonObject(transformToUpdateJson);

        assertNull(update.getJsonObject("newPath"), "newPath should be null");
        assertEquals("MERGE", update.getString("modificationType"));
        assertTrue(update.getJsonObject("unit").getBoolean("hsaHealthCareUnit"), "Should have hsaHealthCareUnit=true");
    }
    
    @Test
    void testTransformToUpdateFunction() throws LDIFException, IOException, URISyntaxException {
        Entry unitEntry = buildEntry("function_entry.ldif");
        
        String transformToUpdateJson = ldapTransformer.transformToUpdateJson(unitEntry.getDN(),
                unitEntry.getAttributes());
        JsonObject update = stringToJsonObject(transformToUpdateJson);
        
        assertNull(update.getJsonObject("newPath"), "newPath should be null");
        assertEquals("MERGE", update.getString("modificationType"));
        assertEquals("Function", update.getJsonObject("function").getString("type"));
    }
    
    
//    {
//        "modificationType": "string",
//        "newPath": "string",
//      }
    @Test
    void testMove() {
        assertEquals("{\"modificationType\":\"MOVE\",\"newPath\":\"countries/Sverige/organizations/Svensk Organisation/units/Enheten\"}", ldapTransformer.transformMoveToJson("countries/Sverige/organizations/Svensk Organisation/units/Enheten"));
        assertEquals("{\"modificationType\":\"MOVE\",\"newPath\":\"countries/Sverige/organizations/Svensk Organisation/units/Annan enhet/units/Enheten\"}", ldapTransformer.transformMoveToJson("countries/Sverige/organizations/Svensk Organisation/units/Annan enhet/units/Enheten"));
    }
    
    @Test
    void testPAtch() {
        List<Modification> asList = Arrays.asList( new Modification(ModificationType.ADD, "objectClass", ObjectClasses.hsaHealthCareUnit));
//        List<Modification> asList = Arrays.asList(
//                new Modification(ModificationType.DELETE, "description"), 
//                    new Modification(ModificationType.ADD, "description", "beskrivning"), 
//                                                  new Modification(ModificationType.DELETE, AttributeNames.telephoneNumber, "+46812345678", "+4688765432"),
//                                                  new Modification(ModificationType.ADD, AttributeNames.telephoneHours, "3-4#08:00#22:00#Mellandagsöppet#20171227#20171230"),
//                                                  new Modification(ModificationType.ADD, AttributeNames.geographicalCoordinates, "X:6598840,Y:1641424"),
//                                                  new Modification(ModificationType.ADD, AttributeNames.postalAddress, "Testgatan 41$123 45 Staden")
//                                                  );
        String transformModifyToPatchJson = ldapTransformer.transformToPatchJson("cn=foo", asList);
        int x=0;
    }
}
