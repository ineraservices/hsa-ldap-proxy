package se.inera.hsa.proxy.handler;

import static org.junit.jupiter.api.Assertions.*;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.junit.jupiter.api.Test;

import com.unboundid.ldap.sdk.ResultCode;

class ResultCodeMapperTest {

    @Test
    void testGetLdapResultCodeStringInt() {
        assertEquals(ResultCode.NO_SUCH_OBJECT_INT_VALUE, ResultCodeMapper.getLdapResultCode(null, 404));
        assertEquals(ResultCode.ENTRY_ALREADY_EXISTS_INT_VALUE, ResultCodeMapper.getLdapResultCode(createError(ResultCode.ENTRY_ALREADY_EXISTS_INT_VALUE), 404));
    }

    private String createError(int entryAlreadyExistsIntValue) {
        JsonObjectBuilder ob = Json.createObjectBuilder();
        ob.add("code", Integer.toString(entryAlreadyExistsIntValue));
        return ob.build().toString();
    }

}
