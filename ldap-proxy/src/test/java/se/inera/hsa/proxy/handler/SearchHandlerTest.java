package se.inera.hsa.proxy.handler;

import static org.junit.jupiter.api.Assertions.*;
import static se.inera.hsa.proxy.AttributeNames.hsaIdentity;
import static se.inera.hsa.proxy.AttributeNames.hsaSyncId;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;

import se.inera.hsa.proxy.AttributeNames;
import se.inera.hsa.proxy.ObjectClasses;

class SearchHandlerTest {

    private static final String HSA_SYNC_ID = "hsaSyncID1234567890";
    private static final String HSA_IDENTITY = "hsaID-0001";
    private SearchHandler handler;
    @BeforeEach
    private void setup() {
        handler = new SearchHandler();
    }
    
    @Test
    void testSimpleHsaSyncIdFilter() {
        assertEquals("@hsaSyncId:(hsaSyncID1234567890)", handler.getFilterAttribute(Filter.createEqualityFilter(hsaSyncId, HSA_SYNC_ID), hsaSyncId));
        
    }
    @Test
    void testAndHsaSyncIdFilter() {
        assertEquals("@hsaSyncId:(hsaSyncID1234567890)", handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.objectClass, ObjectClasses.organizationalUnit), 
                        Filter.createEqualityFilter(hsaSyncId, HSA_SYNC_ID)), hsaSyncId
                ));
    }
    
    @Test
    void testOrHsaSyncIdFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(
                Filter.createORFilter(
                        Filter.createEqualityFilter(AttributeNames.hsaIdentity, "TEST"),  
                        Filter.createEqualityFilter(hsaSyncId, HSA_SYNC_ID)), hsaSyncId),
                "Should return null");
    }
    
    @Test
    void testHsaSyncIdPresentFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.hsaIdentity, "TEST"),  
                        Filter.createPresenceFilter(hsaSyncId)), hsaSyncId),
                "Should return null");
    }
    
    @Test
    void testHsaSyncIdSubStringFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.hsaIdentity, "TEST"),  
                Filter.createSubstringFilter(hsaSyncId, "TEST", null, null)), hsaSyncId), "Should return null");
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.hsaIdentity, "TEST"),  
                Filter.createSubstringFilter(hsaSyncId, null, new String[] {"TEST"}, null)), hsaSyncId), "Should return null");
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.hsaIdentity, "TEST"),  
                Filter.createSubstringFilter(hsaSyncId, null, null, "TEST")), hsaSyncId), "Should return null");

    }
    
    @Test
    void testFiltersFromTest() throws LDAPException {
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaSyncId));
        assertEquals("@hsaSyncId:(SYNCID)",handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalPerson)(hsaSyncId=SYNCID))"), hsaSyncId));
        assertEquals("@hsaSyncId:(SYNCID)",handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalUnit)(hsaSyncId=SYNCID))"), hsaSyncId));
        assertEquals("@hsaSyncId:(SYNCID)",handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalUnit)(hsaSyncId=SYNCID))"), hsaSyncId));
        assertEquals("@hsaSyncId:(SYNCID)",handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalPerson)(hsaSyncId=SYNCID))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=person)(!(hsaSyncId=SYNCID)))"), hsaSyncId));
        assertEquals("@hsaSyncId:(SYNCID)",handler.getFilterAttribute(Filter.create("(hsaSyncId=SYNCID)"), hsaSyncId));
        assertEquals("@hsaSyncId:(HSASYNCID)",handler.getFilterAttribute(Filter.create("(&(hsaSyncId=HSASYNCID)(objectClass=HSAPersonExtension))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(hsaSyncId=*))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=HSAPersonExtension)(ou=*)(!(hsaSyncId=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=HSAPersonExtension)(hsaIdentity=*)(hsaSyncId=*))"), hsaSyncId));
        assertEquals("@hsaSyncId:(HSASYNCID)",handler.getFilterAttribute(Filter.create("(&(hsaSyncId=HSASYNCID)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaSyncId));
        assertNull(handler.getFilterAttribute(Filter.create("(&(hsaSyncId=*)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaSyncId));
    }
    @Test
    void testNotSyncIdFilter() {
        assertNull(handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.objectClass, ObjectClasses.organizationalUnit), 
                        Filter.createNOTFilter(Filter.createEqualityFilter(hsaSyncId, HSA_SYNC_ID))), hsaSyncId
                ), "Should be null");
//        assertEquals("@hsaSyncId:(23)", handler.getFilterAttribute(Filter.create("(|(&(objectClass=person)(hsaSyncId=23))(&(objectClass=person)(hsaSyncId=12356)))")));
    }
    
    @Test
    void testNotHsaIdentityFilter() {
        assertNull(handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.objectClass, ObjectClasses.organizationalUnit), 
                        Filter.createNOTFilter(Filter.createEqualityFilter(hsaIdentity, HSA_IDENTITY))), hsaIdentity
                ), "Should be null");
//        assertEquals("@hsaSyncId:(23)", handler.getFilterAttribute(Filter.create("(|(&(objectClass=person)(hsaSyncId=23))(&(objectClass=person)(hsaSyncId=12356)))")));
    }
    
    
    @Test
    void testSimpleHsaIdentityFilter() {
        assertEquals("@hsaIdentity:("+HSA_IDENTITY+")", handler.getFilterAttribute(Filter.createEqualityFilter(hsaIdentity, HSA_IDENTITY), hsaIdentity));
        
    }
    @Test
    void testAndHsaIdentityFilter() {
        assertEquals("@hsaIdentity:("+HSA_IDENTITY+")", handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.objectClass, ObjectClasses.organizationalUnit), 
                        Filter.createEqualityFilter(hsaIdentity, HSA_IDENTITY)), hsaIdentity
                ));
    }
    
    @Test
    void testOrHsaIdentityFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(
                Filter.createORFilter(
                        Filter.createEqualityFilter(AttributeNames.hsaIdentity, HSA_IDENTITY),  
                        Filter.createEqualityFilter(hsaSyncId, HSA_SYNC_ID)), hsaIdentity),
                "Should return null");
    }
    
    @Test
    void testHsaIdentityPresentFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(
                Filter.createANDFilter(
                        Filter.createEqualityFilter(AttributeNames.hsaSyncId, hsaSyncId),  
                        Filter.createPresenceFilter(hsaIdentity)), hsaIdentity),
                "Should return null");
    }
    
    @Test
    void testHsaIdentitySubStringFilterShouldBeNull() {
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.description, HSA_IDENTITY),  
                Filter.createSubstringFilter(hsaIdentity, HSA_IDENTITY, null, null)), hsaIdentity), "Should return null");
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.description, HSA_IDENTITY),  
                Filter.createSubstringFilter(hsaIdentity, null, new String[] {HSA_IDENTITY}, null)), hsaIdentity), "Should return null");
        assertNull(handler.getFilterAttribute(Filter.createANDFilter(Filter.createEqualityFilter(AttributeNames.description, HSA_IDENTITY),  
                Filter.createSubstringFilter(hsaIdentity, null, null, HSA_IDENTITY)), hsaIdentity), "Should return null");
        
    }
    
    @Test
    void testFiltersFromTestHsaIDentity() throws LDAPException {
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(|(ObjectClass=inetOrgPerson)(ObjectClass=organizationalUnit)(ObjectClass=hsaAdminCommission)(ObjectClass=organizationalRole)(ObjectClass=hsaCommission)(ObjectClass=organization))(|(hsaSyncId=*)(hsaIdentity=*)))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID)"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organization)(hsaIdentity=*))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalUnit)(hsaIdentity=HSAID))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalRole)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalRole)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID*)"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID*)"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=hsaAdminCommission)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(objectClass=hsaCommission)(hsaIdentity=HSAID))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=hsaCommission)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalPerson)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalUnit)(modifyTimestamp>=STARTDATE)(hsaIdentity=*))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID)"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(|(objectClass=organizationalUnit)(objectClass=organization)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(hsaIdentity=*)(objectClass=HSACommission))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(objectClass=HsaOrganizationExtension))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(personalIdentityNumber=PNR)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(objectClass=HsaOrganizationExtension))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(hsaSyncId=*))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(objectClass=organizationalUnit)(!(objectClass=hsaArchivedObject)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=organizationalUnit)(hsaIdentity=*)(!(hsaIdentity=HSAID))(!(hsaIdentity=HSAID)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=HSACommission)(hsaIdentity=*))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(objectClass=HSAPersonExtension)(hsaIdentity=*)(hsaSyncId=*))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID)"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(hsaIdentity=HSAID)"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(objectClass=HSACommission))"), hsaIdentity));
        assertNull(    handler.getFilterAttribute(Filter.create("(&(hsaSyncId=HSASYNCID)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(|(hsaIdentity=HSAID)(hsaIdentity=HSAID))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(|(objectClass=organization)(objectClass=organizationalUnit)))"), hsaIdentity));
        assertEquals("@hsaIdentity:(HSAID)", handler.getFilterAttribute(Filter.create("(&(hsaIdentity=HSAID)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaIdentity));
        assertNull(handler.getFilterAttribute(Filter.create("(&(hsaSyncId=*)(&(objectClass=HsaPersonExtension)(hsaIdentity=*)))"), hsaIdentity));
    }
    

}
