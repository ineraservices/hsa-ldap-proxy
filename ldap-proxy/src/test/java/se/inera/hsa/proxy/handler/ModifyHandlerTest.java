package se.inera.hsa.proxy.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ModifyHandlerTest {

    @Test
    public void testDn2Path() {
        String path = ModifyHandler.transposeDnToApiPatchPath("ou=Vårdcentral,o=Landstinget Dalarna, l=Dalarnas län,c=SE", null);
        String expected = "countries/Sverige/counties/Dalarnas län/organizations/Landstinget Dalarna/units/Vårdcentral";
        assertEquals(expected, path);
    }

}