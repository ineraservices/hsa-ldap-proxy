package se.inera.hsa.proxy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class HelperTest extends BaseTest {

    @Test
    void getParentDn() {
        String dn = "ou=Dala-Floda Vårdcentral,ou=Falu Lasarett,o=Landstinget Dalarna,l=Dalarnas län,c=SE";
        String parentDn = "ou=Falu Lasarett,o=Landstinget Dalarna,l=Dalarnas län,c=SE";
        assertEquals(parentDn, Helper.getParentDn(dn));
    }

}