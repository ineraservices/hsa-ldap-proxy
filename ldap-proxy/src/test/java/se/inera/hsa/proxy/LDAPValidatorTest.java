package se.inera.hsa.proxy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.protocol.BindRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNResponseProtocolOp;
import com.unboundid.ldap.protocol.ModifyRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyResponseProtocolOp;
import com.unboundid.ldap.protocol.SearchRequestProtocolOp;
import com.unboundid.ldap.protocol.SearchResultDoneProtocolOp;
import com.unboundid.ldap.sdk.DereferencePolicy;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;

import se.inera.hsa.proxy.exception.DNException;

class LDAPValidatorTest {
	private static Schema schema;

	@BeforeAll
	static void dasdasas() throws LDIFException, IOException, URISyntaxException {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL resources = cl.getResource("hsa-schema.ldif");
		assert resources != null;
		URI uri = new URI(resources.getFile());
		FileInputStream fileInputStream = new FileInputStream(uri.getPath());
		schema = Schema.getSchema(fileInputStream);
	}

	@Disabled("This should be checked in org-api instead")
	@Test
	void missingAttribute() throws LDIFException, IOException {
		Entry e = new Entry("o=asd,c=Se");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("objectClass", "top");
		e.addAttribute("o", "asd");
		e.addAttribute("missingAttribute", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.UNDEFINED_ATTRIBUTE_TYPE, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Disabled("This should be checked in org-api instead")
	@Test
	void invalidAttributeSyntax() throws LDIFException, IOException {
		Entry e = new Entry("o=asd,c=Se");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("objectClass", "top");
		e.addAttribute("o", "asd");
		e.addAttribute("endDate", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.INVALID_ATTRIBUTE_SYNTAX, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Test
	void invalidDnSyntax() throws LDIFException, IOException {
		Entry e = new Entry("invalidDn");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("objectClass", "top");
		e.addAttribute("o", "asd");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.INVALID_DN_SYNTAX, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Test
	void undefinedObjectClass() throws LDIFException, IOException {
		Entry e = new Entry("o=test,c=Se");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "notValidObjectClass");
		e.addAttribute("o", "asd");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.OBJECT_CLASS_VIOLATION, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Test
	void noStructuraObjectClass() throws LDIFException, IOException {
		Entry e = new Entry("o=test,c=Se");
		e.addAttribute("objectClass", "top");
		e.addAttribute("o", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.OBJECT_CLASS_VIOLATION, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Test
	void multipleStructuraObjectClass() throws LDIFException, IOException {
		Entry e = new Entry("o=test,c=Se");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("objectClass", "organizationalUnit");
		e.addAttribute("o", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.OBJECT_CLASS_VIOLATION, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Disabled("This should be checked in org-api instead")
	@Test
	void missingRequiredAttribute() throws LDIFException, IOException {
		Entry e = new Entry("cn=test");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "person");
		e.addAttribute("cn", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.OBJECT_CLASS_VIOLATION, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Disabled("This should be checked in org-api instead")
	@Test
	void nonAllowedAttribute() throws LDIFException, IOException {
		Entry e = new Entry("o=test,c=se");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("o", "test");
		e.addAttribute("sn", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertEquals(ResultCode.OBJECT_CLASS_VIOLATION, ResultCode.valueOf(validateAddRequest.get().getResultCode()));
	}

	@Test
	void correctEntry() throws LDIFException, IOException {
		Entry e = new Entry("ou=test,o=org,c=se");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "organizationalUnit");
		e.addAttribute("ou", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertFalse(validateAddRequest.isPresent());
	}
	@Test
	void cantAddOrg() throws LDIFException, IOException {
		Entry e = new Entry("o=org,c=se");
		e.addAttribute("objectClass", "top");
		e.addAttribute("objectClass", "organization");
		e.addAttribute("o", "test");

		Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(e.getDN(), e.getAttributes() , schema);

		assertTrue(validateAddRequest.isPresent());
	}

	@Test
	void modifyNotAllowedOnRDNAttribute() {
		List<Modification> modifications = new ArrayList<>();
		modifications.add(new Modification(ModificationType.ADD, AttributeNames.cn, "value"));
		Optional<ModifyResponseProtocolOp> validateModifyRequest = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("cn=foo", modifications ), schema);
		assertFalse(validateModifyRequest.isPresent());

		modifications.clear();
		modifications.add(new Modification(ModificationType.ADD, AttributeNames.ou, "value"));
		Optional<ModifyResponseProtocolOp> validateModifyRequest2 = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("ou=foo", modifications ), schema);
		assertFalse(validateModifyRequest2.isPresent());

		modifications.clear();
		modifications.add(new Modification(ModificationType.DELETE, AttributeNames.cn, "value"));
		Optional<ModifyResponseProtocolOp> validateModifyRequest1 = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("cn=foo", modifications ), schema);
		assertTrue(validateModifyRequest1.isPresent());

		modifications.clear();
		modifications.add(new Modification(ModificationType.DELETE, AttributeNames.ou, "value"));
		Optional<ModifyResponseProtocolOp> validateModifyRequest3 = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("ou=foo", modifications ), schema);
		assertTrue(validateModifyRequest3.isPresent());

		modifications.clear();
		modifications.add(new Modification(ModificationType.ADD, AttributeNames.title, "value"));
		modifications.add(new Modification(ModificationType.INCREMENT, AttributeNames.title, "1"));
		Optional<ModifyResponseProtocolOp> validateModifyRequest4 = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("ou=foo", modifications ), schema);
		assertTrue(validateModifyRequest4.isPresent());
		assertEquals(ResultCode.NOT_SUPPORTED_INT_VALUE, validateModifyRequest4.get().getResultCode(), ""+ResultCode.valueOf(validateModifyRequest4.get().getResultCode()));
	}
	@Test
	void modifyIllegalDnSyntax() {
		List<Modification> modifications = new ArrayList<>();
		Optional<ModifyResponseProtocolOp> validateAddRequest = LDAPValidator.validateModifyRequest(new ModifyRequestProtocolOp("foo", modifications), schema);

		assertTrue(validateAddRequest.isPresent());
	}

	@Test
	void modifyDNRequestValidation() {
		Optional<ModifyDNResponseProtocolOp> errorResponse = LDAPValidator.validateModifyDNRequest(new ModifyDNRequestProtocolOp("foo,c=se", "cn=bar", true, "c=se"), schema);

		assertTrue(errorResponse.isPresent());
	}

	@Test
	void searchRequestValidation() throws LDIFException, IOException, LDAPException {
		//correctFilter
		Filter filter = Filter.create("(&(objectClass=person)(!(middleName=tomte))(|(cn=tomte)(sn=tomte)))");
		Optional<SearchResultDoneProtocolOp> validateSearchRequest1 = LDAPValidator.validateSearchRequest(new SearchRequestProtocolOp("c=se", SearchScope.SUB, DereferencePolicy.ALWAYS, 1, 1, false, filter, Arrays.asList("1.1")), schema);
		assertFalse(validateSearchRequest1.isPresent());

		//incorrectFilter
		Filter filter1 = Filter.create("(&(objectClass=person)(!(middleName=tomte))(|(errorAttribute=tomte)(sn=tomte)))");
		Optional<SearchResultDoneProtocolOp> validateSearchRequest2 = LDAPValidator.validateSearchRequest(new SearchRequestProtocolOp("c=se", SearchScope.SUB, DereferencePolicy.ALWAYS, 1, 1, false, filter1, Arrays.asList("1.1")), schema);
		assertTrue(validateSearchRequest2.isPresent());
		assertEquals(ResultCode.UNDEFINED_ATTRIBUTE_TYPE_INT_VALUE, validateSearchRequest2.get().getResultCode());

		//incorrectDN
		Filter filter2 = Filter.create("(&(objectClass=person))");
		Optional<SearchResultDoneProtocolOp> validateSearchRequest3 = LDAPValidator.validateSearchRequest(new SearchRequestProtocolOp("se", SearchScope.SUB, DereferencePolicy.ALWAYS, 1, 1, false, filter2, Arrays.asList("1.1")), schema);
		assertTrue(validateSearchRequest3.isPresent());
		assertEquals(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, validateSearchRequest3.get().getResultCode());

		//incorrectSearchScope
		Filter filter3 = Filter.create("(&(objectClass=person))");
		Optional<SearchResultDoneProtocolOp> validateSearchRequest4 = LDAPValidator.validateSearchRequest(new SearchRequestProtocolOp("c=se", SearchScope.SUBORDINATE_SUBTREE, DereferencePolicy.ALWAYS, 1, 1, false, filter3, Arrays.asList("1.1")), schema);
		assertTrue(validateSearchRequest4.isPresent());
		assertEquals(ResultCode.PROTOCOL_ERROR_INT_VALUE, validateSearchRequest4.get().getResultCode());
	}
	@Test
	void testDeleteRequestValidation() throws LDIFException, IOException {
		assertFalse(LDAPValidator.validateDeleteRequest("o=test").isPresent());

		assertTrue(LDAPValidator.validateDeleteRequest("test").isPresent());
	}

	@Test
	void testBindRequestValidation() throws LDIFException, IOException {
		assertEquals(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, LDAPValidator.validateBindRequest(new BindRequestProtocolOp("", "")).get().getResultCode());
		assertEquals(ResultCode.AUTH_METHOD_NOT_SUPPORTED_INT_VALUE, LDAPValidator.validateBindRequest(new BindRequestProtocolOp("", "", new ASN1OctetString() )).get().getResultCode());
	}



	@Test
	void correctDnStructure() throws DNException {
	    LDAPValidator.checkDNStructure("c=Se");
	    LDAPValidator.checkDNStructure("o=asd,c=Se");
	    LDAPValidator.checkDNStructure("l=asd,c=Se");
	    LDAPValidator.checkDNStructure("o=test,l=asd,c=Se");
	    LDAPValidator.checkDNStructure("cn=name,o=test,l=asd,c=Se");
	    LDAPValidator.checkDNStructure("ou=sub,o=test,l=asd,c=Se");
	    LDAPValidator.checkDNStructure("cn=name,ou=sub,o=test,l=asd,c=Se");
	    LDAPValidator.checkDNStructure("ou=subusb,ou=sub,o=test,l=asd,c=Se");
	    LDAPValidator.checkDNStructure("cn=name,ou=subusb,ou=sub,o=test,l=asd,c=Se");
	}
	@Test
	void incorrectDnStructure() {
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("o=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("ou=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("l=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("cn=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("ou=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("cn=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("ou=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("cn=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("o=test2,o=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("l=test2,o=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("c=test2,o=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("c=fel,ou=test2,o=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("o=fel,ou=test2,o=test,l=asd,c=Se"));
	    assertThrows(DNException.class, () -> LDAPValidator.checkDNStructure("l=fel,ou=test2,o=test,l=asd,c=Se"));
	}
}
