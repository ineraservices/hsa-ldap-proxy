package se.inera.hsa.proxy.transformer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.unboundid.ldap.sdk.Entry;

import se.inera.hsa.proxy.ObjectClasses;

class JsonMapTransformerTest {

    private JsonMapTransformer jsonTransformer;

    @BeforeEach
    void setUp() {
        jsonTransformer = new JsonMapTransformer();
    }
    
    @Test
    
    void testTransformToEntryMapOfStringObject() {
//        {
//            unitPrescriptionCode=[01110016530], 
//            metaData= {
//              creatorPath=countries/Sverige/organizations/Värmlands län/employees/admin, 
//              path=countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/TestFunk, 
//              numAllSubordinates=0, 
//              creatorUUID=a4c42f36-1069-47b6-ac5c-561c7a9a43da, 
//              numSubordinates=0, 
//              createTimestamp=2019-01-17T09:04:44.777Z[UTC]
//            }, 
//            hsaIdentity=TEST999999903-XXXXX, 
//            telephoneNumber=[+4610520000], 
//            name=TestFunk, 
//            type=Function, 
//            uuid=a4c42f36-1069-47b6-ac5c-561c7a9a43da
//          }
        HashMap<String, Object> test = new HashMap<>();
        test.put("unitPrescriptionCode", Arrays.asList("01110016530"));
        test.put("telephoneNumber", Arrays.asList("+4610520000"));
        test.put("type", "Function");
        test.put("name", "TestFunk");
        test.put("uuid", "a4c42f36-1069-47b6-ac5c-561c7a9a43da");
        test.put("hsaIdentity", "TEST999999903-XXXXX");
        test.put("hsaHealthCareProvider", true);
        test.put("hsaHealthCareUnit", true);
        test.put("localityName", "Karlskrona");
        
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("creatorPath", "countries/Sverige/organizations/Värmlands län/employees/admin");
        metaData.put("path", "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/TestFunk");
        metaData.put("numAllSubordinates", 0);
        metaData.put("numSubordinates", 0);
        metaData.put("creatorUUID", "a4c42f36-1069-47b6-ac5c-561c7a9a43da");
        metaData.put("createTimestamp", "2019-01-17T09:04:44.777Z[UTC]");
        test.put("metadata", metaData);
        Optional<Entry> entry = jsonTransformer.transformToEntry(test);
        
        assertEquals("cn=TestFunk,ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE", entry.get().getDN());
        assertArrayEquals(new String[] {"01110016530"}, entry.get().getAttributeValues("unitPrescriptionCode"));
        assertArrayEquals(new String[] {"+4610520000"}, entry.get().getAttributeValues("telephoneNumber"));
        assertArrayEquals(new String[] {"Karlskrona"}, entry.get().getAttributeValues("l"));
        assertTrue(entry.get().hasObjectClass(ObjectClasses.hsaHealthCareUnit), "Object should have hsaHealthCareUnit");
        assertTrue(entry.get().hasObjectClass(ObjectClasses.hsaHealthCareProvider), "Object should have hsaHealthCareProvider");
    }

}
