package se.inera.hsa.proxy.handler;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.protocol.DeleteRequestProtocolOp;
import com.unboundid.ldap.protocol.DeleteResponseProtocolOp;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.hsa.proxy.transformer.LdapTransformer;

class DeleteHandlerTest {

	@Mock
    WebTarget employeeTarget;

    @Mock
    WebTarget organizationTarget;
    
    @Mock
    WebTarget oldOrganizationTarget;
    
    @Mock
    private LdapTransformer ldapTransformer;
    
    @Mock
    private Invocation.Builder employeeBuilder;
    
    @Mock
    private Response employeeResponse;

    @Mock
    private Response organizationResponse;
    
    @Mock
    private Response.StatusType statusType;

    @Mock
    private Invocation.Builder organizationBuilder;
    
    @Mock
    private Invocation invocation;

    @Mock
    private LDAPListenerClientConnection connection;

    @Mock
    private Entry entry;

    @Mock
    MultivaluedMap<String, Object> map;
    
    @Mock
    private SearchHandler searchHandler;
    
    @InjectMocks
    private DeleteHandler deleteHandler;

    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
    @Test
    void testProcessDeleteOrganizationRequestNoSuchObject() {
    	
    	String dnString = "o=organization,c=SE";
    	
    	DeleteRequestProtocolOp deleteRequestProtocolOp = new DeleteRequestProtocolOp(dnString);
    	
    	when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
    	when(organizationTarget.request()).thenReturn(organizationBuilder);
    	when(organizationBuilder.buildDelete()).thenReturn(invocation);
    	when(invocation.invoke()).thenReturn(organizationResponse);
    	
    	when(organizationResponse.getStatusInfo()).thenReturn(statusType);
    	when(statusType.getFamily()).thenReturn(Response.Status.Family.SERVER_ERROR);
    	when(organizationResponse.readEntity(String.class)).thenReturn(format("{\"code\": \"%s\"}", ResultCode.NO_SUCH_OBJECT_INT_VALUE));
        
    	DeleteResponseProtocolOp message = deleteHandler.handle(deleteRequestProtocolOp, organizationTarget);
    	
    	assertEquals(ResultCode.NO_SUCH_OBJECT_INT_VALUE, message.getResultCode());
    }
    
    
    @Test
    void testProcessDeleteOrganizationRequestSuccess() {

        String dnString = "o=organization,c=SE";

        DeleteRequestProtocolOp deleteRequestProtocolOp = new DeleteRequestProtocolOp(dnString);
        
        when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
        when(organizationTarget.request()).thenReturn(organizationBuilder);
        when(organizationBuilder.buildDelete()).thenReturn(invocation);
        when(invocation.invoke()).thenReturn(organizationResponse);
        
        when(organizationResponse.getStatusInfo()).thenReturn(statusType);
        when(statusType.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);
        
        DeleteResponseProtocolOp message = deleteHandler.handle(deleteRequestProtocolOp, organizationTarget);
        
        assertEquals(ResultCode.SUCCESS_INT_VALUE, message.getResultCode());
    }
	
	@Test
	void testProcessDeleteOrganizationRequestNotALeaf() {
		
		String dnString = "o=organization,c=SE";
		
		DeleteRequestProtocolOp deleteRequestProtocolOp = new DeleteRequestProtocolOp(dnString);
        
        when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
        when(organizationTarget.request()).thenReturn(organizationBuilder);
        when(organizationBuilder.buildDelete()).thenReturn(invocation);
        when(invocation.invoke()).thenReturn(organizationResponse);
        
        when(organizationResponse.getStatusInfo()).thenReturn(statusType);
        when(statusType.getFamily()).thenReturn(Response.Status.Family.SERVER_ERROR);
        when(organizationResponse.readEntity(String.class)).thenReturn(format("{\"code\": \"%s\"}", ResultCode.NOT_ALLOWED_ON_NONLEAF_INT_VALUE));
        
        DeleteResponseProtocolOp message = deleteHandler.handle(deleteRequestProtocolOp, organizationTarget);
		
		assertEquals(ResultCode.NOT_ALLOWED_ON_NONLEAF_INT_VALUE, message.getResultCode());
	}
	
    @Test
    @Disabled
    void testProcessDeleteEmployeeRequestSuccess() {

        String dnString = "cn=Nisse Nilsson,o=organization,c=SE";

        DeleteRequestProtocolOp deleteRequestProtocolOp = new DeleteRequestProtocolOp(dnString);

        // transformer
        when(ldapTransformer.transformToJson(anyString(), anyCollection())).thenReturn("{}");

        // status type successful in calls to both apis
        when(statusType.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);

        // employee target, entity exists
        when(employeeTarget.path(anyString())).thenReturn(employeeTarget);
        when(employeeTarget.request()).thenReturn(employeeBuilder);
        when(employeeBuilder.get()).thenReturn(employeeResponse);
        when(employeeBuilder.delete()).thenReturn(employeeResponse);
        when(employeeResponse.getStatusInfo()).thenReturn(statusType);
        when(employeeResponse.hasEntity()).thenReturn(true);

        // organization, no entity exists
        when(oldOrganizationTarget.path(anyString())).thenReturn(oldOrganizationTarget);
        when(oldOrganizationTarget.request()).thenReturn(organizationBuilder);
        
        when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
        when(organizationTarget.request()).thenReturn(organizationBuilder);
        when(organizationBuilder.get()).thenReturn(organizationResponse);
        when(organizationResponse.getStatusInfo()).thenReturn(statusType);
        when(organizationResponse.hasEntity()).thenReturn(false);


        DeleteResponseProtocolOp message = deleteHandler.handle(deleteRequestProtocolOp, organizationTarget);
        
        assertEquals(ResultCode.SUCCESS_INT_VALUE, message.getResultCode());

    }

}
