package se.inera.hsa.proxy;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;
import com.unboundid.ldif.LDIFReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class BaseTest {

    protected Entry buildEntry(String fileName) throws IOException, LDIFException, URISyntaxException {
        // handle spaces in path by using uri
        URI fileRef = getFileRef(fileName);
        FileInputStream fileInputStream = new FileInputStream(Paths.get(fileRef).toString());
        return LDIFReader.readEntries(fileInputStream).get(0);
    }

    protected JsonObject stringToJsonObject(String input) {

        StringReader stringReader = new StringReader(input);

        JsonReader jsonReader = Json.createReader(stringReader);

        JsonObject object = jsonReader.readObject();

        stringReader.close();

        jsonReader.close();

        return object;
    }

    protected String parseFile(String fileName) throws IOException, URISyntaxException {
        URI fileRef = getFileRef(fileName);
        return new String(Files.readAllBytes(Paths.get(Paths.get(fileRef).toString())), StandardCharsets.UTF_8);
    }

    private URI getFileRef(String fileName) throws URISyntaxException, MalformedURLException {
        // handle spaces in path by using uri
        URL resource = getClass().getClassLoader().getResource(fileName);
        assert resource != null;
        URI uri = resource.toURI();
        return uri;
    }

}
