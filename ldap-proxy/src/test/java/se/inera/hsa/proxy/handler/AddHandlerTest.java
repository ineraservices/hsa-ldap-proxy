package se.inera.hsa.proxy.handler;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.protocol.AddRequestProtocolOp;
import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Entry;

import se.inera.hsa.proxy.transformer.LdapTransformer;

class AddHandlerTest {
	@Mock
    WebTarget employeeTarget;

    @Mock
    WebTarget organizationTarget;
    
    @Mock
    WebTarget oldOrganizationTarget;
    
    @Mock
    private LdapTransformer ldapTransformer;
    
    @Mock
    private Invocation.Builder employeeBuilder;
    
    @Mock
    private Response employeeResponse;

    @Mock
    private Response organizationResponse;
    
    @Mock
    private Response.StatusType statusType;

    @Mock
    private Invocation.Builder organizationBuilder;
    
    @Mock
    private Invocation invocation;

    @Mock
    private LDAPListenerClientConnection connection;

    @Mock
    private Entry entry;

    @Mock
    MultivaluedMap<String, Object> map;
    
    @Mock
    private SearchHandler searchHandler;
    
    @InjectMocks
    private AddHandler addHandler;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test() {
        String path = AddHandler.transposeDnToApiPostPath("o=Landstinget Dalarna, l=Dalarnas län,c=SE", Collections.emptyList());
        String expected = "countries/Sverige/counties/Dalarnas län/organizations";
        assertEquals(expected, path);
    }
    
    @Test
    @Disabled
    void testProcessAddEmployeeRequestSuccess() {

        String dnString = "cn=Nisse Nilsson,c=SE";
        Attribute attribute = new Attribute("objectClass", "person");
        List<Attribute> attributes = singletonList(attribute);
        AddRequestProtocolOp addRequestProtocolOp = new AddRequestProtocolOp(dnString, attributes);

        // transformer
        when(ldapTransformer.transformToJson(anyString(), anyCollection())).thenReturn("{}");

        // status type successful in calls to both apis
        when(statusType.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);

        // employee target, no entity exists => put
        when(employeeTarget.path(anyString())).thenReturn(employeeTarget);
        when(employeeTarget.request()).thenReturn(employeeBuilder);
        when(employeeBuilder.get()).thenReturn(employeeResponse);
        when(employeeBuilder.put(any())).thenReturn(employeeResponse);
        when(employeeResponse.hasEntity()).thenReturn(false);
        when(employeeResponse.getStatusInfo()).thenReturn(statusType);

        // organization, entity exists
        when(oldOrganizationTarget.path(anyString())).thenReturn(oldOrganizationTarget);
        when(oldOrganizationTarget.request()).thenReturn(organizationBuilder);
        
        when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
        when(organizationTarget.request()).thenReturn(organizationBuilder);
        when(organizationBuilder.get()).thenReturn(organizationResponse);
        when(organizationResponse.getStatusInfo()).thenReturn(statusType);

        when(organizationResponse.hasEntity()).thenReturn(true);

        AddResponseProtocolOp addResponseProtocolOp = addHandler.handle(addRequestProtocolOp, organizationTarget);

        assertEquals(0, addResponseProtocolOp.getResultCode());
    }

    @SuppressWarnings("unchecked")
    @Test
    void testProcessAddOrganizationRequestSuccess() {

        String dnString = "o=organization,c=SE";
        Attribute attribute = new Attribute("objectClass", "organization");
        List<Attribute> attributes = singletonList(attribute);
        AddRequestProtocolOp addRequestProtocolOp = new AddRequestProtocolOp(dnString, attributes);

        // transformer
        when(ldapTransformer.transformToJson(anyString(), anyCollection())).thenReturn("{}");

        // status type successful in calls to both apis
        when(statusType.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);

        // organization, entity does not exist
        when(oldOrganizationTarget.path(anyString())).thenReturn(oldOrganizationTarget);
        when(oldOrganizationTarget.request()).thenReturn(organizationBuilder);
        
        when(organizationTarget.path(anyString())).thenReturn(organizationTarget);
        when(organizationTarget.request()).thenReturn(organizationBuilder);
        when(organizationBuilder.get()).thenReturn(organizationResponse);
        when(organizationBuilder.put(any())).thenReturn(organizationResponse);
        when(organizationBuilder.buildPost(any())).thenReturn(invocation);
        
        when(invocation.invoke()).thenReturn(organizationResponse);
        
        when(organizationResponse.hasEntity()).thenReturn(false);
        when(organizationResponse.readEntity(String.class)).thenReturn("{\"id\": \"1\"}");
        when(organizationResponse.getStatusInfo()).thenReturn(statusType);
        when(organizationResponse.getHeaders()).thenReturn(map);
        when(map.get(anyString())).thenReturn(new ArrayList<>());

        AddResponseProtocolOp addResponseProtocolOp = addHandler.handle(addRequestProtocolOp, organizationTarget);

        assertEquals(0, addResponseProtocolOp.getResultCode());
    }
}
