package se.inera.hsa.proxy.handler;

import com.unboundid.ldap.sdk.ResultCode;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.core.Response;

/**
 * Mapper that tries to map http status code to ldap resultCode.
 */
public enum ResultCodeMapper {
    FAMILY_SUCCESS(Response.Status.Family.SUCCESSFUL.ordinal(), ResultCode.SUCCESS_INT_VALUE),
    FORBIDDEN(Response.Status.FORBIDDEN.getStatusCode(), ResultCode.ENTRY_ALREADY_EXISTS_INT_VALUE),
    BAD_REQUEST(Response.Status.BAD_REQUEST.getStatusCode(), ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE),
    NOT_FOUND(Response.Status.NOT_FOUND.getStatusCode(), ResultCode.NO_SUCH_OBJECT_INT_VALUE),
    NO_CODE_MAPPING_FOUND(-1, ResultCode.UNWILLING_TO_PERFORM_INT_VALUE);


    private final int httpStatusCode;
    private final int ldapResultCode;

    ResultCodeMapper(int httpStatusCode, int ldapResultCode) {
        this.httpStatusCode = httpStatusCode;
        this.ldapResultCode = ldapResultCode;
    }

    public static int getLdapResultCode(int httpStatusCode) {
        for (ResultCodeMapper resultCodeMapper : ResultCodeMapper.values()) {
            if (resultCodeMapper.httpStatusCode == httpStatusCode) {
                return resultCodeMapper.ldapResultCode;
            }
        }
        return NO_CODE_MAPPING_FOUND.ldapResultCode;
    }

    /**
     * Tries to get an errorcode from the json object and return that, if no code is found in json
     * then tries to map http Statuscode to ldap-resultcode
     */
    public static int getLdapResultCode(String errorJson, int httpStatusCode) {
       String codeStr = null;
        if ( errorJson != null && !errorJson.isEmpty() ) {
            try (JsonReader jsonReader = Json.createReader(new StringReader(errorJson))) {
                JsonObject jsonObject = jsonReader.readObject();

                if ( jsonObject != null ) {
                    try {
                        codeStr = jsonObject.getString("code", "");
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if ( codeStr == null || codeStr.isEmpty() ) {
           return getLdapResultCode(httpStatusCode);
        }
        return Integer.valueOf(codeStr);
    }

    /**
     * Get errorMessage from json message, if no error message exist return empty string.
     */
    public static String getErrorMessage(String errorMessageJson) {
    	String message ="";
    	if ( errorMessageJson != null && !errorMessageJson.isEmpty() ) {
    	    try (JsonReader jsonReader = Json.createReader(new StringReader(errorMessageJson))) {
    	        JsonObject jsonObject = jsonReader.readObject();

    	        if ( jsonObject != null ) {
    	            if ( jsonObject.containsKey("message")) {
    	                message += jsonObject.getString("message", "");
    	            } else if ( jsonObject.containsKey("unit")) {
                        message += jsonObject.getString("unit", "");
                    } else if ( jsonObject.containsKey("function")) {
                        message += jsonObject.getString("function", "");
                    } else if ( jsonObject.containsKey("organization")) {
                        message += jsonObject.getString("organization", "");
                    }
    	        }
    	    }
    	}
    	return message;
    }
}
