package se.inera.hsa.proxy.handler;

import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EQUALS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FRONT_SLASH;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNITS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNIT_RDN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.unboundid.ldap.protocol.DeleteRequestProtocolOp;
import com.unboundid.ldap.protocol.DeleteResponseProtocolOp;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.hsa.proxy.Helper;
import se.inera.hsa.proxy.transformer.ApiSpecifics;

/**
 * This handler is responsible for handling the DELETE LDAP request. The request is converted to JSON format that
 * is then sent to the organization api using the converted path as URL.
 */
public class DeleteHandler {

    private static final String EMPTY_STRING = "";

   	public DeleteResponseProtocolOp handle(DeleteRequestProtocolOp request, WebTarget restApiTarget) {
        Response response = callRestApi(request, restApiTarget);

        return mapResponse(request.getDN(), response);
    }


    /**
     * Transforms the DeleteRequest to json format and then calls the organization api DELETE with correct path.
     */
    private Response callRestApi(DeleteRequestProtocolOp request, WebTarget baseApiTarget) {
        String restApiResourcePath = transposeDnToApiDeletePath(request, baseApiTarget);

        WebTarget target = baseApiTarget.path(restApiResourcePath);
        Builder deleteRequest = target.request();
        deleteRequest.accept(MediaType.APPLICATION_JSON);
        System.out.println("Calling api DELETE Path: " + target.getUri() + " incoming dn = " + request.getDN());
        return deleteRequest.buildDelete().invoke();
    }

    /**
     * Map from REST-restResponse to LDAP-restResponse
     * Make sure errors are mapped etc
     * Make sure api returns codes in body for clarification.
     *
     * @param dn dn from request
     * @param restResponse rest restResponse
     * @return ldap restResponse
     */
    private DeleteResponseProtocolOp mapResponse(String dn, Response restResponse) {
        if (restResponse.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            return new DeleteResponseProtocolOp(ResultCode.SUCCESS_INT_VALUE, EMPTY_STRING, EMPTY_STRING,
                    null);
        }

        String errorJson = restResponse.readEntity(String.class);
        int responseCode = ResultCodeMapper.getLdapResultCode(errorJson, restResponse.getStatus());
        String errorMessage = ResultCodeMapper.getErrorMessage(errorJson);
        return new DeleteResponseProtocolOp(responseCode, Helper.getParentDn(dn), errorMessage, null);
    }

    /**
     * Transform dn to the path used in organization-api.
     * If the DN starts with 'cn=' then a search is made to get the correct organization-path,
     * since several objects use 'cn=' as rdn attribute in DN.
     */
    public static String transposeDnToApiDeletePath(DeleteRequestProtocolOp request, WebTarget baseApiTarget) {
        if ( request.getDN().toLowerCase().startsWith("cn=")) {
            String tmpPath = SearchHandler.getPathToDn(request.getDN(), baseApiTarget);
            if ( tmpPath != null && !tmpPath.isEmpty() ) {
                return tmpPath;
            }
        }
        String tmpDn=request.getDN().replaceAll("(?i)"+Pattern.quote("c=se"), "c=Sverige");
        List<String> list = Arrays.asList(tmpDn.split(ApiSpecifics.COMMA));
        List<String> transposedList = list.stream().map(DeleteHandler::replaceRDNWithPathElement).collect(Collectors.toList());
        Collections.reverse(transposedList);
        return String.join(ApiSpecifics.FRONT_SLASH, transposedList);
    }

    /**
     * Replaces RDN with the path equivalent name.
     */
    private static String replaceRDNWithPathElement(String rdn) {
        return rdn.trim().replace(COUNTRY_RDN + EQUALS, COUNTRIES + FRONT_SLASH)
                .replace(COUNTY_RDN + EQUALS, COUNTIES + FRONT_SLASH)
                .replace(ORGANIZATION_RDN + EQUALS, ORGANIZATIONS + FRONT_SLASH)
                .replace(UNIT_RDN + EQUALS, UNITS + FRONT_SLASH)
                .replace(FUNCTION_RDN + EQUALS, FUNCTIONS + FRONT_SLASH);
    }

}
