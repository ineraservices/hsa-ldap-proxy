package se.inera.hsa.proxy.handler;

import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EQUALS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FRONT_SLASH;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNITS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNIT_RDN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.unboundid.ldap.protocol.ModifyDNRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNResponseProtocolOp;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.hsa.proxy.Helper;
import se.inera.hsa.proxy.transformer.ApiSpecifics;
import se.inera.hsa.proxy.transformer.LdapTransformer;

public class ModifyDNHandler {
    private static final String EMPTY_STRING = "";

    @Inject
    private LdapTransformer ldapTransformer;


    public ModifyDNResponseProtocolOp handle(ModifyDNRequestProtocolOp request, WebTarget apiTarget) {
        Response response = callRestApi(request, apiTarget);

        return mapResponse(request.getDN(), response);
    }

    /**
     * Transforms the ModifyDNRequest to json format and then calls the organization api PUT with correct path.
     */
    private Response callRestApi(ModifyDNRequestProtocolOp request, WebTarget baseApiTarget) {
//      TODO: This method is to be re-written to use PATCH and the new format on PATCH, when it is implemented in version 0.5+ organization api
        String restApiResourcePath = transposeDnToApiPutPath(request.getDN(), baseApiTarget);

        WebTarget target = baseApiTarget.path(restApiResourcePath);
        Builder postRequest = target.request();
        postRequest.accept(MediaType.APPLICATION_JSON);

        String newPath = generateNewPath(request, baseApiTarget, restApiResourcePath);

        String modificationJson = ldapTransformer.transformMoveToJson(newPath);

        Entity<String> stringEntities = Entity.json(modificationJson);

        System.out.println("Calling api PUT Path: " + target.getUri() + " incoming dn = " + request.getDN());
        return postRequest.buildPut(stringEntities).invoke();
    }

    /**
     * Generate the new path from the request
     */
    private String generateNewPath(ModifyDNRequestProtocolOp request, WebTarget baseApiTarget,
            String restApiResourcePath) {
        int lastIndexOfSlash = restApiResourcePath.lastIndexOf('/');
        int lastIndexOf = restApiResourcePath.lastIndexOf('/', lastIndexOfSlash-1);

        String newSuperiorPath = restApiResourcePath.substring(0, lastIndexOf);
        if ( request.getNewSuperiorDN() != null && !request.getNewSuperiorDN().isEmpty() ) {
            newSuperiorPath = transposeDnToApiPutPath(request.getNewSuperiorDN(), baseApiTarget);
        }
        String type = restApiResourcePath.substring(lastIndexOf, lastIndexOfSlash+1);
        String newName = request.getNewRDN().substring(request.getNewRDN().indexOf('=')+1);

        String newPath = newSuperiorPath+type+newName;
        return newPath;
    }


    /**
     * Transform dn to the path used in organization-api.
     * If the DN starts with 'cn=' then a search is made to get the correct organization-path,
     * since several objects use 'cn=' as rdn attribute in DN.
     */
	public static String transposeDnToApiPutPath(String dn, WebTarget baseApiTarget) {
	    if ( dn.toLowerCase().startsWith("cn=")) {
            String tmpPath = SearchHandler.getPathToDn(dn, baseApiTarget);
            if ( tmpPath != null && !tmpPath.isEmpty() ) {
                return tmpPath;
            }
        }
	    String tmp = dn.replaceAll("(?i)"+Pattern.quote("c=se"), "c=Sverige");
        List<String> list = Arrays.asList(tmp.split(ApiSpecifics.COMMA));
        List<String> transposedList = list.stream().map(ModifyDNHandler::replaceRDNWithPathElement).collect(Collectors.toList());
        Collections.reverse(transposedList);
        return String.join(ApiSpecifics.FRONT_SLASH, transposedList);
    }

	/**
     * Replaces RDN with the path equivalent name.
     */
    private static String replaceRDNWithPathElement(String rdn) {
        return rdn.trim().replace(COUNTRY_RDN + EQUALS, COUNTRIES + FRONT_SLASH)
                         .replace(COUNTY_RDN + EQUALS, COUNTIES + FRONT_SLASH)
                         .replace(ORGANIZATION_RDN + EQUALS, ORGANIZATIONS + FRONT_SLASH)
                         .replace(UNIT_RDN + EQUALS, UNITS + FRONT_SLASH)
                         .replace(FUNCTION_RDN + EQUALS, FUNCTIONS + FRONT_SLASH);
    }

    /**
     * Map from REST-restResponse to LDAP-restResponse
     * Make sure errors are mapped etc
     * Make sure api returns codes in body for clarification.
     *
     * @param dn dn from request
     * @param restResponse rest restResponse
     * @return ldap restResponse
     */
    private ModifyDNResponseProtocolOp mapResponse(String dn, Response restResponse) {
        if (restResponse.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            return new ModifyDNResponseProtocolOp(ResultCode.SUCCESS_INT_VALUE, EMPTY_STRING, EMPTY_STRING,
                    null);
        }

        String errorJson = restResponse.readEntity(String.class);
        int responseCode = ResultCodeMapper.getLdapResultCode(errorJson, restResponse.getStatus());
        String errorMessage = ResultCodeMapper.getErrorMessage(errorJson);

        return new ModifyDNResponseProtocolOp(responseCode, Helper.getParentDn(dn), errorMessage, null);
    }
}
