package se.inera.hsa.proxy.handler;

import static se.inera.hsa.proxy.AttributeNames.objectClass;
import static se.inera.hsa.proxy.ObjectClasses.organizationalRole;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EMPLOYEES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EQUALS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FRONT_SLASH;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNITS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNIT_RDN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.unboundid.ldap.protocol.AddRequestProtocolOp;
import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.hsa.proxy.transformer.ApiSpecifics;
import se.inera.hsa.proxy.transformer.LdapTransformer;

/**
 * This handler is responsible for handling the ADD LDAP request. The request is converted to JSON format that
 * is then sent to the organization api using the converted path as URL.
 */
public class AddHandler {
    private static final String EMPTY_STRING = "";

    /**
     * This is used to transform the LDAP request to json format which the organization rest api accepts.
     */
    @Inject
    private LdapTransformer ldapTransformer;


    /**
     * Processes the LDAP request
     */
    public AddResponseProtocolOp handle(AddRequestProtocolOp request, WebTarget restApiTarget) {
        Response response = callRestApi(request, restApiTarget);

        return mapResponse(request.getDN(), response);
    }

    /**
     * Transforms the AddRequest to json format and then calls the organization api POST with correct path.
     */
    private Response callRestApi(AddRequestProtocolOp request, WebTarget baseApiTarget) {
        String restApiResourcePath = transposeDnToApiPostPath(request.getDN(), request.getAttributes());

        WebTarget target = baseApiTarget.path(restApiResourcePath);
        Builder postRequest = target.request();
        postRequest.accept(MediaType.APPLICATION_JSON);

        String json = ldapTransformer.transformToJson(request.getDN(), request.getAttributes());
        Entity<String> stringEntities = Entity.json(json);

        System.out.println("Calling api POST Path: " + target.getUri() + " incoming dn = " + request.getDN());
        return postRequest.buildPost(stringEntities).invoke();
    }

    /**
     * Map from REST-restResponse to LDAP-restResponse
     * Make sure errors are mapped etc
     * Make sure api returns codes in body for clarification.
     *
     * @param dn dn from request
     * @param restResponse rest restResponse
     * @return ldap restResponse
     */
    private AddResponseProtocolOp mapResponse(String dn, Response restResponse) {
        if (restResponse.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            return new AddResponseProtocolOp(ResultCode.SUCCESS_INT_VALUE, EMPTY_STRING, EMPTY_STRING,
                    null);
        }

        String errorJson = restResponse.readEntity(String.class);

        int httpCode = ResultCodeMapper.getLdapResultCode(errorJson, restResponse.getStatus());
        String errorMessage = ResultCodeMapper.getErrorMessage(errorJson);
        return new AddResponseProtocolOp(httpCode, "", errorMessage, null);
    }

    /**
     * Transform dn to the path used in organization-api. Since several objects use attribute 'cn' in DN the attribute
     * 'objectClass' is used to determine which kind of object is sent so that a correct path is generated.
     * @param dn the dn to be transformed.
     * @param attributes the attribute 'objectClass' is used to determine which kind of object is used
     * @return path
     */
    public static String transposeDnToApiPostPath(String dn, List<Attribute> attributes) {
        String tmpDn=dn.replaceAll("(?i)"+Pattern.quote("c=se"), "c=Sverige");
        List<String> list = Arrays.asList(tmpDn.split(ApiSpecifics.COMMA));
        List<String> transposedList = list.stream().map(AddHandler::replaceRDNWithPathElement).collect(Collectors.toList());
        Collections.reverse(transposedList);
        String dnAsPath = String.join(ApiSpecifics.FRONT_SLASH, transposedList);
        String path = dnAsPath.substring(0, dnAsPath.lastIndexOf('/'));
        for (Attribute attribute : attributes) {
            if ( attribute.getName().equalsIgnoreCase(objectClass)) {
                for (String anObjectClass : attribute.getValues()) {
                    if ( anObjectClass.equalsIgnoreCase(organizationalRole)) {
                        return path.replace(EMPLOYEES, FUNCTIONS);
                    }
                    //TODO: Add other types hsaAdminCommission, hsaCommission
                }
            }
        }
        return path;
    }

    /**
     * Replaces RDN with the path equivalent name.
     */
    private static String replaceRDNWithPathElement(String rdn) {
        return rdn.trim().replace(COUNTRY_RDN + EQUALS, COUNTRIES + FRONT_SLASH)
                .replace(COUNTY_RDN + EQUALS, COUNTIES + FRONT_SLASH)
                .replace(ORGANIZATION_RDN + EQUALS, ORGANIZATIONS + FRONT_SLASH)
                .replace(UNIT_RDN + EQUALS, UNITS + FRONT_SLASH)
                .replace(FUNCTION_RDN + EQUALS, EMPLOYEES + FRONT_SLASH);
    }

}
