package se.inera.hsa.proxy.handler;

import static java.lang.String.format;
import static se.inera.hsa.proxy.AttributeNames.cn;
import static se.inera.hsa.proxy.AttributeNames.dirxEntryUUID;
import static se.inera.hsa.proxy.AttributeNames.hsaCodeEntry;
import static se.inera.hsa.proxy.AttributeNames.hsaCodeName;
import static se.inera.hsa.proxy.AttributeNames.hsaIdentity;
import static se.inera.hsa.proxy.AttributeNames.hsaSyncId;
import static se.inera.hsa.proxy.AttributeNames.objectClass;
import static se.inera.hsa.proxy.ObjectClasses.hsaCodeTable;
import static se.inera.hsa.proxy.ObjectClasses.top;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EQUALS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FRONT_SLASH;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNITS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNIT_RDN;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.protocol.SearchRequestProtocolOp;
import com.unboundid.ldap.protocol.SearchResultDoneProtocolOp;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.schema.AttributeTypeDefinition;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;

import se.inera.hsa.proxy.AttributeNames;
import se.inera.hsa.proxy.exception.SizeLimitException;
import se.inera.hsa.proxy.transformer.ApiSpecifics;
import se.inera.hsa.proxy.transformer.JsonMapTransformer;

/**
 * This class handles the search requests, depending of which baseDN is used the rest-call is made either to organization-api
 * or the codesystem api. The Codesystem parts are a good candidate of refactoring both for cleaner code and performance wise,
 * since there are some code duplication.
 *
 * If/when timelimit is to be implemented it comes with some challenges.
 * - The whole call to proxy should take X seconds.
 * - This class will make call to rest-api. What if that call takes more than X seconds?
 * - The resulting entries still have to be processed but that will take some time too.
 */
public class SearchHandler {
    private static final String META_DATA = "metaData";
    private static final String ECHO_NA = "echo na";
    private static final String EMPTY_STRING = "";
    private static final Control[] EMPTY_CONTROL_ARRAY = new Control[0];

    private static final String GET_PATH = "search/get-path/";
    private static final String ONE_LEVEL_PATH = "search/one/parent-path/";
    private static final String SUB_LEVEL_PATH = "search/sub/parent-path/";

    static final String[] types = {"countries", "counties", "organizations", "units", "functions", "employees"};
    private static final String NAMES = "names";
    @Inject
    private JsonMapTransformer jsonTransformer;

    private List<String> operationalAttrs = Collections.emptyList();

    @PostConstruct
    public void init() {
        InputStream hsaShemaStream = getClass().getClassLoader().getResourceAsStream("hsa-schema.ldif");
        try {
            Schema schema = Schema.getSchema(hsaShemaStream);
            operationalAttrs  = schema.getOperationalAttributeTypes().stream().map(AttributeTypeDefinition::getNameOrOID).map(String::toLowerCase).collect(Collectors.toList());
        } catch (LDIFException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void stop() {
        // not used atm.
    }

    /**
     * Removes attributes from entry which are not in the attributes list.
     * If list contains special ldap-attribute '+' then it is replaced with all operationalAttributes in the list.
     */
    private void removeAttributesNotInList(Entry entry, List<String> attributes)  {
        List<String> attrsToBeRemoved = new ArrayList<>();
        if ( attributes.contains("+") ) {
            attributes.remove("+");
            attributes.addAll(operationalAttrs);
        }

        boolean includeAll = getAllAttributes(attributes);
        for (Attribute attribute : entry.getAttributes()) {
            boolean isOperationalAttr = operationalAttrs.contains(attribute.getName().toLowerCase());
            if ( !attributes.contains(attribute.getName().toLowerCase()) ) {
                if ( !includeAll || isOperationalAttr) {
                    attrsToBeRemoved.add(attribute.getName());
                }
            }
        }
        attrsToBeRemoved.forEach(entry::removeAttribute);
    }


    private boolean getAllAttributes(List<String> attributes) {
        return attributes.isEmpty() || attributes.contains("*");
    }

    /**
     * Handle search request operation. Depending on which searchScope and/or baseDn is used different calls to rest-api is used.
     * If a freetext/detailedSearch Term is the search-api in organiztion-api is called.
     * The reason for that is that if the ldap-filter is simple then you shouldn't get all the entries below the baseDN from REST-api
     * and filter them out here on proxy-side.
     * simple/freetext/detailed search
     */
    public SearchResultDoneProtocolOp handle(int messageID, SearchRequestProtocolOp request,
                              List<Control> controls, LDAPListenerClientConnection connection,
                              WebTarget apiTarget) {
        if ( isBaseDnServices(request) )  {
            return processCodeSystemCall(messageID, request, connection, apiTarget);
        } else {
            Response response;
            String freeText = getFreeTextSearchTerm(request.getFilter());
            if ( freeText != null ) {
                response = callFreeTextRestApi(freeText, apiTarget);
            } else {
                response = callRestApi(request.getBaseDN(), request.getScope(), apiTarget);
            }

            return mapResponse(messageID, controls, connection, response, request);
        }
    }

    /**
     * handles ldap-searches for codesystems. Dependending on which searchScope is used either one codesystem is sent back to client or all codesystems.
     */
    private SearchResultDoneProtocolOp processCodeSystemCall(int messageID, SearchRequestProtocolOp request,
            LDAPListenerClientConnection connection, WebTarget apiTarget) {
        // TODO: Codesystem related code maybe should be refactored into cleaner code, and own class..

        List<Entry> entries = new ArrayList<>();
        Map<String, String> codeSystemIdToDN = CodeSystemContants.CODE_SYSTEM_ID_TO_DN;
        if ( showAllCodeSystemCodes(request) ) {

            Response response = getAllCodeSystems(apiTarget);
            response.bufferEntity();
            String result = response.readEntity(String.class);
            JsonArray array = getJsonArray(result);
            List<String> ids = new ArrayList<>();
            for (JsonValue jsonValue : array) {
                ids.add(jsonValue.asJsonObject().getString("codeSystemId"));
            }

            Collection<String> codeSystemIds = ids;
            try {
                for (String aCodeSystem : codeSystemIds) {
                    entries.add(getCodeSystemEntry(apiTarget, aCodeSystem));
                }
            } catch (Exception e) {
                // TODO Replace with logger output
                e.printStackTrace();
            }
        } else if ( request.getScope().equals(SearchScope.BASE) ) {
            String codeSystemId = codeSystemIdToDN.get(request.getBaseDN());
            try {
                entries.add(getCodeSystemEntry(apiTarget, codeSystemId));
            } catch (Exception e) {
                // TODO Replace with logger output
                e.printStackTrace();
            }
        }
        try {
            processEntrySearchResult(entries, messageID, connection, request);
            return new SearchResultDoneProtocolOp(ResultCode.SUCCESS_INT_VALUE, EMPTY_STRING, EMPTY_STRING,null);
        } catch (SizeLimitException e) {
            return  new SearchResultDoneProtocolOp(ResultCode.SIZE_LIMIT_EXCEEDED_INT_VALUE, EMPTY_STRING, e.getMessage(),null);
        }
    }

    private boolean showAllCodeSystemCodes(SearchRequestProtocolOp request) {
        return request.getBaseDN().equalsIgnoreCase(CodeSystemContants.DN_TO_KODER) && (request.getScope().equals(SearchScope.SUB) || request.getScope().equals(SearchScope.ONE) );
    }


    /**
     * convert a codeSystemEntry to a LDAP-entry
     */
    private Entry getCodeSystemEntry(WebTarget apiTarget, String aCodeSystemId) {
        //Get info about one codeSystem
        System.out.println("getting codeSystemId: "+aCodeSystemId);
        Response response = getACodeSystem(apiTarget, aCodeSystemId);
        response.bufferEntity();

        //TODO fix codeSystem to return an object instead of JSON string?
        String result = response.readEntity(String.class);
        JsonObject codeSystemObject = getJsonObject(result);
        Entry entry = getCodeSystemEntry(aCodeSystemId, codeSystemObject);
        JsonArray codeEntryArray = getHsaCodeEntryValues(apiTarget, aCodeSystemId);
        addHsaCodeEntryValuesToEntry(entry, codeEntryArray);
        return entry;
    }

    /**
     * convert and add a codeEntry value (in a codesystem entry) to a ldap-entry.
     */
    private void addHsaCodeEntryValuesToEntry(Entry entry, JsonArray codeEntryArray) {
        List<String> values  = new ArrayList<>();
        for (JsonValue aHsaCodeEntry : codeEntryArray) {
            JsonObject jsonCodeEntry = aHsaCodeEntry.asJsonObject();
            String codeDescription = jsonCodeEntry.getString("codeDescription", "");
            String extra1 = jsonCodeEntry.getString("extra1", "");
            String extra2 = jsonCodeEntry.getString("extra2", "");
            String extra3 = jsonCodeEntry.getString("extra3", "");
            StringBuilder sb = new StringBuilder();
            sb.append(jsonCodeEntry.getString("code")).append("#")
              .append(jsonCodeEntry.getString("name"));
            if ( codeDescription != null && !codeDescription.toLowerCase().startsWith(ECHO_NA) && !codeDescription.equals("") && !codeDescription.equals(" ")) {
                sb.append("#").append(codeDescription);
            }
            if ( extra1 != null && !extra1.equals("") && !extra1.toLowerCase().startsWith(ECHO_NA) && !codeDescription.equals("") && !codeDescription.equals(" ")) {
                sb.append("#").append(extra1);
            }
            if ( extra2 != null && !extra2.equals("") && !extra2.toLowerCase().startsWith(ECHO_NA) && !codeDescription.equals("") && !codeDescription.equals(" ")) {
                sb.append("#").append(extra2);
            }
            if ( extra3 != null && !extra3.equals("") && !extra3.toLowerCase().startsWith(ECHO_NA) && !codeDescription.equals("") && !codeDescription.equals(" ")) {
                sb.append("#").append(extra3);
            }

            values.add(sb.toString());
        }
        entry.addAttribute(hsaCodeEntry, values);
    }

    /**
     * returns true if basedn in request is descendant of CodeSystem .
     */
    private boolean isBaseDnServices(SearchRequestProtocolOp request)  {
        try {
            return DN.isDescendantOf(request.getBaseDN(), CodeSystemContants.DN_TO_KODER, true);
        } catch (LDAPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    /**
     *  Get all codeSystems from CodeSystem-Api
     */
    private Response getAllCodeSystems(WebTarget apiTarget) {
        Builder getRequest = apiTarget.request();
        getRequest.accept(MediaType.APPLICATION_JSON);
        return getRequest.buildGet().invoke();
    }

    /**
     * Get one CodeSystemEntry from CodeSystem-api
     */
    private Response getACodeSystem(WebTarget apiTarget, String aCodeSystemId) {
        // Get all codeSystems
        WebTarget target = apiTarget.path(aCodeSystemId);
        Builder getRequest = target.request();
        getRequest.accept(MediaType.APPLICATION_JSON);
        return getRequest.buildGet().invoke();
    }

    /**
     * Get codes (codeEntries) for a given codeSystemEntry
     */
    private Response getCodeSystemCodes(WebTarget apiTarget, String aCodeSystemId) {
        WebTarget codeEntryTarget = apiTarget.path(aCodeSystemId).path("codes");
        Builder codeEntryRequest = codeEntryTarget.request();
        codeEntryRequest.accept(MediaType.APPLICATION_JSON);
        return codeEntryRequest.buildGet().invoke();
    }

    /*
     * Generate initial ldapEntry from a json-codesystem entry
     */
    private Entry getCodeSystemEntry(String aCodeSystemId, JsonObject codeSystemObject) {
        String name = codeSystemObject.getString("name");
        Entry entry = new Entry("cn="+name+",dc=koder,dc=Services,c=SE");
        entry.addAttribute(objectClass, hsaCodeTable, top);
        entry.addAttribute(cn, name);
        String hsaCodeNameValue = codeSystemObject.getString(hsaCodeName, null);
        if ( hsaCodeNameValue != null) {
            entry.addAttribute(hsaCodeName, hsaCodeNameValue);
        }
        entry.addAttribute(AttributeNames.hsaMetaIdentity, aCodeSystemId);
        return entry;
    }

    /**
     * getHsaCodeEntryValues from codeSystem-api
     */
    private JsonArray getHsaCodeEntryValues(WebTarget apiTarget, String aCodeSystemId) {
        Response codeEntryResponse = getCodeSystemCodes(apiTarget, aCodeSystemId);
        codeEntryResponse.bufferEntity();
        String codeEntryResult = codeEntryResponse.readEntity(String.class);
        return getJsonArray(codeEntryResult);
    }

    /**
     *  convert String to JsonObject
     */
    private JsonObject getJsonObject(String result) {
        //TODO: Move to json util class?
        JsonReader jsonReader = Json.createReader(new StringReader(result));
        JsonObject object = jsonReader.readObject();
        jsonReader.close();
        return object;
    }

    /**
     *  convert String to JsonArray
     */
    private JsonArray getJsonArray(String result) {
        //TODO: Move to json util class?
        JsonReader jsonReader = Json.createReader(new StringReader(result));
        JsonArray array = jsonReader.readArray();
        jsonReader.close();
        return array;
    }

    /**
     * Converts a LDAP filter to a (detailed search) organization api search-term.
     */
    private String getFreeTextSearchTerm(Filter filter) {
        String uuid = getFilterAttribute(filter, dirxEntryUUID);
        if (uuid != null && uuid.length() > 0)
            return uuid;
        String aHsaSyncId = getFilterAttribute(filter, hsaSyncId);
        if (aHsaSyncId != null && aHsaSyncId.length() > 0)
            return aHsaSyncId;
        String aHsaIdentity = getFilterAttribute(filter, hsaIdentity);
        if (aHsaIdentity != null && aHsaIdentity.length() > 0)
            return aHsaIdentity;

        return null;
    }

    /**
     * Calls (detailed-) search endpoint in organization-api
     */
   private Response callFreeTextRestApi(String freeTextFilter, WebTarget apiTarget) {
       freeTextFilter=freeTextFilter.replaceAll("-", "\\\\-");
       WebTarget target = apiTarget.path("search/free-text/").path(freeTextFilter);
       Builder getRequest = target.request();
       getRequest.accept(MediaType.APPLICATION_JSON);
       System.out.println("Calling api FREE-TEXT : " + target.getUri() + " incoming free-text = " + freeTextFilter);
       return getRequest.buildGet().invoke();
    }

   public static String transposeDnToApiSearchPath(String requestDn, SearchScope searchScope, WebTarget baseApiTarget, boolean useNames) {
       if ( isDnToCommonName(requestDn) && (searchScope.intValue() == SearchScope.SUB.intValue() || searchScope.intValue() == SearchScope.ONE.intValue()) ) {
           String tmpPath = getPathToDn(requestDn, baseApiTarget);
           if ( tmpPath != null && !EMPTY_STRING.equals(tmpPath)) {
               return tmpPath;
           }
       }

       String dn = requestDn;

       dn=dn.replaceAll("(?i)"+Pattern.quote("c=se"), "c=Sverige");
       List<String> list = Arrays.asList(dn.split(ApiSpecifics.COMMA));
       List<String> transposedList = list.stream().map(e -> replaceRDNWithPathElement(e, useNames)).collect(Collectors.toList());
       Collections.reverse(transposedList);
       return String.join(ApiSpecifics.FRONT_SLASH, transposedList);
   }

    private static String replaceRDNWithPathElement(String rdn, boolean useNames) {
        return rdn.trim().replace(COUNTRY_RDN + EQUALS, COUNTRIES + FRONT_SLASH)
                         .replace(COUNTY_RDN + EQUALS, COUNTIES + FRONT_SLASH)
                         .replace(ORGANIZATION_RDN + EQUALS, ORGANIZATIONS + FRONT_SLASH)
                         .replace(UNIT_RDN + EQUALS, UNITS + FRONT_SLASH)
                         .replace(FUNCTION_RDN + EQUALS, (useNames ? NAMES : FUNCTIONS) + FRONT_SLASH);
    }

    public static String transposeDnToApiSearchPathNames(String requestDn) {
        String dn = requestDn;

        dn=dn.replaceAll("(?i)"+Pattern.quote("c=se"), "c=Sverige");
        List<String> list = Arrays.asList(dn.split(ApiSpecifics.COMMA));
        List<String> transposedList = list.stream().map(SearchHandler::replaceRDNWithPathElementNames).collect(Collectors.toList());
        Collections.reverse(transposedList);
        return String.join(ApiSpecifics.FRONT_SLASH, transposedList);
    }

    private static String replaceRDNWithPathElementNames(String rdn) {
        return rdn.trim().replace(COUNTRY_RDN + EQUALS, COUNTRIES + FRONT_SLASH)
                .replace(COUNTY_RDN + EQUALS, COUNTIES + FRONT_SLASH)
                .replace(ORGANIZATION_RDN + EQUALS, ORGANIZATIONS + FRONT_SLASH)
                .replace(UNIT_RDN + EQUALS, UNITS + FRONT_SLASH)
                .replace(FUNCTION_RDN + EQUALS, NAMES + FRONT_SLASH);
    }

    static Response callRestApi(String baseDn, SearchScope searchScope, WebTarget baseApiTarget) {
        String path;

        WebTarget target = baseApiTarget;
        switch (searchScope.intValue()) {
        case SearchScope.BASE_INT_VALUE:
            target = target.path(GET_PATH);
            path = transposeDnToApiSearchPath(baseDn, searchScope, baseApiTarget, true);
            break;
        case SearchScope.ONE_INT_VALUE:
            target = target.path(ONE_LEVEL_PATH);
            path = transposeDnToApiSearchPath(baseDn, searchScope, baseApiTarget, false);
            break;
        case SearchScope.SUB_INT_VALUE:
            target = target.path(SUB_LEVEL_PATH);
            path = transposeDnToApiSearchPath(baseDn, searchScope, baseApiTarget, false);
            break;
        default:
            path = transposeDnToApiSearchPath(baseDn, searchScope, baseApiTarget, false);
            break;
        }

        target = target.path(path);

        Builder getRequest = target.request();
        getRequest.accept(MediaType.APPLICATION_JSON);
        System.out.println("Calling api GET Path: " + target.getUri() + " incoming dn = " + baseDn);
        return getRequest.buildGet().invoke();
    }

    private static boolean isDnToCommonName(String baseDn) {
        return baseDn.toLowerCase().startsWith("cn=");
    }

    @SuppressWarnings("unchecked")
    public static String getPathToDn(String dn, WebTarget target) {
        String path = transposeDnToApiSearchPath(dn, SearchScope.BASE, target, true);
        WebTarget base = target.path(GET_PATH);
        base = base.path(path);
        Builder getRequest = base.request();
        getRequest.accept(MediaType.APPLICATION_JSON);
        System.out.println("Calling api GET Path: " + base.getUri());
        Response invoke = getRequest.buildGet().invoke();
        if (invoke.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
            invoke.bufferEntity();
            Map<String,Object> tmp = invoke.readEntity(Map.class);
            if ( tmp != null ) {
                for (String type : types) {
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tmp.get(type);
                    if ( list != null ) {
                        for (Map<String, Object> map : list) {
                            if ( map.containsKey(META_DATA) )  {
                                return (String) ((Map<String, Object>) map.get(META_DATA)).get("path");
                            }
                        }
                    }
                }
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Map from REST-restResponse to LDAP-restResponse
     * Make sure errors are mapped etc
     * Make sure api returns codes in body for clarification.
     *
     * @param dn dn from request
     * @param restResponse rest restResponse
     * @param request
     * @return ldap restResponse
     */
    private SearchResultDoneProtocolOp mapResponse(int messageID, List<Control> controls,
            LDAPListenerClientConnection connection, Response restResponse, SearchRequestProtocolOp request) {

        if (restResponse.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {

            try {
                getSearchResponse(messageID, connection, restResponse, request);
            } catch (SizeLimitException e) {
               return  new SearchResultDoneProtocolOp(ResultCode.SIZE_LIMIT_EXCEEDED_INT_VALUE, EMPTY_STRING, e.getMessage(),
                       null);
            }

            return new SearchResultDoneProtocolOp(ResultCode.SUCCESS_INT_VALUE, EMPTY_STRING, EMPTY_STRING,
                    null);
        }

        String errorJson = restResponse.readEntity(String.class);
        int responseCode = ResultCodeMapper.getLdapResultCode(errorJson, restResponse.getStatus());
        String errorMessage = ResultCodeMapper.getErrorMessage(errorJson);

        return new SearchResultDoneProtocolOp(responseCode, request.getBaseDN(), errorMessage, null);
    }

    @SuppressWarnings("unchecked")
    private void getSearchResponse(int messageID, LDAPListenerClientConnection connection, Response restResponse,
            SearchRequestProtocolOp request) throws SizeLimitException {
        restResponse.bufferEntity();
        List<Map<String,Object>> organizationEntities = new ArrayList<>();
        Map<String,Object> tmp = restResponse.readEntity(Map.class);
            for (String type : types) {
                organizationEntities.addAll((List<Map<String, Object>>) tmp.get(type));
            }
            processSearchResult(organizationEntities, messageID, connection, request);
    }


    /**
     * This method is responsible to
     *  - looping through the results
     *  - converting them to LDAP-entries
     *  - comparing the entries to the ldap-filter
     *  - if mathing the filter send them back to client
     *  - keeps track of how many entries have been returned if need to throw sizeLimitExceeded.
     *
     *  Since this is a variant of below method, this could be refactored.
     */
    private void processSearchResult(List<Map<String,Object>> listOfEntries, int messageID, LDAPListenerClientConnection connection,
                                     SearchRequestProtocolOp request) throws SizeLimitException {

        if (listOfEntries == null || listOfEntries.isEmpty()) {
            return;
        }

        int sizeLimit = request.getSizeLimit() <=0 ? listOfEntries.size() : request.getSizeLimit();

        //TODO: HOSP, behörighet

        int sentEntries=0;
        for (Map<String, Object> anEntry : listOfEntries) {
           Optional<Entry> transformToEntry = jsonTransformer.transformToEntry(anEntry);
           if ( !transformToEntry.isPresent()) {
               continue;
           }

           if ( sentEntries >= sizeLimit) {
               throw new SizeLimitException(format("Search operation hit size limit. Found %d entries.", sentEntries));
           }

           Entry entry = transformToEntry.get();
           if ( matchesFilterBaseAndScope(request, entry)) {
               removeAttributesNotInList(entry, request.getAttributes().stream().map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new)));

               sendSearchResultEntryToClient(messageID, connection, entry, request);
               sentEntries++;
           }
        }
    }

    /**
     * This method is responsible to
     *  - looping through the results
     *  - converting them to LDAP-entries
     *  - comparing the entries to the ldap-filter
     *  - if mathing the filter send them back to client
     *  - keeps track of how many entries have been returned if need to throw sizeLimitExceeded.
     *
     *  Since this is a variant of above method, this could be refactored.
     */
    private void processEntrySearchResult(List<Entry> listOfEntries, int messageID, LDAPListenerClientConnection connection,
            SearchRequestProtocolOp request) throws SizeLimitException {

        if (listOfEntries == null || listOfEntries.isEmpty()) {
            return;
        }

        int sizeLimit = request.getSizeLimit() <=0 ? listOfEntries.size() : request.getSizeLimit();

        //TODO: HOSP, kodverk, employee, behörighet

        int sentEntries=0;
        for (Entry anEntry : listOfEntries) {
            if ( sentEntries >= sizeLimit) {
                throw new SizeLimitException(format("Search operation hit size limit. Found %d entries.", sentEntries));
            }

            if ( matchesFilterBaseAndScope(request, anEntry)) {
                removeAttributesNotInList(anEntry, request.getAttributes().stream().map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new)));

                sendSearchResultEntryToClient(messageID, connection, anEntry, request);
                sentEntries++;
            }
        }
    }


    private boolean matchesFilterBaseAndScope(SearchRequestProtocolOp request, Entry entry) {
        try {
            boolean matchesBaseAndScope = entry.matchesBaseAndScope(request.getBaseDN(), request.getScope());
            boolean matchesEntry = request.getFilter().matchesEntry(entry);

           return matchesBaseAndScope && matchesEntry;
        } catch (LDAPException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void sendSearchResultEntryToClient(int messageID, LDAPListenerClientConnection connection, Entry entry, SearchRequestProtocolOp request) {
        try {
            removeAttributesNotInList(entry, request.getAttributes().stream().map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new)));
            connection.sendSearchResultEntry(messageID, entry, EMPTY_CONTROL_ARRAY);
        } catch (LDAPException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public Optional<List<String>> getCountries(WebTarget orgApi) {
        WebTarget target = orgApi.path("search/free-text/").path("@type:(Country)");
        List<String> countries = new ArrayList<>();

        Builder targetRequest = target.request();
        targetRequest.accept(MediaType.APPLICATION_JSON);
        Response targetResponse = targetRequest.get();

        Response.Status.Family orgStatusFamily = targetResponse.getStatusInfo().getFamily();
        if (orgStatusFamily != Family.SUCCESSFUL) {
            //TODO LOg?
            return Optional.empty();
        } else {
            targetResponse.bufferEntity();
            Map<String,Object> results;
            results = targetResponse.readEntity(Map.class);

            List<Map<String,Object>> tmp = (List<Map<String,Object>>) results.get("countries");
            tmp.forEach( map -> {
                String countryPathToDn = countryPathToDn((String) ((Map<String,Object>) map.get(META_DATA)).get("path"));
                countries.add(countryPathToDn);
            });
        }
		return Optional.of(countries);
    }

    private static String countryPathToDn(String path) {
        return path.trim().replace(FRONT_SLASH, EQUALS)
                          .replace(COUNTRIES, COUNTRY_RDN)
                          .replaceAll("(?i)Sverige", "SE");
    }

    /**
     * Get a "detailed search" search string which can be sent to search api in organization api.
     */
    protected String getFilterAttribute(Filter filter, String attributeName) {
        if(filter != null)
        {
            if(filter.getAttributeName() != null && filter.getAttributeName().equalsIgnoreCase(attributeName))
            {
                //(attribute=value)
                if ( filter.getFilterType() == Filter.FILTER_TYPE_EQUALITY) {
                    if (attributeName.equalsIgnoreCase(hsaIdentity)) {
                        return new StringBuilder("@hsaIdentity:(").append(filter.getAssertionValue()).append(")").toString();
                    } else if (attributeName.equalsIgnoreCase(hsaSyncId)) {
                        return new StringBuilder("@hsaSyncId:(").append(filter.getAssertionValue()).append(")").toString();
                    } else if (attributeName.equalsIgnoreCase(dirxEntryUUID)) {
                        return new StringBuilder("@uuid:(").append(filter.getAssertionValue()).append(")").toString();
                    }
                }
            }
            if ( filter.getFilterType()==Filter.FILTER_TYPE_AND ) {
                //(&((attribute1=value1)(attribute2=value2))
                Optional<String> findFirst = Arrays.stream(filter.getComponents())
                        .map(f -> getFilterAttribute(f, attributeName))
                        .filter( Objects::nonNull )
                        .findFirst();

                return findFirst.orElse(null);
            }
        }
        return null;
    }


}
