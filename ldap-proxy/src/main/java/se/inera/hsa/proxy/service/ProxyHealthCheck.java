package se.inera.hsa.proxy.service;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.enterprise.context.ApplicationScoped;

@Health
@ApplicationScoped
public class ProxyHealthCheck implements HealthCheck  {

    //@Inject OrganizationClient organizationClient;
    //@Inject EmployeeClient employeeClient;
    //@Inject LdapClient ldapClient;

    /**
     * @Todo Do some factual verification that we are up
     * 1. For example verify that our port is up, some ldap-ping
     * 2. Verify that we can reach our API's
     */
    @Override
    public HealthCheckResponse call() {
        /**
         * organizationClient.ping();
         * employeeClient.ping();
         * ldapClient.ping();
         */
        return HealthCheckResponse.named("proxy").up().build();
    }
}
