package se.inera.hsa.proxy.transformer;

import static se.inera.hsa.proxy.AttributeNames.businessClassificationCode;
import static se.inera.hsa.proxy.AttributeNames.businessClassificationName;
import static se.inera.hsa.proxy.AttributeNames.businessClassificationType;
import static se.inera.hsa.proxy.AttributeNames.createTimeStamp;
import static se.inera.hsa.proxy.AttributeNames.creatorsName;
import static se.inera.hsa.proxy.AttributeNames.dirxEntryUUID;
import static se.inera.hsa.proxy.AttributeNames.geographicalCoordinates;
import static se.inera.hsa.proxy.AttributeNames.geographicalCoordinatesSweref99TM;
import static se.inera.hsa.proxy.AttributeNames.hsaJpegLogotype;
import static se.inera.hsa.proxy.AttributeNames.jpegPhoto;
import static se.inera.hsa.proxy.AttributeNames.l;
import static se.inera.hsa.proxy.AttributeNames.modifiersName;
import static se.inera.hsa.proxy.AttributeNames.modifyTimestamp;
import static se.inera.hsa.proxy.AttributeNames.numAllSubordinates;
import static se.inera.hsa.proxy.AttributeNames.numSubordinates;
import static se.inera.hsa.proxy.AttributeNames.objectClass;
import static se.inera.hsa.proxy.AttributeNames.postalAddress;
import static se.inera.hsa.proxy.AttributeNames.userCertificate;
import static se.inera.hsa.proxy.AttributeNames.userPassword;
import static se.inera.hsa.proxy.ObjectClasses.HSAOrganizationExtension;
import static se.inera.hsa.proxy.ObjectClasses.HSAPersonExtension;
import static se.inera.hsa.proxy.ObjectClasses.HSARoleExtension;
import static se.inera.hsa.proxy.ObjectClasses.InetOrgPerson;
import static se.inera.hsa.proxy.ObjectClasses.hsaHealthCareProvider;
import static se.inera.hsa.proxy.ObjectClasses.hsaHealthCareUnit;
import static se.inera.hsa.proxy.ObjectClasses.hsaLocalityExtension;
import static se.inera.hsa.proxy.ObjectClasses.locality;
import static se.inera.hsa.proxy.ObjectClasses.organization;
import static se.inera.hsa.proxy.ObjectClasses.organizationalPerson;
import static se.inera.hsa.proxy.ObjectClasses.organizationalRole;
import static se.inera.hsa.proxy.ObjectClasses.organizationalUnit;
import static se.inera.hsa.proxy.ObjectClasses.person;
import static se.inera.hsa.proxy.ObjectClasses.pkiUser;
import static se.inera.hsa.proxy.ObjectClasses.top;
import static se.inera.hsa.proxy.transformer.JsonConstants.ADDRESS_LINE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COMMENT;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_RT90_X_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_RT90_Y_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_SWEREF_E_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_SWEREF_N_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.DN;
import static se.inera.hsa.proxy.transformer.JsonConstants.DOLLAR_SIGN;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_DATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_DAY;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_TIME;
import static se.inera.hsa.proxy.transformer.JsonConstants.HASH;
import static se.inera.hsa.proxy.transformer.JsonConstants.HOURS;
import static se.inera.hsa.proxy.transformer.JsonConstants.HYPHEN;
import static se.inera.hsa.proxy.transformer.JsonConstants.OBJECT_CLASS;
import static se.inera.hsa.proxy.transformer.JsonConstants.PATH;
import static se.inera.hsa.proxy.transformer.JsonConstants.TEMP_DN;
import static se.inera.hsa.proxy.transformer.JsonConstants.TIME_SPAN;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_DATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_DAY;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_TIME;
import static se.inera.hsa.proxy.transformer.JsonConstants.TYPE;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPException;

import se.inera.hsa.proxy.Helper;

/**
 * The purpose of this class is to transform a json object to LDAP-Entry.
 */
public class JsonMapTransformer implements JsonTransformer {

    /**
     * This method transform the incoming map (json) to an LDAP entry.
     */
    @Override
    public Optional<Entry> transformToEntry(Map<String,Object> json) {

        if (json != null && !json.isEmpty()) {
            Entry jsonToEntry = jsonToEntry(json);
            addLdapSpecifiObjectClasses(jsonToEntry);
            return Optional.of(jsonToEntry);
        }

        return Optional.empty();
    }


    private Entry jsonToEntry(Map<String, Object> json) {
        Entry entry = new Entry(TEMP_DN);

        for (java.util.Map.Entry<String, Object> mapEntry : json.entrySet()) {

            String jsonAttrName = mapEntry.getKey();

            Object value = mapEntry.getValue();

            if (jsonAttrName.equalsIgnoreCase(DN)) {
                entry.setDN(value.toString());
            } else {
                addAttributeToEntry(entry, jsonAttrName, value);
            }
        }
        entry.removeAttribute("name");
        entry.removeAttribute("type");
        entry.removeAttribute("uuid");
        return entry;
    }



    /**
     * Add default objectclasses that hsa LDAP entries have to the object depending of which type of object it is.
     */
    private void addLdapSpecifiObjectClasses(Entry entry) {
        if (!entry.hasObjectClass(top)) {
            entry.addAttribute(OBJECT_CLASS, top);
        }

        if (entry.hasObjectClass(person) || entry.hasObjectClass("employee")) {
            entry.addAttribute(OBJECT_CLASS, HSAPersonExtension, InetOrgPerson, organizationalPerson, pkiUser, person);
            entry.removeAttributeValue(OBJECT_CLASS, "employee");
        } else if (entry.hasObjectClass(organization) || entry.hasObjectClass(organizationalUnit)) {
            entry.addAttribute(OBJECT_CLASS, HSAOrganizationExtension);
        } else if (entry.hasObjectClass(organizationalRole)) {
            entry.addAttribute(OBJECT_CLASS, HSARoleExtension, pkiUser);
        } else if (entry.hasObjectClass(locality)) {
            entry.addAttribute(OBJECT_CLASS, hsaLocalityExtension);
        } else if (entry.hasObjectClass("county")) {
            entry.addAttribute(OBJECT_CLASS, hsaLocalityExtension);
            entry.addAttribute(OBJECT_CLASS, locality);
            entry.removeAttributeValue(OBJECT_CLASS, "county");
        }
    }

    /**
     * Add an attribute to an LDAP entry. Depending of which type of attribute it is, the Object is cast and transformed to its correct
     */
    @SuppressWarnings("unchecked")
    private void addAttributeToEntry(Entry entry, String jsonAttrName, Object value) {
        if ( value instanceof BigDecimal ) {
            String number = ((BigDecimal) value).toPlainString();
            if (number != null) {
                entry.addAttribute(jsonAttrName, number);
            }
        } else if( value instanceof String ) {
            addStringAttribute(entry, jsonAttrName, value);
        } else if ( value instanceof Map ) {
            addObjectToEntry(entry, jsonAttrName, value);
        }
        else if (value instanceof List ) { // array?
            List<Object> values = (List<Object>) value;
            for (Object object : values) {
                addAttributeToEntry(entry, jsonAttrName, object);
            }
        }
        else if (value instanceof Boolean ) { // array?
            Boolean aValue = (Boolean) value;
            if (jsonAttrName.equals("hsaHealthCareUnit") && aValue) {
                entry.addAttribute(objectClass, hsaHealthCareUnit);
            } else if (jsonAttrName.equals("hsaHealthCareProvider") && aValue) {
                entry.addAttribute(objectClass, hsaHealthCareProvider);
            }
        }
    }

    /**
     * Transforms an json object to ldap format and adds it to the ldap Entry.
     */
    @SuppressWarnings("unchecked")
    private void addObjectToEntry(Entry entry, String jsonAttrName, Object value) {
        Map<String,Object> map = (Map<String,Object>) value;
        if (jsonAttrName.equalsIgnoreCase(businessClassificationType)) {
            entry.addAttribute(businessClassificationName, map.get(businessClassificationName).toString());
            entry.addAttribute(businessClassificationCode, map.get(businessClassificationCode).toString());
        } else if (jsonAttrName.equalsIgnoreCase("metadata") ) {
//              "metaData": {
   //                "createTimestamp": "2018-11-19T12:24:44.231Z[UTC]",
   //                "creatorPath": "path/to/creator",
   //                "creatorUUID": "31cac4c0-0115-4f9e-abb0-1d441f66c787",
   //                "numAllSubordinates": 0,
   //                "numSubordinates": 0,
   //                "path": "path/to/resource"
//                  },
            entry.addAttribute(createTimeStamp,convertTimestamp(map.get("createTimestamp").toString()));
            entry.addAttribute(creatorsName,Helper.transposePathToDnV3(map.get("creatorPath").toString()));
            if ( map.get("modifierPath") != null ) {
                entry.addAttribute(modifiersName,Helper.transposePathToDnV3(map.get("modifierPath").toString()));
            }
            if ( map.get("modifyTimestamp") != null ) {
                entry.addAttribute(modifyTimestamp,convertTimestamp(map.get("modifyTimestamp").toString()));
            }
            entry.addAttribute(numSubordinates, map.get("numSubordinates").toString());
            entry.addAttribute(numAllSubordinates, map.get("numAllSubordinates").toString());
            String string = map.get(PATH).toString();
            entry.setDN(Helper.transposePathToDnV3(string));
            try {
                entry.addAttribute(entry.getRDN().getAttributeNames()[0], entry.getRDN().getAttributeValues()[0]);
                entry.removeAttribute("name");
            } catch (LDAPException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else if (jsonAttrName.equalsIgnoreCase(geographicalCoordinates)) {
            entry.addAttribute(geographicalCoordinates, transformToGeographicalCoordinatesRT90(map));
        } else if (jsonAttrName.equalsIgnoreCase(geographicalCoordinatesSweref99TM)) {
            entry.addAttribute(geographicalCoordinatesSweref99TM, transformToGeographicalCoordinatesSWEREF99(map));
        } else if (jsonAttrName.equalsIgnoreCase(postalAddress)) {
            entry.addAttribute(postalAddress, transformToPostalAddress(map));
        } else if (isTimeSpan(jsonAttrName)) {
            entry.addAttribute(jsonAttrName, transformToHours(map));
        }
    }

    /**
     * Adds a string attribute to the LDAP entry.
     */
    private void addStringAttribute(Entry entry, String jsonAttrName, Object value) {
        String string = ((String) value);
        if (!string.isEmpty()) {
            if (jsonAttrName.equals("uuid")) {
                entry.setAttribute(dirxEntryUUID, string);
                entry.removeAttribute("uuid");
            } else if (jsonAttrName.equalsIgnoreCase(TYPE)) {
                entry.addAttribute(OBJECT_CLASS, ((String) value).toLowerCase().replace("unit", organizationalUnit).replace("function", organizationalRole));
            } else if (jsonAttrName.equals("localityName")) {
                entry.setAttribute(l, string);
                entry.removeAttribute("localityName");
            } else if (jsonAttrName.equals(jpegPhoto) || jsonAttrName.equals(hsaJpegLogotype)  || jsonAttrName.equals(userCertificate)) {
                entry.addAttribute(jsonAttrName,Base64.getDecoder().decode(string));
            } else if (jsonAttrName.equals(userPassword)) {
                // TODO: Remove this if in the future
                entry.addAttribute(jsonAttrName,Base64.getDecoder().decode(string));
            } else {
                entry.addAttribute(jsonAttrName, string);
            }
        }
    }

    /**
     * Converts detault iso format stringt to ldap format.
     * @param timeStamp
     * @return
     */
    private String convertTimestamp(String timeStamp) {
        return ZonedDateTime.parse(timeStamp).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss'Z'"));
    }

    /**
     * transoform a GeographicalCoordinates RT90 json format to ldap-format.
     */
    @SuppressWarnings("unchecked")
    String transformToGeographicalCoordinatesRT90(Map<String,Object> map) {

        if (map == null)
            return null;

        Map<String,Object> coord = (Map<String, Object>) map.get(COORDINATE);

        return COORDINATE_RT90_X_KEY.toUpperCase() +
                ":" +
                coord.get(COORDINATE_RT90_X_KEY) +
                "," +
                COORDINATE_RT90_Y_KEY.toUpperCase() +
                ":" +
                coord.get(COORDINATE_RT90_Y_KEY);

    }

    /**
     * transoform a GeographicalCoordinates SWEREF99 json format to ldap-format.
     */
    @SuppressWarnings("unchecked")
    String transformToGeographicalCoordinatesSWEREF99(Map<String,Object> map) {

    	if (map == null)
    		return null;

    	Map<String,Object> coord = (Map<String,Object>)map.get(COORDINATE);

    	return COORDINATE_SWEREF_N_KEY +
    			":" +
    			coord.get(COORDINATE_SWEREF_N_KEY) +
    			"," +
    			COORDINATE_SWEREF_E_KEY +
    			":" +
    			coord.get(COORDINATE_SWEREF_E_KEY);

    }

    /**
     * Returns true if attributename ends with 'hours'.
     */
    boolean isTimeSpan(String jsonAttrName) {
        if (jsonAttrName == null || jsonAttrName.trim().isEmpty()) {
            return false;
        }
        return jsonAttrName.toLowerCase().endsWith(HOURS);
    }

    /**
     * Transform a json Timespan format to ldap-format.
     */
    @SuppressWarnings("unchecked")
    String transformToHours(Map<String,Object> timeSpanJson) {

        if (timeSpanJson == null)
            return null;

        Map<String,Object> obj = (Map<String,Object>) timeSpanJson.get(TIME_SPAN);

        StringBuilder stringBuilder = new StringBuilder()
                .append(obj.get(FROM_DAY))
                .append(HYPHEN).append(obj.get(TO_DAY))
                .append(HASH).append(obj.get(FROM_TIME))
                .append(HASH).append(obj.get(TO_TIME));

        if (obj.get(COMMENT) != null)
            stringBuilder.append(HASH).append(obj.get(COMMENT));

        if (obj.get(FROM_DATE) != null)
            stringBuilder.append(HASH).append(obj.get(FROM_DATE));

        if (obj.get(TO_DATE) != null)
            stringBuilder.append(HASH).append(obj.get(TO_DATE));

        return stringBuilder.toString();

    }

    /**
     * Transforms a postalAddress json format to its ldap equivalent.
     */
    @SuppressWarnings("unchecked")
    String transformToPostalAddress(Map<String,Object> postalAddress) {

        if (postalAddress == null)
            return null;

        List<Object> lines = (List<Object>) postalAddress.get(ADDRESS_LINE);

        StringBuilder sb = new StringBuilder();

        lines.forEach(i -> sb.append(i).append(DOLLAR_SIGN));

        return sb.deleteCharAt(sb.length() - 1).toString();
    }
}
