package se.inera.hsa.proxy.transformer;

import java.util.Collection;
import java.util.List;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Modification;

/**
 * LDAP to json transformer interface.
 */
public interface LdapTransformer {

    String transformToJson(String dn, Collection<Attribute> attributes);

    String transformMoveToJson(String newPath);

    String transformToUpdateJson(String dn, Collection<Attribute> attributes);
    String transformToPatchJson(String dn, List<Modification> modifications);
}
