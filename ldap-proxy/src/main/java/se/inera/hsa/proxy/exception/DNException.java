package se.inera.hsa.proxy.exception;

public class DNException extends Exception{

    public DNException() {
        super();
    }

    public DNException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DNException(String message, Throwable cause) {
        super(message, cause);
    }

    public DNException(String message) {
        super(message);
    }

    public DNException(Throwable cause) {
        super(cause);
    }

}
