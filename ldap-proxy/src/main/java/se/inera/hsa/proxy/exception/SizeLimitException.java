package se.inera.hsa.proxy.exception;

public class SizeLimitException extends Exception {

    public SizeLimitException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SizeLimitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public SizeLimitException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public SizeLimitException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SizeLimitException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
