package se.inera.hsa.proxy;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.protocol.AddRequestProtocolOp;
import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.protocol.BindRequestProtocolOp;
import com.unboundid.ldap.protocol.BindResponseProtocolOp;
import com.unboundid.ldap.protocol.CompareRequestProtocolOp;
import com.unboundid.ldap.protocol.DeleteRequestProtocolOp;
import com.unboundid.ldap.protocol.DeleteResponseProtocolOp;
import com.unboundid.ldap.protocol.ExtendedRequestProtocolOp;
import com.unboundid.ldap.protocol.LDAPMessage;
import com.unboundid.ldap.protocol.ModifyDNRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNResponseProtocolOp;
import com.unboundid.ldap.protocol.ModifyRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyResponseProtocolOp;
import com.unboundid.ldap.protocol.SearchRequestProtocolOp;
import com.unboundid.ldap.protocol.SearchResultDoneProtocolOp;
import com.unboundid.ldap.protocol.UnbindRequestProtocolOp;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.hsa.proxy.handler.AddHandler;
import se.inera.hsa.proxy.handler.DeleteHandler;
import se.inera.hsa.proxy.handler.ModifyDNHandler;
import se.inera.hsa.proxy.handler.ModifyHandler;
import se.inera.hsa.proxy.handler.SearchHandler;


/**
 * This class handled all the requests. It was later split up into smaller classes which only handled one type of request.
 * Maybe this class should be refactored and merged with {@link ProxyLDAPListenerRequestHandler}?
 *
 */
public class RequestProcessorImpl implements RequestProcessor {
    WebTarget restApiTargetv0_3;

    WebTarget codeSystemTarget;

    /**
     * The URL for organization v0.3 endpoint.
     */
    @Inject
    @ConfigProperty(name = "organization.uri.v03")
    private String restApiBaseUrlv0_3;

    /**
     * Endpoint for codesystems.
     */
    @Inject
    @ConfigProperty(name = "codesystem.uri")
    private String codeSystemUri;

    @Inject
    private AddHandler addHandler;

    @Inject
    private DeleteHandler deleteHandler;

    @Inject
    private ModifyHandler modifyHandler;

    @Inject
    private SearchHandler searchHandler;

    @Inject
    private ModifyDNHandler modifyDNHandler;

    private Client client;

    @PostConstruct
    public void init() {
        client = ClientBuilder.newClient();
        this.restApiTargetv0_3 = client.target(restApiBaseUrlv0_3);
        this.codeSystemTarget = client.target(codeSystemUri);

        //TODO: if timeout should be used. it seems to be in the client not webtarget.
        System.out.println("RequestProcessorImpl PostConstruct");
    }

    @PreDestroy
    public void stop() {
        if ( client != null ) {
            client.close();
            System.out.println("Client closed");
        }
        System.out.println("RequestProcessorImpl PreDestroy");
    }


    @Override
    public LDAPMessage processAddRequest(int messageID, AddRequestProtocolOp request, List<Control> controls) {
        try {
            AddResponseProtocolOp addResponse = addHandler.handle(request, restApiTargetv0_3);
            System.out.println(addResponse);
            return new LDAPMessage(messageID, addResponse, controls);
        } catch (Exception e) {
            AddResponseProtocolOp addResponse = new AddResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, "" ,"Unknown Ldap-proxy error. Can't handle request. " + e, null);
            System.out.println(addResponse + " EXCEPTION "+ e);
            return new LDAPMessage(messageID, addResponse, controls);
        }
    }


    @Override
    public LDAPMessage processCompareRequest(int messageID, CompareRequestProtocolOp request, List<Control> controls) {
        log(messageID, "processCompareRequest", null, null);
        return null;
    }

    @Override
    public LDAPMessage processDeleteRequest(int messageID, DeleteRequestProtocolOp request, List<Control> controls) {
        try {
            DeleteResponseProtocolOp deleteResponse = deleteHandler.handle(request, restApiTargetv0_3);
            return new LDAPMessage(messageID, deleteResponse, controls);
        } catch (Exception e) {
            DeleteResponseProtocolOp deleteResponse = new DeleteResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, "" ,"Unknown Ldap-proxy error." + e, null);
            return new LDAPMessage(messageID, deleteResponse, controls);
        }
    }

    @Override
    public LDAPMessage processExtendedRequest(int messageID, ExtendedRequestProtocolOp request, List<Control> controls) {
        log(messageID, "processExtendedRequest", null, null);
        return null;
    }

    @Override
    public LDAPMessage processModifyRequest(int messageID, ModifyRequestProtocolOp request, List<Control> controls) {
        try {
            ModifyResponseProtocolOp modifyResponse = modifyHandler.handle(request, restApiTargetv0_3);
            return new LDAPMessage(messageID, modifyResponse, controls);
        } catch (Exception e) {
            ModifyResponseProtocolOp modifyResponse = new ModifyResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, "" ,"Unknown Ldap-proxy error." + e, null);
            return new LDAPMessage(messageID, modifyResponse, controls);
        }
    }

    @Override
    public LDAPMessage processModifyDNRequest(int messageID, ModifyDNRequestProtocolOp request, List<Control> controls) {
        try {
            ModifyDNResponseProtocolOp modifyResponse = modifyDNHandler.handle(request, restApiTargetv0_3);
            return new LDAPMessage(messageID, modifyResponse, controls);
        } catch (Exception e) {
            ModifyDNResponseProtocolOp modifyResponse = new ModifyDNResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, "" ,"Unknown Ldap-proxy error." + e, null);
            return new LDAPMessage(messageID, modifyResponse, controls);
        }
    }

    @Override
    public LDAPMessage processSearchRequest(int messageID, SearchRequestProtocolOp request, List<Control> controls,
                                            LDAPListenerClientConnection connection) {
        try {
            WebTarget target = restApiTargetv0_3;
            if ( DN.isDescendantOf(request.getBaseDN(), "dc=koder,dc=Services,c=SE", true) )  {
                target = codeSystemTarget;
            }

            SearchResultDoneProtocolOp searchResponse = searchHandler.handle(messageID, request, controls, connection, target);
            return new LDAPMessage(messageID, searchResponse, controls);
        } catch (Exception e) {
            SearchResultDoneProtocolOp searchResponse = new SearchResultDoneProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, "" ,"Unknown Ldap-proxy error." + e, null);
            return new LDAPMessage(messageID, searchResponse, controls);
        }
    }

    @Override
    public LDAPMessage processBindRequest(int messageID, BindRequestProtocolOp request, List<Control> controls) {
        log(messageID, "processBindRequest", null, null);
        LDAPMessage message;

//    	if ( // Kontrollera lösenordet, ifall fel ) {
//    		final BindResponseProtocolOp protocolOp = new BindResponseProtocolOp(ResultCode.INVALID_CREDENTIALS_INT_VALUE,
//          null, "Invalid credentials!", null, null);
//    		message = new LDAPMessage(messageID, protocolOp, controls);
//        } else {
        BindResponseProtocolOp responseProtocolOp = new BindResponseProtocolOp(0, null, null, null, null);
        message = new LDAPMessage(messageID, responseProtocolOp);
//        }
        return message;
    }

    @Override
    public LDAPMessage processUnbindRequest(int messageID, UnbindRequestProtocolOp request, List<Control> controls) {
        log(messageID, "processUnbindRequest", null, null);
        return null;
    }

    @SuppressWarnings("unchecked")
    private void log(int messageId, String methodName, String additionalInfoKey, String additionalInfoValue) {
        System.out.println("proxy log id:" + messageId + " type:" + methodName + "additionalInfoKey:" + additionalInfoKey + " additionalInfoValue:" + additionalInfoValue);
    }

    @Override
    public List<String> getCountries() {
        Optional<List<String>> countries = searchHandler.getCountries(restApiTargetv0_3);
        return countries.orElse(Collections.emptyList());
    }
}
