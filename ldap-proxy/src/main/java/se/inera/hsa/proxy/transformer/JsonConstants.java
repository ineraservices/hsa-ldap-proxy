package se.inera.hsa.proxy.transformer;

/**
 * This class contains constants used in JSON messages.
 */
public class JsonConstants {

    private JsonConstants() {
        // not used at the moment
    }

    static final String OBJECT_CLASS = "objectClass";

    static final String TIME_SPAN = "timeSpan";

    static final String DN = "dn";

    static final String PATH = "path";

    static final String COORDINATE = "coordinate";
    static final String COORDINATE_TYPE_KEY = "type";
    static final String COORDINATE_RT90_TYPE_VALUE = "rt90";
    static final String COORDINATE_RT90_X_KEY = "x";
    static final String COORDINATE_RT90_Y_KEY = "y";

    static final String COORDINATE_SWEREF_TYPE_VALUE = "sweref";
    static final String COORDINATE_SWEREF_N_KEY = "N";
    static final String COORDINATE_SWEREF_E_KEY = "E";

    static final String TEMP_DN = "o=tmp";

    static final String HOURS = "hours";

    static final String FROM_DATE = "fromDate";

    static final String TO_DATE = "toDate";

    static final String FROM_DAY = "fromDay";

    static final String TO_DAY = "toDay";

    static final String FROM_TIME = "fromTime2";

    static final String TO_TIME = "toTime2";

    static final String HYPHEN = "-";

    static final String HASH = "#";

    static final String COMMA = ",";

    static final char COLON = ':';

    static final String COMMENT = "comment";

    static final String ADDRESS_LINE = "addressLine";

    static final String DOLLAR_SIGN = "$";

    static final String TYPE = "type";

    static final String JPEG_PHOTO = "jpegphoto";

    static final String HSA_JPEG_LOGO = "hsajpeglogotype";

    static final String USER_CERT = "usercertificate";

    static final String USER_PASSWORD = "userpassword";
}
