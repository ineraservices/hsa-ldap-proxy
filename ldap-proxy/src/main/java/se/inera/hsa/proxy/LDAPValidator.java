package se.inera.hsa.proxy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.protocol.BindRequestProtocolOp;
import com.unboundid.ldap.protocol.BindResponseProtocolOp;
import com.unboundid.ldap.protocol.DeleteResponseProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNResponseProtocolOp;
import com.unboundid.ldap.protocol.ModifyRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyResponseProtocolOp;
import com.unboundid.ldap.protocol.SearchRequestProtocolOp;
import com.unboundid.ldap.protocol.SearchResultDoneProtocolOp;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.RDN;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.schema.AttributeTypeDefinition;
import com.unboundid.ldap.sdk.schema.EntryValidator;
import com.unboundid.ldap.sdk.schema.Schema;

import se.inera.hsa.proxy.exception.DNException;
import se.inera.hsa.proxy.handler.CodeSystemContants;

/**
 * This class performs some basic sanity-checks for the different incoming LDAP-requests. More indepth checks are performed in the organization-api.
 */
public class LDAPValidator {

    private LDAPValidator() {
        //Not used at the moment.
    }

    private static final String INVALID_DN_SYNTAX = "Invalid DN Syntax";

    /**
     * Validate an LDAP add request.  Trying to add CodeSystems through ldap-proxy will result in "NOT_SUPPORTED (92)" LDAP-result code.
     */
	static Optional<AddResponseProtocolOp> validateAddRequest(String dn, Collection<Attribute> attributes, Schema schema) {
	    if (!DN.isValidDN(dn)) {
            return Optional.of(new AddResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, null, INVALID_DN_SYNTAX, null));
        }

        EntryValidator validator = new EntryValidator(schema);
//        validator.setCheckAttributeSyntax(false);
        validator.setCheckMissingAttributes(false);
        validator.setCheckUndefinedAttributes(false);
        validator.setCheckProhibitedAttributes(false);
//        validator.setCheckSingleValuedAttributes(false);




        try {
            if ( DN.isDescendantOf(dn, CodeSystemContants.DN_TO_KODER, true)) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "Not allowed to add CodeSystems through ldap-proxy", null));
            }
        } catch (LDAPException e1) {
            e1.printStackTrace();
        }

        try {
            checkDNStructure(dn);
        } catch (DNException e) {
            return Optional.of(new AddResponseProtocolOp(ResultCode.NAMING_VIOLATION_INT_VALUE, null, e.getMessage(), null));
        }

        Entry addEntry = new Entry(dn, attributes);
        List<String> invalidReasons = new ArrayList<>(); // lista på saker som gick fel

        boolean valid = validator.entryIsValid(addEntry, invalidReasons);
        if (!valid) {
            if (validator.getTotalUndefinedAttributes() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.UNDEFINED_ATTRIBUTE_TYPE_INT_VALUE, null, "The entry contains attribute which is not defined in the schema. " + invalidReasons, null));
            } else if (validator.getTotalAttributesViolatingSyntax() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.INVALID_ATTRIBUTE_SYNTAX_INT_VALUE, null, "The entry contains attibutes which have invalid attribute syntax." + invalidReasons, null));
            } else if (validator.getMalformedDNs() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, dn, "Invalid DN syntax." + invalidReasons, null));
            } else if (validator.getTotalUndefinedObjectClasses() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, "The entry contains an object class which is not defined in the schema." + invalidReasons, null));
            } else if (validator.getEntriesMissingStructuralObjectClass() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, "No structural objectClass" + invalidReasons, null));
            } else if (validator.getEntriesWithMultipleStructuralObjectClasses() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, "Too many structural objectClass "+ invalidReasons, null));
            } else if (validator.getTotalMissingAttributes() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, "Entry missing required attribute. "+ invalidReasons, null));
            }
            else if (validator.getTotalProhibitedAttributes() > 0) {
                return Optional.of(new AddResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, "The entry contains an attribute which is not allowed by its object classes and/or DIT content rule. " + invalidReasons, null));
            }
            return Optional.of(new AddResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, null, "Unwilling to perform, not valid entry " + invalidReasons, null));
        }
        if ( dn.toLowerCase().startsWith("o=")) {
            return Optional.of(new AddResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "Add not allowed on an organization. ", null));
        }
        return Optional.empty();
    }

	/**
	 * Validate an LDAP delete request. Trying to delete CodeSystems through ldap-proxy will result in "NOT_SUPPORTED (92)" LDAP-result code
	 */
    static Optional<DeleteResponseProtocolOp> validateDeleteRequest(String dn) {
        if (!DN.isValidDN(dn)) {
            return Optional.of(new DeleteResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, null, INVALID_DN_SYNTAX, null));
        }

        try {
            if ( DN.isDescendantOf(dn, CodeSystemContants.DN_TO_KODER, true)) {
                return Optional.of(new DeleteResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "Not allowed to delete CodeSystems through ldap-proxy", null));
            }
        } catch (LDAPException e1) {
            e1.printStackTrace();
        }

        return Optional.empty();
    }

    /**
     * Validate an LDAP bind request.
     */
    static Optional<BindResponseProtocolOp> validateBindRequest(BindRequestProtocolOp request) {
        if (!DN.isValidDN(request.getBindDN())) {
            return Optional.of(new BindResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, request.getBindDN(), INVALID_DN_SYNTAX, null, null));
        }

        if (request.getCredentialsType() != BindRequestProtocolOp.CRED_TYPE_SIMPLE) {
            return Optional.of(new BindResponseProtocolOp(ResultCode.AUTH_METHOD_NOT_SUPPORTED_INT_VALUE, null, "Only simple authentication is supported", null, null));
        }
        if (request.getBindDN().isEmpty() || request.getSimplePassword().getValueLength() == 0) {
            return Optional.of(new BindResponseProtocolOp(ResultCode.UNWILLING_TO_PERFORM_INT_VALUE, null, "Anonymous bind not supported!", null, null));
        }
        if (request.getVersion() != 3) {
            return Optional.of(new BindResponseProtocolOp(ResultCode.PROTOCOL_ERROR_INT_VALUE, null, "Only version LDAP v3 is supported", null, null));
        }

        return Optional.empty();
    }

    /**
     * Validate an LDAP modify request. Trying to modify CodeSystems through ldap-proxy will result in "NOT_SUPPORTED (92)" LDAP-result code.
     */
    public static Optional<ModifyResponseProtocolOp> validateModifyRequest(ModifyRequestProtocolOp request, Schema schema) {
        String dn = request.getDN();
        if (!DN.isValidDN(dn)) {
            return Optional.of(new ModifyResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, dn, INVALID_DN_SYNTAX, null));
        }

        try {
            if ( DN.isDescendantOf(dn, CodeSystemContants.DN_TO_KODER, true)) {
                return Optional.of(new ModifyResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "Not allowed to modify CodeSystems through ldap-proxy", null));
            }
        } catch (LDAPException e1) {
            e1.printStackTrace();
        }

        for (Modification modification : request.getModifications()) {
            AttributeTypeDefinition attributeType = schema.getAttributeType(modification.getAttributeName());

            if ( modification.getModificationType().equals(ModificationType.INCREMENT)) {
                return Optional.of(new ModifyResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "ModificationType Increment not supported!", null));
            }

            //TODO: If replace is allowed then this will have to be deleted
            if ( modification.getModificationType().equals(ModificationType.REPLACE)) {
                return Optional.of(new ModifyResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "ModificationType Replace not supported!", null));
            }

            if( attributeType != null && attributeType.isOperational()) {
                return Optional.of(new ModifyResponseProtocolOp(ResultCode.OBJECT_CLASS_VIOLATION_INT_VALUE, null, String.format("Cannot %s operational attribute %s.", modification.getModificationType(), modification.getAttributeName()), null));
            }

            if (modification.getModificationType().equals(ModificationType.DELETE) && ((modification.getAttributeName().equals(AttributeNames.ou) && dn.toLowerCase().startsWith("ou=")) ||
                    (modification.getAttributeName().equals(AttributeNames.cn) && dn.toLowerCase().startsWith("cn=")))) {
//                If the modify request attempts to remove an attribute value that is used in the entry’s RDN, then the server should return a “notAllowedOnRDN” result.
                return Optional.of(new ModifyResponseProtocolOp(ResultCode. NOT_ALLOWED_ON_RDN_INT_VALUE, null, "NOT_ALLOWED_ON_RDN", null));
            }
        }

//		If the modify request attempts to add or replace attribute values and includes a value that does not conform to the associated attribute syntax, then the server should return an "invalidAttributeSyntax" result.
//	    If the modify request attempts to add an attribute value that already exists in the entry, or attempts to add an additional value to a single-valued attribute, then the server should return an "attributeOrValueExists" result.
//		If the modify request attempts to use a delete modification type to remove an attribute or value that does not exist in the entry, then the server should return a "noSuchAttribute" result.
//		If the modify request attempts to add an attribute that is not allowed by any of the object classes or DIT content rule, or to remove an attribute that is required by any of the object classes or DIT content rule, then the server should return an "objectClassViolation" result.
//		If the modify request attempts to change the structural class for the entry, then the server should return an "objectClassModsProhibited" result.


        return Optional.empty();
    }


    /**
      * Validate an LDAP search request.
     */
    public static Optional<SearchResultDoneProtocolOp> validateSearchRequest(SearchRequestProtocolOp request, Schema schema) {
        if (!DN.isValidDN(request.getBaseDN())) {
            return Optional.of(new SearchResultDoneProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, request.getBaseDN(), INVALID_DN_SYNTAX, null));
        }

        if ( request.getScope().intValue() == SearchScope.SUBORDINATE_SUBTREE_INT_VALUE) {
            return Optional.of(new SearchResultDoneProtocolOp(ResultCode.PROTOCOL_ERROR_INT_VALUE, "", "Undefined Search Scope", null));
        }

        //Kolla igenom filtret för att se ifall den innehåller attribut som inte finns med i schemat
        List<String> filterAttrs = getFilterAttrs(request.getFilter());
        for (String aFilterAttrName : filterAttrs) {
            AttributeTypeDefinition attributeType =  schema.getAttributeType(aFilterAttrName);
            if ( attributeType == null ) {
                return Optional.of(new SearchResultDoneProtocolOp(ResultCode.UNDEFINED_ATTRIBUTE_TYPE_INT_VALUE, "", String.format("Attribute %s not defined in schema", aFilterAttrName), null));
            }
        }
        return Optional.empty();
    }

    /**
     * Returns all the attributes used in the filter as a List<String>
     */
    private static List<String> getFilterAttrs(Filter theFilter)
    {
        ArrayList<String> attrs = new ArrayList<>();
        if(theFilter != null)
        {
            if(theFilter.getAttributeName() != null)
            {
                attrs.add(theFilter.getAttributeName());
            }

            for(Filter component : theFilter.getComponents())
            {
                attrs.addAll(getFilterAttrs(component));
            }

            attrs.addAll(getFilterAttrs(theFilter.getNOTComponent()));
        }
        return attrs;
    }

    /**
     * Validate an LDAP modifyDN request. Trying to modifyDN CodeSystems through ldap-proxy will result in "NOT_SUPPORTED (92)" LDAP-result code. Same if trying to move/rename an organization through ldap-proxy.
     */
    public static Optional<ModifyDNResponseProtocolOp> validateModifyDNRequest(ModifyDNRequestProtocolOp request, Schema schema) {
        if (!DN.isValidDN(request.getDN())) {
            return Optional.of(new ModifyDNResponseProtocolOp(ResultCode.INVALID_DN_SYNTAX_INT_VALUE, request.getDN(), INVALID_DN_SYNTAX, null));
        }
        if ( request.getDN().toLowerCase().startsWith("o=")) {
            return Optional.of(new ModifyDNResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "ModifyDN operation not allowed on an organization", null));
        }

        try {
            if ( DN.isDescendantOf(request.getDN(), CodeSystemContants.DN_TO_KODER, true)) {
                return Optional.of(new ModifyDNResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, null, "Not allowed to rename/move CodeSystems through ldap-proxy", null));
            }
        } catch (LDAPException e1) {
            e1.printStackTrace();
        }


        return Optional.empty();
    }

    /**
     * Check that the DN structure is valid.
     *  - Country (c) should only have locality(l) and/or organizations(o) under them.
     *  - localities(l) should only have organizations(o) under them.
     *  - Organiztions should only have units and/or objects which use common-name (cn) under them
     *  - units should only have units and/or objects which use common-name (cn) under them
     *  - objects which use common-name(cn) shouldn't have anything under them.
     */
    static void checkDNStructure(String dnStr) throws DNException {
        try {
            DN dn = new DN(dnStr);
            RDN[] rdNs = dn.getRDNs();
            int max = rdNs.length-1;
            for (int i = max; i >= 0; i--) {
                String currentRdn = rdNs[i].getAttributeNames()[0];
                String parentRdn = null;
                if( i < max ) {
                    parentRdn = rdNs[i+1].getAttributeNames()[0];
                }
                if ( parentRdn != null) {
                    if ( parentRdn.equalsIgnoreCase("c") && !(currentRdn.equalsIgnoreCase("l") || currentRdn.equalsIgnoreCase("o"))) {
                        throw new DNException("Invalid DN structure. Only locality or organization under country. "+dnStr);
                    }
                    if ( parentRdn.equalsIgnoreCase("l") && !currentRdn.equalsIgnoreCase("o")) {
                        throw new DNException("Invalid DN structure. Only organization under locality. "+dnStr);
                    }
                    if ( parentRdn.equalsIgnoreCase("o") && !(currentRdn.equalsIgnoreCase("ou") || currentRdn.equalsIgnoreCase("cn")) ) {
                        throw new DNException("Invalid DN structure. Only units or cns under organization. "+dnStr);
                    }
                    if ( parentRdn.equalsIgnoreCase("ou") && !(currentRdn.equalsIgnoreCase("ou") || currentRdn.equalsIgnoreCase("cn")) ) {
                        throw new DNException("Invalid DN structure. Only units or cns under units. "+dnStr);
                    }
                    if ( parentRdn.equalsIgnoreCase("cn") ) {
                        throw new DNException("Invalid DN structure. Function, person, hsaAdmincommiision, hsacommission shouldn't have any children. "+dnStr);
                    }

                } else if ( !currentRdn.equalsIgnoreCase("c") ) {
                    throw new DNException("Invalid DN structure. Country should be top. "+dnStr);
                }
            }
        } catch (LDAPException e) {
            // TODO: handle exception
        }
    }

}
