package se.inera.hsa.proxy;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.protocol.*;
import com.unboundid.ldap.sdk.Control;

import java.util.List;


/*
 * Interface exposing ldap methods i.e the methods the proxy must implement
 * in order to fulfill the criteria of acting as a functional ldap server
 *
 * general ldap info:
 * https://ldap.com
 *
 * result codes reference:
 * https://ldap.com/ldap-result-code-reference-core-ldapv3-result-codes
 */

public interface RequestProcessor {


    /*
     * ldap result codes in add response:
     * 0 (success), 1, 2, 3, 8, 10, 11, 12, 13, 17, 19, 20, 21, 32,
     * 34, 36, 50, 51, 52, 53, 54, 64, 65, 68, 71, 80 (other)
     */
    LDAPMessage processAddRequest(int messageID,
                                  AddRequestProtocolOp request,
                                  List<Control> controls);

    /*
     * ldap result codes in compare response:
     * 1, 2, 3, 5, 6,  8, 10, 11, 12, 13, 16, 32,
     * 34, 36, 50, 51, 52, 53, 54, 80 (other)
     */
    LDAPMessage processCompareRequest(int messageID,
                                      CompareRequestProtocolOp request,
                                      List<Control> controls);


    /*
     * ldap result codes in delete response:
     * 0 (success), 1, 2, 3, 8, 10, 11, 12, 13, 16, 32,
     * 34, 36, 50, 51, 52, 53, 54, 80 (other)
     */
    LDAPMessage processDeleteRequest(int messageID,
                                     DeleteRequestProtocolOp request,
                                     List<Control> controls);


    /*
     * ldap result codes in extended response:
     * 0 (success), 1, 2, 3, 8, 10, 11, 12, 13, 32,
     * 34, 36, 50, 51, 52, 53, 54, 71, 80 (other)
     */
    LDAPMessage processExtendedRequest(int messageID,
                                       ExtendedRequestProtocolOp request,
                                       List<Control> controls);

    /*
     * ldap result codes in modify response:
     * 0 (success), 1, 2, 3, 8, 10, 11, 12, 13, 16, 17, 19, 20, 21, 32,
     * 34, 36, 50, 51, 52, 53, 54, 65, 67, 69, 71, 80 (other)
     */
    LDAPMessage processModifyRequest(int messageID,
                                     ModifyRequestProtocolOp request,
                                     List<Control> controls);

    /*
     * ldap result codes in modify DN response:
     * 0 (success), 1, 2, 3, 8, 10, 11, 12, 13, 19, 21, 32,
     * 34, 36, 50, 51, 52, 53, 54, 64, 65, 66, 68, 71, 80 (other)
     */
    LDAPMessage processModifyDNRequest(int messageID,
                                       ModifyDNRequestProtocolOp request,
                                       List<Control> controls);

    /*
     * ldap result codes in search response:
     * 0 (success), 1, 2, 3, 4, 8, 10, 11, 12, 13, 18, 32, 33,
     * 34, 36, 50, 51, 52, 53, 54, 80 (other)
     */
    LDAPMessage processSearchRequest(int messageID,
                                     SearchRequestProtocolOp request,
                                     List<Control> controls, LDAPListenerClientConnection connection);


    /*
     * ldap result codes in bind response:
     * 0 (success), 2, 3, 8, 10, 11, 12, 13, 14,
     * 34, 36, 48, 49, 51, 52, 53, 54, 80 (other)
     */
    LDAPMessage processBindRequest(int messageID,
                                   BindRequestProtocolOp request,
                                   List<Control> controls);

    /*
     * ldap result codes in unbind response: none
     */
    LDAPMessage processUnbindRequest(int messageID,
                                     UnbindRequestProtocolOp request,
                                   List<Control> controls);

    
    List<String> getCountries();

}
