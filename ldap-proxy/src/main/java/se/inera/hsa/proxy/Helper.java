package se.inera.hsa.proxy;

import static se.inera.hsa.proxy.transformer.ApiSpecifics.COMMA;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRIES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTRY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.COUNTY_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EMPLOYEES;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EMPLOYEE_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.EQUALS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FRONT_SLASH;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.FUNCTION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATIONS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.ORGANIZATION_RDN;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNITS;
import static se.inera.hsa.proxy.transformer.ApiSpecifics.UNIT_RDN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Helper {

    public enum API {
        ALL, AUTHORIZATION, EMPLOYEE, ORGANIZATION
    }

    public static String transposePathToDnV3(String dn) {

        List<String> pathList = Arrays.asList(dn.split(FRONT_SLASH));

        List<String> transposedList = new ArrayList<>();

        Iterator<String> pathIterator = pathList.iterator();


        while (pathIterator.hasNext()) {
            String type = replacePathElementWithRDNV3(pathIterator.next());
            String typeValue = pathIterator.next();
            transposedList.add(type + EQUALS + typeValue);

        }

        Collections.reverse(transposedList);

        return String.join(COMMA, transposedList).replaceAll("(?i)Sverige", "SE");
    }

    /**
     * returns parent of a DN.
     * @param dn
     * @return
     */
    public static String getParentDn(String dn) {
        return dn.substring(1+dn.indexOf(','));
    }

    private static String replacePathElementWithRDNV3(String pathElement) {
        return pathElement.trim()
                .replace(EMPLOYEES,EMPLOYEE_RDN)
                .replace(FUNCTIONS,FUNCTION_RDN)
                .replace(UNITS,UNIT_RDN)
                .replace(ORGANIZATIONS,ORGANIZATION_RDN)
                .replace(COUNTIES,COUNTY_RDN)
                .replace(COUNTRIES, COUNTRY_RDN);
    }

}
