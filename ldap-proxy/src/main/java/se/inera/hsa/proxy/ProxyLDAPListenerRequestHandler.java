package se.inera.hsa.proxy;


import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Init;

import com.unboundid.ldap.listener.LDAPListenerClientConnection;
import com.unboundid.ldap.listener.LDAPListenerRequestHandler;
import com.unboundid.ldap.protocol.AbandonRequestProtocolOp;
import com.unboundid.ldap.protocol.AddRequestProtocolOp;
import com.unboundid.ldap.protocol.AddResponseProtocolOp;
import com.unboundid.ldap.protocol.BindRequestProtocolOp;
import com.unboundid.ldap.protocol.BindResponseProtocolOp;
import com.unboundid.ldap.protocol.CompareRequestProtocolOp;
import com.unboundid.ldap.protocol.CompareResponseProtocolOp;
import com.unboundid.ldap.protocol.DeleteRequestProtocolOp;
import com.unboundid.ldap.protocol.DeleteResponseProtocolOp;
import com.unboundid.ldap.protocol.ExtendedRequestProtocolOp;
import com.unboundid.ldap.protocol.ExtendedResponseProtocolOp;
import com.unboundid.ldap.protocol.LDAPMessage;
import com.unboundid.ldap.protocol.ModifyDNRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyDNResponseProtocolOp;
import com.unboundid.ldap.protocol.ModifyRequestProtocolOp;
import com.unboundid.ldap.protocol.ModifyResponseProtocolOp;
import com.unboundid.ldap.protocol.SearchRequestProtocolOp;
import com.unboundid.ldap.protocol.SearchResultDoneProtocolOp;
import com.unboundid.ldap.protocol.UnbindRequestProtocolOp;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.controls.ProxiedAuthorizationV2RequestControl;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;
import com.unboundid.util.Validator;

/**
 * An instance of this class is created for each client that is connected to LDAP-proxy. When a client connects, newInstance is called.
 * After that it will listen for ldap-operations and handle them by doing a simple LDAP validation on the request then process them and send them to API.
 *
 */
public final class ProxyLDAPListenerRequestHandler extends LDAPListenerRequestHandler {

    /**
     * DN for the schema entry
     */
    private static final String CN_SCHEMA = "cn=schema";

    /**
     *  hook for sending message to another handler
     */
    private final RequestProcessor requestProcessor;

    /**
     * A pre-allocated empty array of controls.
     */
    private static final Control[] EMPTY_CONTROL_ARRAY = new Control[0];

    /**
     * Connection back to the client. This is used to send search result entries back to the client.
     */
    private LDAPListenerClientConnection connection;

    /**
     * LDAP-schema that is used for LDAP-proxy
     */
    private Schema schema;

    /**
     * Some objects that only exist in LDAP-proxy. These are initialized in *initRootObjects*
     */
    private Map<String, Entry> rootObjects;

    /**
     * Creates a new LDAP request handler that will write detailed
     * information about the contents of all requests and responses that pass
     * through it using the provided log handler, and will process client requests
     * using the provided request handler.
     */
    ProxyLDAPListenerRequestHandler(RequestProcessor requestProcessor, LDAPListenerClientConnection connection) throws IOException, LDIFException {
        Validator.ensureNotNull(requestProcessor);
        this.requestProcessor = requestProcessor;
        this.connection = connection;

        InputStream hsaShemaStream = getClass().getClassLoader().getResourceAsStream("hsa-schema.ldif");
        assert hsaShemaStream != null;
        schema = Schema.getSchema(hsaShemaStream);

        initRootObjects();

    }

    /**
     * Initializes the root object
     */
    private void initRootObjects() {
//        If wanting to get the actual countries from rest-api then below would work. But since it's always 'Sverige' it feels like unnecessary db calls
//        List<String> countries = requestProcessor.getCountries();
//        String[] countriesArray = (String[]) countries.toArray(new String[countries.size()]);

        // Hardcoded to c=SE for now, see above comment.
        String[] countriesArray = {"c=SE"};
        Entry root = new Entry("");
        root.addAttribute(AttributeNames.namingContexts, countriesArray);
        root.addAttribute(AttributeNames.subSchemaSubentry, CN_SCHEMA);
        root.addAttribute(AttributeNames.objectClass, ObjectClasses.top, ObjectClasses.ldapRootDSE, ObjectClasses.subEntry);
        root.addAttribute(AttributeNames.SupportedLDAPVersion, "3");
        root.addAttribute(AttributeNames.ogSupportedProfile, "1.2.826.0.1050.11.1.1");
        root.addAttribute(AttributeNames.supportedControl, SimplePagedResultsControl.PAGED_RESULTS_OID); //
        root.addAttribute(AttributeNames.VendorName, "Inera AB");
        root.addAttribute(AttributeNames.VendorVersion, "LDAP-Proxy "+ProxyServletContextListener.version);

        rootObjects = new HashMap<>();
        rootObjects.put("", root);
        rootObjects.put(CN_SCHEMA, schema.getSchemaEntry());
    }

    @Override
    public ProxyLDAPListenerRequestHandler newInstance(LDAPListenerClientConnection connection) throws LDAPException {
        System.out.println(format("New Instance of LdapProxy for id: %s ip: %s", connection.getConnectionID(), connection.getSocket().getLocalAddress().getHostAddress()));
        try {
            return new ProxyLDAPListenerRequestHandler(requestProcessor, connection);
        } catch (LDIFException | IOException e) {
            throw new LDAPException(ResultCode.CONNECT_ERROR, "Schema error!");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public void closeInstance() {
        if ( connection != null ) {
            System.out.println(format("Close Instance of LdapProxy for id: %d",connection.getConnectionID()));
        } else {
            System.out.println("Close Instance of LdapProxy. ");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public void processAbandonRequest(final int messageID,
                                      final AbandonRequestProtocolOp request,
                                      final List<Control> controls) {
        // Not used at the moment
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processAddRequest(final int messageID,
                                         final AddRequestProtocolOp request,
                                         final List<Control> controls) {
        Optional<AddResponseProtocolOp> validateAddRequest = LDAPValidator.validateAddRequest(request.getDN(),
                request.getAttributes(), schema);
        return validateAddRequest.map(addResponseProtocolOp -> new LDAPMessage(messageID, addResponseProtocolOp, controls))
                .orElseGet(() -> requestProcessor.processAddRequest(messageID, request, controls));

    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processBindRequest(final int messageID,
                                          final BindRequestProtocolOp request,
                                          final List<Control> controls) {
        Optional<BindResponseProtocolOp> validateBindRequest = LDAPValidator.validateBindRequest(request);
        return validateBindRequest.map(bindResponseProtocolOp -> new LDAPMessage(messageID, bindResponseProtocolOp,
                EMPTY_CONTROL_ARRAY)).orElseGet(() -> {
                   System.out.println(format("Bind from id: %d: ip: %s", connection.getConnectionID(), connection.getSocket().getLocalAddress().getHostAddress()));
                   return requestProcessor.processBindRequest(messageID, request, controls);
                });
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processCompareRequest(final int messageID,
                                             final CompareRequestProtocolOp request,
                                             final List<Control> controls) {
        CompareResponseProtocolOp result = new CompareResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, "", "Operation not supportet yet.", null);

        return new LDAPMessage(messageID, result, EMPTY_CONTROL_ARRAY);
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processDeleteRequest(final int messageID,
                                            final DeleteRequestProtocolOp request,
                                            final List<Control> controls) {
        Optional<DeleteResponseProtocolOp> invalidDeleteResponse = LDAPValidator.validateDeleteRequest(request.getDN());
        return invalidDeleteResponse.map(deleteResponseProtocolOp -> new LDAPMessage(messageID, deleteResponseProtocolOp,
                EMPTY_CONTROL_ARRAY)).orElseGet(() -> requestProcessor.processDeleteRequest(messageID, request, controls));
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processExtendedRequest(final int messageID,
                                              final ExtendedRequestProtocolOp request,
                                              final List<Control> controls) {
        ExtendedResponseProtocolOp result = new ExtendedResponseProtocolOp(ResultCode.NOT_SUPPORTED_INT_VALUE, "", "Operation not supportet yet.", null, null, null);

        return new LDAPMessage(messageID, result, EMPTY_CONTROL_ARRAY);
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processModifyRequest(final int messageID,
                                            final ModifyRequestProtocolOp request,
                                            final List<Control> controls) {
        Optional<ModifyResponseProtocolOp> errorResponse = LDAPValidator.validateModifyRequest(request, schema);
        if ( errorResponse.isPresent()) {
            return new LDAPMessage(messageID, errorResponse.get(), controls);
        }
        return requestProcessor.processModifyRequest(messageID, request, controls);
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processModifyDNRequest(final int messageID,
                                              final ModifyDNRequestProtocolOp request,
                                              final List<Control> controls) {
        Optional<ModifyDNResponseProtocolOp> errorResponse = LDAPValidator.validateModifyDNRequest(request, schema);
        if ( errorResponse.isPresent()) {
            return new LDAPMessage(messageID, errorResponse.get(), controls);
        }
        return requestProcessor.processModifyDNRequest(messageID, request, controls);
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public LDAPMessage processSearchRequest(final int messageID,
                                            final SearchRequestProtocolOp request,
                                            final List<Control> controls) {
        Optional<SearchResultDoneProtocolOp> invalidSearchResponse = LDAPValidator.validateSearchRequest(request, schema);
        if ( invalidSearchResponse.isPresent()) {
            return new LDAPMessage(messageID, invalidSearchResponse.get(), controls);
        }

        handleRootSearch(messageID, request);
        if (request.getScope().intValue() == SearchScope.BASE_INT_VALUE && (request.getBaseDN().equals(CN_SCHEMA) || request.getBaseDN().equals(""))) {

            SearchResultDoneProtocolOp addResponse = new SearchResultDoneProtocolOp(ResultCode.SUCCESS_INT_VALUE, request.getBaseDN(), "", null);
            return new LDAPMessage(messageID, addResponse, controls);
        } else {
            SearchRequestProtocolOp searchRequest = request;
            if ( request.getBaseDN().equals("") && request.getScope().intValue() == SearchScope.ONE_INT_VALUE) {
                searchRequest = new SearchRequestProtocolOp("C=SE", SearchScope.BASE, request.getDerefPolicy(), request.getSizeLimit(), request.getTimeLimit(), request.typesOnly(), request.getFilter(), request.getAttributes());
            }
            return requestProcessor.processSearchRequest(messageID, searchRequest, controls, connection);
        }
    }

    /**
     * Checks if the searchRequest matches the root objects, if it does, send them back to client.
     */
    private void handleRootSearch(final int messageID, final SearchRequestProtocolOp request) {
            for (java.util.Map.Entry<String, Entry> entrySet : rootObjects.entrySet()) {
                try {
                    Entry entry = entrySet.getValue();
                    boolean matchesBaseAndScope = entry.matchesBaseAndScope(request.getBaseDN(), request.getScope());
                    boolean matchesEntry = request.getFilter().matchesEntry(entry);
                    if (matchesBaseAndScope && matchesEntry) {
                        connection.sendSearchResultEntry(messageID, entry, EMPTY_CONTROL_ARRAY);
                    }
                } catch (LDAPException e) {
                    e.printStackTrace();
                }
            }
    }

    /**
     * {@inheritDoc}
     */
    @Override()
    public void processUnbindRequest(final int messageID,
                                     final UnbindRequestProtocolOp request,
                                     final List<Control> controls) {
        System.out.println(format("Unbind from id: %d: ip: %s", connection.getConnectionID(), connection.getSocket().getLocalAddress().getHostAddress()));
        requestProcessor.processUnbindRequest(messageID, request, controls);
    }

    /**
     * Preparation when/if needed to use ProxiedAuthorization is needed.
     */
    private Optional<DN> getProxiedAuthDnFromControl(List<Control> controls)
            throws LDAPException {
        for (Control control : controls) {
            // Osäker om ProxiedAuthorizationV1RequestControl används eller om det endast är v2
//    		if ( control.getOID().equals(ProxiedAuthorizationV1RequestControl.PROXIED_AUTHORIZATION_V1_REQUEST_OID));
//    		{
//    			ProxiedAuthorizationV1RequestControl p1 = (ProxiedAuthorizationV1RequestControl) control;
//    			// Lägga in hsa-schemat?
//    			final DN authzDN = new DN(p1.getProxyDN(), null);
//    			if ( !authzDN.isNullDN() )
//    			{
//    				return Optional.of(authzDN);
//    			}
//    		}

            if (control.getOID().equals(ProxiedAuthorizationV2RequestControl.PROXIED_AUTHORIZATION_V2_REQUEST_OID)) {
                ProxiedAuthorizationV2RequestControl p2 = (ProxiedAuthorizationV2RequestControl) control;
                return Optional.of(new DN(p2.getAuthorizationID()));
            }

        }
        return Optional.empty();
    }
}
