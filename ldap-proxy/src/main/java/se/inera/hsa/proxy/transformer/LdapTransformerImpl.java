package se.inera.hsa.proxy.transformer;

import static java.lang.String.format;
import static se.inera.hsa.proxy.AttributeNames.mobile;
import static se.inera.hsa.proxy.AttributeNames.mobileTelephoneNumber;
import static se.inera.hsa.proxy.AttributeNames.pager;
import static se.inera.hsa.proxy.AttributeNames.pagerTelephoneNumber;
import static se.inera.hsa.proxy.AttributeNames.street;
import static se.inera.hsa.proxy.AttributeNames.streetAddress;
import static se.inera.hsa.proxy.ObjectClasses.hsaHealthCareProvider;
import static se.inera.hsa.proxy.ObjectClasses.hsaHealthCareUnit;
import static se.inera.hsa.proxy.transformer.JsonConstants.ADDRESS_LINE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COLON;
import static se.inera.hsa.proxy.transformer.JsonConstants.COMMA;
import static se.inera.hsa.proxy.transformer.JsonConstants.COMMENT;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_SWEREF_E_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_SWEREF_N_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_SWEREF_TYPE_VALUE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_TYPE_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_RT90_TYPE_VALUE;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_RT90_X_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.COORDINATE_RT90_Y_KEY;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_DATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_DAY;
import static se.inera.hsa.proxy.transformer.JsonConstants.FROM_TIME;
import static se.inera.hsa.proxy.transformer.JsonConstants.HASH;
import static se.inera.hsa.proxy.transformer.JsonConstants.HSA_JPEG_LOGO;
import static se.inera.hsa.proxy.transformer.JsonConstants.HYPHEN;
import static se.inera.hsa.proxy.transformer.JsonConstants.JPEG_PHOTO;
import static se.inera.hsa.proxy.transformer.JsonConstants.TIME_SPAN;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_DATE;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_DAY;
import static se.inera.hsa.proxy.transformer.JsonConstants.TO_TIME;
import static se.inera.hsa.proxy.transformer.JsonConstants.USER_CERT;
import static se.inera.hsa.proxy.transformer.JsonConstants.USER_PASSWORD;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.RDN;
import com.unboundid.ldap.sdk.schema.AttributeTypeDefinition;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFException;

import se.inera.hsa.proxy.AttributeNames;
import se.inera.hsa.proxy.Helper;
import se.inera.hsa.proxy.Helper.API;
import se.inera.hsa.proxy.ObjectClasses;


/**
 * The purpose of this class is to transform a LDAP-entry to json object so that it can be sent to the organization-api. T
 */
public class LdapTransformerImpl implements LdapTransformer {

    private static final Pattern timeHours = Pattern.compile("[1-7]-[1-7](#[0-2][0-9]:[0-5][0-9]){2}(#[0-9a-zA-Z &'()+,-./:;\\xC0-\\xFF&&[^\\xD7\\xF7]]{0,19})?(#(\\d{8})?){0,2}(?<![\\x20])$");

    private static final Pattern geoGraphicalCoordinatesRegExp = Pattern.compile("[xX]:\\d{7},[yY]:\\d{7}");
    private static final Pattern geoGraphicalCoordinatesSwerefRegExp = Pattern.compile("[nN]:\\d{7},[eE]:\\d{6}");

    private static final String POSTAL_ADDRESS_REGEX = "\\$";

    private static final String GEOGRAPHICAL_COORDINATES_REGEX = "\\s+";

    private static final String IS_BINARY_POSTFIX = ";binary";


    /// values should be lowercase
    private static final List<String> OBJECT_CLASSES_TO_REMOVE = Arrays.asList("top", "hsalocalityextension", "hsaorganizationextension",
            "hsaroleextension", "hsapersonextension",
            "inetorgperson", "organizationalperson");

    private Schema schema;

    /**
     * Used to load the ldap-schema.
     */
    @PostConstruct
    public void init() {
        InputStream hsaShemaStream = getClass().getClassLoader().getResourceAsStream("hsa-schema.ldif");
        try {
             schema = Schema.getSchema(hsaShemaStream);
        } catch (LDIFException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Transform an ldap Entry to json format.
     */
    @Override
    public String transformToJson(String dn, Collection<Attribute> attributes) {
        JsonObject unit = transformEntryToPUTformat(dn, attributes);

        return unit.toString();
    }

    /**
     * This method transforms an attribute to a json format, using attribute name as json name.
     */
    private void transformAttributeToJson(String dn, JsonObjectBuilder jsonBuilder, Attribute attribute) {
        transformAttributeToJson(dn, jsonBuilder, attribute, false);
    }

    /**
     * This method transforms an attribute to a json format.
     * If *usePatchAttributeNames* is used then it will add the attribute with 'attributeName: [attributename]' instead of [attributeName]: [attributeValue]
     */
	private void transformAttributeToJson(String dn, JsonObjectBuilder jsonBuilder, Attribute attribute, boolean usePatchAttributeNames) {
		String attributeName = attribute.getName();
		if (attributeName.toLowerCase().contains(";binary") ) {
		    attributeName = attributeName.replace(";binary", "");
		}
		AttributeTypeDefinition attributeType = schema.getAttributeType(attributeName);
		if (isBinary(attributeName)) {
			transformBinaryAttr(jsonBuilder, attribute,  attributeType.isSingleValued());
		} else {
		    String[] values = attribute.getValues();
		    if ("objectClass".equalsIgnoreCase(attributeName)) {
		        transformObjectClass(dn, jsonBuilder, values, usePatchAttributeNames);
		    } else {
		        attributeName = mapAttributeName(attributeName);
		        if ( attributeType != null ) {
		            String baseAttributeName = attributeName;
		            if (!attributeType.isSingleValued()) {
		                transfomMultiValueAttribute(jsonBuilder, baseAttributeName, values, usePatchAttributeNames);
		            } else if ( values.length > 1 ){
		                System.out.print(format("Attribute %s is single-value but has multiple values. Discarding attribute from json.", baseAttributeName));
		            }  else {
		                transformSingleValueAttribute(jsonBuilder, baseAttributeName, values[0], usePatchAttributeNames);

		            }
		        } else {
		            System.out.print(format("Attribute %s is not found in ldap-schema.", attributeName));
		            if ( values.length > 1) {
		                transfomMultiValueAttribute(jsonBuilder, attributeName, values, usePatchAttributeNames);
		            } else {
		                transformSingleValueAttribute(jsonBuilder, attributeName, values[0], usePatchAttributeNames);
		            }
		        }
		    }
		}
	}

	/**
	 * Transforms an LDAP objectClass o jsonFormat.
	 */
    private void transformObjectClass(String dn, JsonObjectBuilder jsonBuilder, String[] values,
            boolean usePatchAttributeNames) {
        if (usePatchAttributeNames)
            transformObjectClassPatch(dn, jsonBuilder, values);
        else
            transformObjectClass(dn, jsonBuilder, values);
    }

    /**
     * Transforms a single value attribute to json format.
     */
    private void transformSingleValueAttribute(JsonObjectBuilder jsonBuilder, String attributeName, String attributeValue,
            boolean usePatchAttributeNames) {
        if (usePatchAttributeNames)
            transformSingleValueAttributePatch(jsonBuilder, attributeName, attributeValue);
        else
            transformSingleValueAttribute(jsonBuilder, attributeName, attributeValue);
    }

    /**
     * Transform an objectClass to json patch format.
     */
    private void transformObjectClassPatch(String dn, JsonObjectBuilder jsonBuilder, String[] values) {
        //TODO handle pkiUser
        if ( Arrays.stream(values).anyMatch(hsaHealthCareProvider::equalsIgnoreCase) ) {
            jsonBuilder.remove("attributeName");
            jsonBuilder.add("attributeName", hsaHealthCareProvider);
            jsonBuilder.add("booleanValue", true);
            values = Arrays.stream(values).filter( o -> !o.equalsIgnoreCase(ObjectClasses.hsaHealthCareProvider)).toArray(String[]::new);
        }
        if ( Arrays.stream(values).anyMatch(hsaHealthCareUnit::equalsIgnoreCase) ) {
            values = Arrays.stream(values).filter( o -> !o.equalsIgnoreCase(ObjectClasses.hsaHealthCareUnit)).toArray(String[]::new);
            jsonBuilder.remove("attributeName");
            jsonBuilder.add("attributeName", hsaHealthCareUnit);
            jsonBuilder.add("booleanValue", true);
        }
    }

    /**
     * Transform an objectClass to json format.
     */
    private void transformObjectClass(String dn, JsonObjectBuilder jsonBuilder, String[] values) {
        //TODO handle pkiUser
        if ( Arrays.stream(values).anyMatch(hsaHealthCareProvider::equalsIgnoreCase) ) {
            values = Arrays.stream(values).filter( o -> !o.equalsIgnoreCase(ObjectClasses.hsaHealthCareProvider)).toArray(String[]::new);
            jsonBuilder.add(hsaHealthCareProvider, true);
        }
        if ( Arrays.stream(values).anyMatch(hsaHealthCareUnit::equalsIgnoreCase) ) {
            values = Arrays.stream(values).filter( o -> !o.equalsIgnoreCase(ObjectClasses.hsaHealthCareUnit)).toArray(String[]::new);
            jsonBuilder.add(hsaHealthCareUnit, true);
        }
        values = filterObjectClasses(values, dn.substring(0, dn.indexOf('=')));

        jsonBuilder.add("type", values[0]);
    }

    /**
     * Transform a binary object to JSON format by encoding the bytes in base64 format.
     */
    private void transformBinaryAttr(JsonObjectBuilder jsonBuilder, Attribute attribute, boolean isSingleValue) {
        byte[][] valueByteArrays = attribute.getValueByteArrays();
        if (!isSingleValue) {
            JsonArrayBuilder multiValue = Json.createArrayBuilder();
            Arrays.stream(valueByteArrays).map(i -> Json.createValue(encodeBase64(i)))
                    .forEach(multiValue::add);
            jsonBuilder.add(attribute.getName(), multiValue.build());
        } else {
            jsonBuilder.add(attribute.getName(), encodeBase64(valueByteArrays[0]));
        }
    }

    /**
     * Transforms a multivalue attribute to json format.
     */
    private void transfomMultiValueAttribute(JsonObjectBuilder jsonBuilder, final String attributeName, String[] values, boolean usePatchAttributeNames) {
        if (usePatchAttributeNames)
            transfomMultiValueAttributePatch(jsonBuilder, attributeName, values);
        else
            transfomMultiValueAttribute(jsonBuilder, attributeName, values);

    }

    /**
     * Transforms a multivalue attribute to json format.
     */
    private void transfomMultiValueAttribute(JsonObjectBuilder jsonBuilder, final String attributeName, String[] values) {
        JsonArrayBuilder multiValue = Json.createArrayBuilder();
        Arrays.stream(values).filter(Objects::nonNull).map(value ->  {
                    if (isTimeSpan(value)) {
                        return timeSpanToJson(value);
                    } else if (isRT90Coordinates(value)) {
                        return transformRT90CoordinatesToJson(value);
                    }  else if (isSweRefCoordinates(value)) {
                        return transformSweRefCoordinatesToJson(value);
                    } else if (attributeName.equalsIgnoreCase(AttributeNames.postalAddress)) {
                        return  transformAddressLineToJson(value);
                    }
                    else {
                        return Json.createValue(value);
                    }
                }
           ).forEach(multiValue::add);
        if ( attributeName.equals("cn") ) {
            jsonBuilder.add("cn", multiValue.build());
        } else if ( attributeName.equals("ou") ) {
            jsonBuilder.add("ou", multiValue.build());
        } else if ( attributeName.equals("o") ) {
            jsonBuilder.add("o", multiValue.build());
        } else if (attributeName.equals("l")) {
            jsonBuilder.add("localityName", multiValue.build());
        } else {
            jsonBuilder.add(attributeName, multiValue.build());
        }
    }

    /**
     * Transforms a multivalue attribute to json format. USing patch format on attribute names.
     */
    private void transfomMultiValueAttributePatch(JsonObjectBuilder jsonBuilder, final String attributeName, String[] values) {
        JsonArrayBuilder multiValue = Json.createArrayBuilder();
        Arrays.stream(values).filter(Objects::nonNull).map(value ->
        {
            if (isTimeSpan(value)) {
                return timeSpanToJson(value);
            } else if (isRT90Coordinates(value)) {
                return transformRT90CoordinatesToJson(value);
            }  else if (isSweRefCoordinates(value)) {
                return transformSweRefCoordinatesToJson(value);
            } else if (attributeName.equalsIgnoreCase(AttributeNames.postalAddress)) {
                return  transformAddressLineToJson(value);
            }
            else {
                return Json.createValue(value);
            }
        }
                ).forEach(multiValue::add);
        if ( attributeName.toLowerCase().endsWith("hours") ) {
            jsonBuilder.add("hoursValues", multiValue.build());
        } else {
            jsonBuilder.add("stringValues", multiValue.build());
        }
    }

    /**
     * Transforms a single value attribute to json format.
     */
    private void transformSingleValueAttribute(JsonObjectBuilder jsonBuilder, String attributeName, String value) {
        if (isTimeSpan(value)) {
            jsonBuilder.add(attributeName, timeSpanToJson(value));
        } else if (isRT90Coordinates(value)) {
            jsonBuilder.add(attributeName, transformRT90CoordinatesToJson(value));
        } else if (isSweRefCoordinates(value)) {
            //TODO: swerefCoordinates need to be supported in Patch if its to be used
            jsonBuilder.add(attributeName, transformSweRefCoordinatesToJson(value));
        } else if (attributeName.equalsIgnoreCase(AttributeNames.postalAddress)) {
            jsonBuilder.add(attributeName, transformAddressLineToJson(value));
        } else {
        	if ( attributeName.equals("cn") ) {
                 jsonBuilder.add("cn", value);
            } else if (attributeName.equals("ou") ) {
                jsonBuilder.add("ou", value);
            } else if (attributeName.equals("o") ) {
                jsonBuilder.add("o", value);
            } else if (attributeName.equals("hsaIdentity")) {
                jsonBuilder.add("hsaIdentity", value);
            } else if (attributeName.equals("l")) {
                jsonBuilder.add("localityName", value);
            }
            else {
                jsonBuilder.add(attributeName, value);
            }
        }
    }


    /**
     * Transforms a single value attribute to json format. Using patch format.
     */
    private void transformSingleValueAttributePatch(JsonObjectBuilder jsonBuilder, String attributeName, String value) {
        if (isTimeSpan(value)) {
            jsonBuilder.add("hoursValues", timeSpanToJson(value));
        } else if (isRT90Coordinates(value)) {
            jsonBuilder.add("geographicalCoordinatesValue", transformRT90CoordinatesToJson(value));
        } else if (isSweRefCoordinates(value)) {
            //TODO: swerefCoordinates need to be supported in Patch if its to be used
            jsonBuilder.add("geographicalCoordinatesValue", transformSweRefCoordinatesToJson(value));
        } else if (attributeName.equalsIgnoreCase(AttributeNames.postalAddress)) {
            jsonBuilder.add("postalAddressValue", transformAddressLineToJson(value));
        } else if ( attributeName.equals("cn") || attributeName.equals("ou") || attributeName.equals("o")) {
            jsonBuilder.remove("attributeName");
            jsonBuilder.add("attributeName", "name");
            jsonBuilder.add("stringValue", value);
        } else {
            jsonBuilder.add("stringValue", value);
        }

        //TODO: FIXME ou/o/cn -> attributeName => name
    }

    /**
     * Transforms an RT90 cooridnate to jsonformat.
     * Example:
     * X:6598840,Y:1641424 ->
     * "geographicalCoordinates": {
     *   "coordinate": {
     *     "type": "rt90",
     *     "X": "6598840",
     *     "Y": "1641424"
     *   }
     *  },
     */
    JsonObject transformRT90CoordinatesToJson(String coordinates) {
        if (coordinates == null || !isRT90Coordinates(coordinates))
            return null;

        JsonObjectBuilder coordinatesObject = Json.createObjectBuilder();

        JsonObjectBuilder coordinate = Json.createObjectBuilder();

        String[] part = coordinates.toUpperCase().split(COMMA);

        coordinate.add(COORDINATE_TYPE_KEY, Json.createValue(COORDINATE_RT90_TYPE_VALUE));
        coordinate.add(COORDINATE_RT90_X_KEY, Json.createValue(part[0].substring(part[0].indexOf(COLON) + 1)));
        coordinate.add(COORDINATE_RT90_Y_KEY, Json.createValue(part[1].substring(part[1].indexOf(COLON) + 1)));

        coordinatesObject.add(COORDINATE, coordinate.build());
        return coordinatesObject.build();
    }

    /**
     * Returns true if the input string is in RT90 format.
     */
    boolean isRT90Coordinates(String input) {
        return geoGraphicalCoordinatesRegExp.matcher(input.replaceAll(GEOGRAPHICAL_COORDINATES_REGEX, "")).matches();
    }

    /**
     * Transforms a SWEREF99 to json format.
     * @param coordinates
     * @return
     */
    JsonObject transformSweRefCoordinatesToJson(String coordinates) {
        if (coordinates == null || !isSweRefCoordinates(coordinates))
            return null;

        JsonObjectBuilder coordinatesObject = Json.createObjectBuilder();

        JsonObjectBuilder coordinate = Json.createObjectBuilder();

        String[] part = coordinates.toUpperCase().split(COMMA);

        coordinate.add(COORDINATE_TYPE_KEY, Json.createValue(COORDINATE_SWEREF_TYPE_VALUE));
        coordinate.add(COORDINATE_SWEREF_N_KEY, Json.createValue(part[0].substring(part[0].indexOf(COLON) + 1)));
        coordinate.add(COORDINATE_SWEREF_E_KEY, Json.createValue(part[1].substring(part[1].indexOf(COLON) + 1)));

        coordinatesObject.add(COORDINATE, coordinate.build());
        return coordinatesObject.build();
    }


    /**
     * Returns true if the input string is in SWEREF99 format.
     */
    boolean isSweRefCoordinates(String input) {
        return geoGraphicalCoordinatesSwerefRegExp.matcher(input.replaceAll(GEOGRAPHICAL_COORDINATES_REGEX, "")).matches();
    }

    /**
     * Returns true if the attribute is in binary format. The check is made using attributeName.
     */
    boolean isBinary(String attributeName) {
        if (attributeName == null || attributeName.trim().isEmpty()) {
            return false;
        }
        if (attributeName.toLowerCase().endsWith(IS_BINARY_POSTFIX)) {
            return true;
        }
        switch (attributeName.toLowerCase()) {
            case USER_CERT:
            case USER_PASSWORD:
            case HSA_JPEG_LOGO:
            case JPEG_PHOTO:
                return true;
            default:
                return false;
        }
    }


    /**
     * Transforms a timespan to jsonformat.
     * Example:
     * 1-5#08:00#10:00 or 1-5#08:00#10:00#(optional comment)#(optional start date)#(optional end date)
     * =>
     *  timeSpan:{
     *    fromDay:1,
     *    fromTime2:08:00,
     *    toTime2:10:00,
     *    toDay:5
     *  }
     */
    JsonObject timeSpanToJson(String ldapFormattedTimeSpan) {

        if (ldapFormattedTimeSpan == null || !isTimeSpan(ldapFormattedTimeSpan))
            return null;

        JsonObjectBuilder timeSpanBuilder = Json.createObjectBuilder();

        JsonObjectBuilder dataBuilder = Json.createObjectBuilder();

        String[] part = ldapFormattedTimeSpan.split(HASH);

        String[] day = part[0].split(HYPHEN);

        dataBuilder.add(FROM_DAY, Json.createValue(day[0]));
        dataBuilder.add(FROM_TIME, Json.createValue(part[1]));
        dataBuilder.add(TO_TIME, Json.createValue(part[2]));
        dataBuilder.add(TO_DAY, Json.createValue(day[1]));

        if (part.length > 3)
            dataBuilder.add(COMMENT, Json.createValue(part[3]));

        if (part.length > 4)
            dataBuilder.add(FROM_DATE, Json.createValue(part[4]));

        if (part.length > 5)
            dataBuilder.add(TO_DATE, Json.createValue(part[5]));

        timeSpanBuilder.add(TIME_SPAN, dataBuilder.build());

        return timeSpanBuilder.build();
    }


    /**
     * Transforms postalAddress to jsonformat.
     * Example:
     * Vårdcentral Malung$Box 64$782 22 Malung ..
     * =>
     *   "addressLine":[
     *   "Vårdcentral Malung"
     *   "Box 64",
     *   "782 22 Malung",
     *   ]
     */
    JsonObject transformAddressLineToJson(String ldapFormattedPostalAddress) {

        if (ldapFormattedPostalAddress == null)
            return null;

        JsonObjectBuilder addressLineBuilder = Json.createObjectBuilder();

        String[] part = ldapFormattedPostalAddress.split(POSTAL_ADDRESS_REGEX);

        JsonArrayBuilder addressPartsBuilder = Json.createArrayBuilder();

        Arrays.stream(part).forEach(addressPartsBuilder::add);

        addressLineBuilder.add(ADDRESS_LINE, addressPartsBuilder.build());

        return addressLineBuilder.build();
    }

    /**
     * Returns true if the input is in timespan format.
     */
    boolean isTimeSpan(String input) {
        return timeHours.matcher(input).matches();
    }

    /**
     * Encodes bytes in base64 format.
     */
    private String encodeBase64(byte[] input) {
        return Base64.getEncoder().encodeToString(input);
    }

    /**
     * Filter out
     */
    String[] filterObjectClasses(String[] values, String rdn) {

        String type;

        final List<String> COMMON_OBJECT_CLASSES_TO_REMOVE = Collections.singletonList("top");

        final List<String> ORGANIZATION_UNIT_CLASSES_TO_REMOVE = Arrays.asList("hsaorganizationextension",
                "hiddenobject", "hsafeigneddataobject");
        final List<String> FUNCTION_CLASSES_TO_REMOVE = Arrays.asList("hsaroleextension", "hiddenobject", "hsafeigneddataobject", "pkiuser");

        final List<String> PERSON_CLASSES_TO_REMOVE = Arrays.asList("hsapersonextension", "inetorgperson", "organizationalperson", "pkiuser", "hiddenobject", "hsafeigneddataobject");

        switch (rdn) {
            case "o":
            case "ou":
                type = Arrays.stream(values).filter(v -> !COMMON_OBJECT_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
				                        	.filter(v -> !ORGANIZATION_UNIT_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
				                        	.collect(Collectors.joining(","));
                type = type.replace("organizationalUnit", "Unit")
			               .replace("organization", "Organization");
                break;
            case "cn":
            	type = Arrays.stream(values).filter(v -> !COMMON_OBJECT_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
            	                            .filter(v -> !FUNCTION_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
            	                            .filter(v -> !PERSON_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
            								.collect(Collectors.joining(","));
            	type = type.replace("organizationalRole", "Function");
            	type = type.replace("person", "Employee");
            	break;
            default:
                type = Arrays.stream(values).filter(v -> !OBJECT_CLASSES_TO_REMOVE.contains(v.toLowerCase()))
                        .collect(Collectors.joining());

        }
        return new String[]{type};
    }

    /**
     * Tranforms a ldap entry to JSON format used in PUT opera
     */
	@Override
	public String transformToUpdateJson(String dn, Collection<Attribute> attributes) {
	    JsonObjectBuilder updateBuilder = Json.createObjectBuilder();
	    updateBuilder.add("modificationType", "MERGE");
	    JsonObject object = transformEntryToPUTformat(dn, attributes);

	    updateBuilder.add(object.getString("type").toLowerCase(), object);
        return updateBuilder.build().toString();

	}

	/**
	 * transforms ldap entry to json format used in PUT rest calls.
	 */
    private JsonObject transformEntryToPUTformat(String dn, Collection<Attribute> attributes) {
        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
        for (Attribute attribute : attributes) {
            transformAttributeToJson(dn, jsonBuilder, attribute);
        }
        jsonBuilder.add("name", getRDN(dn));
        return jsonBuilder.build();
    }

    private String getRDN(String dn) {
        try {
            return DN.getRDNs(dn)[0].getAttributeValues()[0];
        } catch (LDAPException e) {
            System.out.println("This should not happen cause dn should be validated correctly in earlier step");
            e.printStackTrace();
        }
        return "";
    }

	private String mapAttributeName(String attributeName) {
		if (mobileTelephoneNumber.equalsIgnoreCase(attributeName)) {
		    attributeName = mobile;
		} else if (pagerTelephoneNumber.equalsIgnoreCase(attributeName)) {
		    attributeName = pager;
		} else if (streetAddress.equalsIgnoreCase(attributeName)) {
		    attributeName = street;
		}
		return attributeName;
	}

	/**
	 * Transforms path to a json Move operation.
new:
{
  "modificationType": "string",
  "newPath": "string",
}
     */
    @Override
    public String transformMoveToJson(String newPath) {
        JsonObjectBuilder modificationsBuilder = Json.createObjectBuilder();
        modificationsBuilder.add("modificationType", "MOVE");
        modificationsBuilder.add("newPath", newPath);

        return modificationsBuilder.build().toString();
    }

    /**
     * Transforms a ldap modify operation to patch format used in organization api.
     */
    @Override
    public String transformToPatchJson(String dn, List<Modification> modifications) {
        JsonObjectBuilder modificationsBuilder = Json.createObjectBuilder();
        JsonArrayBuilder createArrayBuilder = Json.createArrayBuilder();
        for (Modification modification : modifications) {
            JsonObjectBuilder aModBuilder = Json.createObjectBuilder();
            aModBuilder.add("modificationType", modification.getModificationType().getName());
            aModBuilder.add("attributeName", modification.getAttributeName());

            Attribute attribute = modification.getAttribute();
            if ( attribute.hasValue()) {
                transformAttributeToJson(dn, aModBuilder, attribute, true);
            }
            createArrayBuilder.add(aModBuilder);
        }
        modificationsBuilder.add("modifications", createArrayBuilder);
        return modificationsBuilder.build().toString();
    }

    /**
     * used for testing to set the schema.
     * @param schema
     */
    void setSchema(Schema schema) {
        this.schema = schema;
    }

}
