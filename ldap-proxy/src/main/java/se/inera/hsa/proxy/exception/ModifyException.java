package se.inera.hsa.proxy.exception;

public class ModifyException extends Exception {
    private int resultCode = -1;

    public ModifyException() {
    }

    public ModifyException(String message) {
        super(message);
    }
    public ModifyException(int resultCode) {
        super();
        this.resultCode=resultCode;
    }
    public ModifyException(String message, int resultCode) {
        super(message);
        this.resultCode=resultCode;
    }

    public ModifyException(Throwable cause) {
        super(cause);
    }
    public ModifyException(int resultCode, Throwable cause) {
        super(cause);
        this.resultCode=resultCode;
    }

    public ModifyException(String message, Throwable cause) {
        super(message, cause);
    }
   
    public ModifyException(String message, int resultCode, Throwable cause) {
        super(message, cause);
        this.resultCode=resultCode;
    }

    public int getResultCode() {
        return resultCode;
    }
}
