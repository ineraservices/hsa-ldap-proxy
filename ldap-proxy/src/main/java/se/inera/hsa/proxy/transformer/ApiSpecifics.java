package se.inera.hsa.proxy.transformer;

/**
 * This class contains constants used when calling the rest-api.
 */
public class ApiSpecifics {

    private ApiSpecifics() {
        //not used at the moment.
    }

    public static final String EQUALS = "=";
    public static final String FRONT_SLASH = "/";
    public static final String COMMA = ",";

    // used for v0.3 org api path
    public static final String COUNTRIES = "countries";
    public static final String COUNTIES = "counties";
    public static final String ORGANIZATIONS = "organizations";
    public static final String UNITS = "units";
    public static final String FUNCTIONS = "functions";
    public static final String EMPLOYEES = "employees";

    public static final String COUNTRY_RDN = "c";
    public static final String COUNTY_RDN = "l";
    public static final String ORGANIZATION_RDN = "o";
    public static final String UNIT_RDN = "ou";
    public static final String EMPLOYEE_RDN = "cn";
    public static final String FUNCTION_RDN = "cn";

}
