package se.inera.hsa.proxy;

import com.unboundid.ldap.listener.LDAPListener;
import com.unboundid.ldap.listener.LDAPListenerConfig;
import com.unboundid.ldif.LDIFException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.unboundid.ldap.listener.LDAPListener;
import com.unboundid.ldap.listener.LDAPListenerConfig;
import com.unboundid.ldif.LDIFException;

import java.io.IOException;

/**
 * Start point of LDAP-proxy. Initializes tha ldap-listener and request handler.
 * @author TESI
 *
 */
@WebListener
public class ProxyServletContextListener implements ServletContextListener {
    private static final Logger log = LogManager.getLogger(ProxyServletContextListener.class);
    static String version = "";

    @Inject
    private RequestProcessorImpl handler;

    private LDAPListener ldapListener;

    private ProxyLDAPListenerRequestHandler requestHandler;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            InputStream input = sce.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF");
            Manifest f = new Manifest(input);
            Attributes mainAttributes = f.getMainAttributes();
            version = mainAttributes.getValue("Implementation-Version");
            requestHandler = new ProxyLDAPListenerRequestHandler(handler, null);

            LDAPListenerConfig config = new LDAPListenerConfig(1389, requestHandler);

            //            LDAPListenerConfig config = new LDAPListenerConfig(1636, requestHandler);
//
//            // Create an SSLUtil instance that is configured to trust any certificate,
//            // and use it to create a socket factory.
//            try {
//                SSLUtil sslUtil = new SSLUtil(new TrustAllTrustManager());
//                SSLServerSocketFactory sslSocketFactory = sslUtil.createSSLServerSocketFactory();
//                config.setServerSocketFactory(sslSocketFactory);
//            } catch (GeneralSecurityException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
            ldapListener = new LDAPListener(config);
            ldapListener.setDaemon(true);
            ldapListener.startListening();
            System.out.println("hsa-ldap-proxy started");
            System.out.println(String.format("Listening on port %s", config.getListenPort()));
            log.info("log4j funkar!");
            log.debug("log4j funkar!");
            log.error("log4j funkar!");
            log.warn("log4j funkar!");
        } catch (IOException | LDIFException e) {
            e.printStackTrace();
            System.out.println("hsa-ldap-proxy failed to start");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (requestHandler != null ) requestHandler.closeInstance();
        ldapListener.shutDown(true);
        System.out.println("hsa-ldap-proxy stopped");
    }
}

