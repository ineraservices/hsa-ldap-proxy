package se.inera.hsa.proxy;

/**
 * Helper class with hsa objectClasses.
 */
public class ObjectClasses {
	private ObjectClasses(){}

	public static final String cRLDistributionPoint = "cRLDistributionPoint";
	public static final String country = "country";
	public static final String cybConfigurationGroup = "cybConfigurationGroup";
	public static final String domain = "domain";
	public static final String groupOfUniqueNames = "groupOfUniqueNames";
	public static final String hiddenObject = "hiddenObject";
	public static final String hsaAdminCommission = "hsaAdminCommission";
	public static final String hsaArchivedObject = "hsaArchivedObject";
	public static final String hsaCertifier = "hsaCertifier";
	public static final String hsaCodeTable = "hsaCodeTable";
	public static final String hsaCommission = "hsaCommission";
	public static final String hsaConfidentialPerson = "hsaConfidentialPerson";
	public static final String hsaContract = "hsaContract";
	public static final String hsaDeletedPersonWithValidCertificates = "hsaDeletedPersonWithValidCertificates";
	public static final String hsaDomain = "hsaDomain";
	public static final String hsaDomainArea = "hsaDomainArea";
	public static final String hsaFeignedDataObject = "hsaFeignedDataObject";
	public static final String hsaGln = "hsaGln";
	public static final String hsaGlnConsigneeAddress = "hsaGlnConsigneeAddress";
	public static final String hsaGlnDeliveryPlace = "hsaGlnDeliveryPlace";
	public static final String hsaGlnInvoiceAddress = "hsaGlnInvoiceAddress";
	public static final String hsaGlnPurchaser = "hsaGlnPurchaser";
	public static final String hsaGlnUnit = "hsaGlnUnit";
	public static final String hsaHealthCareOffering = "hsaHealthCareOffering";
	public static final String hsaHealthCareProvider = "hsaHealthCareProvider";
	public static final String hsaHealthCareUnit = "hsaHealthCareUnit";
	public static final String hsaInaccurateHCP = "hsaInaccurateHCP";
	public static final String hsaInaccurateHCU = "hsaInaccurateHCU";
	public static final String hsaLocalityExtension = "hsaLocalityExtension";
	public static final String HSAOrganizationExtension = "HSAOrganizationExtension";
	public static final String HSAPersonExtension = "HSAPersonExtension";
	public static final String HSARoleExtension = "HSARoleExtension";
	public static final String hsaServiceExtension = "hsaServiceExtension";
	public static final String hsaSosPerson = "hsaSosPerson";
	public static final String InetOrgPerson = "InetOrgPerson";
	public static final String ldapRootDSE = "ldapRootDSE";
	public static final String locality = "locality";
	public static final String organization = "organization";
	public static final String organizationalPerson = "organizationalPerson";
	public static final String organizationalRole = "organizationalRole";
	public static final String organizationalUnit = "organizationalUnit";
	public static final String person = "person";
	public static final String pkiCA = "pkiCA";
	public static final String pkiUser = "pkiUser";
	public static final String subEntry = "subEntry";
	public static final String top = "top";
}