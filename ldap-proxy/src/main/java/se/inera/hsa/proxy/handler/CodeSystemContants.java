package se.inera.hsa.proxy.handler;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class contains constants which are used when processing search request for the codeSystem api.
 */
public class CodeSystemContants {
    private CodeSystemContants() {
        //Empty for now.
    }

    public static final String DN_TO_KODER = "dc=koder,dc=Services,c=SE";
    public static final Map<String, String> CODE_SYSTEM_ID_TO_DN;

    static {
        Map<String, String> tmp = new HashMap<>();
        tmp.put("cn=Ägarform,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.14");
        tmp.put("cn=Befattning,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.4");
        tmp.put("cn=Chefskod,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.103");
        tmp.put("cn=Enhetstyp,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.12");
        tmp.put("cn=Finansierande landsting kommun,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.97");
        tmp.put("cn=Godkända system-id,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.98");
        tmp.put("cn=Kommunkod,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.17");
        tmp.put("cn=Länskod,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.18");
        tmp.put("cn=Legitimerad yrkesgrupp,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.6");
        tmp.put("cn=Medarbetaruppdragets ändamål,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.100");
        tmp.put("cn=Medarbetaruppdragets rättigheter Aktivitet,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.102");
        tmp.put("cn=Medarbetaruppdragets rättigheter Informationstyp,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.99");
        tmp.put("cn=Medarbetaruppdragets rättighetern organisationsomfång,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.101");
        tmp.put("cn=Reseverade funktionsnamn,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.99");
        tmp.put("cn=Utökad yrkeskod,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.96");
        tmp.put("cn=Vård- och omsorgsform,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.13");
        tmp.put("cn=Verksamhetskod,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.3");
        tmp.put("cn=Visas för,dc=koder,dc=Services,c=SE", "1.2.752.29.23.1.11");
        tmp.put("cn=Gruppförskrivarkod,dc=koder,dc=Services,c=SE", "1.2.752.129.2.2.1.95");
        CODE_SYSTEM_ID_TO_DN = Collections.unmodifiableMap(tmp);
    }
}
