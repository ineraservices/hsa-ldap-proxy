package se.inera.hsa.proxy.transformer;

import com.unboundid.ldap.sdk.Entry;

import java.util.Map;
import java.util.Optional;

/**
 * JSON to LDAP Entry transformer interface.
 */
public interface JsonTransformer {

    Optional<Entry> transformToEntry(Map<String, Object> json);
}
