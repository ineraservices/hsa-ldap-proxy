Number of Audit-files processed: 2666
        18: ABANDON
     25045: BIND
     21967: CL-IDLE-EXPIRED
        23: MODIFY: add: cardNumber, add: validNotBefore, add: serialNumber, add: userCertificate;binary, add: validNotAfter, add: hsaMifareSerialNumber
        13: MODIFY: add: hsaMifareSerialNumber
        15: MODIFY: add: hsaMifareSerialNumber, add: cardNumber
     23341: MODIFY: add: hsaMifareSerialNumber, add: userCertificate;binary, add: validNotBefore, add: serialNumber, add: cardNumber, add: validNotAfter
        17: MODIFY: add: serialNumber
       199: MODIFY: add: userCertificate;binary, add: cardNumber, add: hsaMifareSerialNumber, add: serialNumber, add: validNotBefore
        28: MODIFY: add: userCertificate;binary, add: cardNumber, add: serialNumber, add: validNotBefore
     26063: MODIFY: add: userCertificate;binary, add: serialNumber, add: validNotBefore
      2287: MODIFY: add: userCertificate;binary, add: validNotAfter, add: serialNumber, add: validNotBefore
       913: MODIFY: add: userCertificate;binary, add: validNotAfter, add: serialNumber, add: validNotBefore, add: cardNumber
         3: MODIFY: add: validNotBefore, add: hsaMifareSerialNumber, add: userCertificate;binary, add: validNotAfter, add: serialNumber
        32: MODIFY: add: validNotBefore, add: userCertificate;binary, add: serialNumber
         2: MODIFY: add: validNotBefore, add: userCertificate;binary, add: validNotAfter, add: serialNumber
     13874: MODIFY: delete: cardNumber
     13703: MODIFY: delete: hsaMifareSerialNumber
     24330: MODIFY: delete: serialNumber
     29002: MODIFY: delete: userCertificate;binary
     18941: MODIFY: delete: validNotAfter
     28963: MODIFY: delete: validNotBefore
   1818766: SEARCH: (!(hsaProtectedPerson=PRES))
       429: SEARCH: (&(&(objectClass=organizationalPerson)(!(hsaProtectedPerson=PRES)))(!(personalIdentityNumber=xxxxx))(hsaPassportNumber=xxxxx))
        30: SEARCH: (&(&(objectClass=organizationalPerson)(!(hsaProtectedPerson=PRES)))(personalIdentityNumber=xxxxx))
       426: SEARCH: (&(&(objectClass=organizationalPerson)(!(hsaProtectedPerson=PRES))))
         9: SEARCH: (&(objectClass=domain)(dc=Nod1))
        12: SEARCH: (&(objectClass=domain)(dc=Services))
         2: SEARCH: (&(objectClass=domain)(dc=nod1))
         2: SEARCH: (&(objectClass=domain)(dc=services))
       724: SEARCH: (&(objectClass=locality)(l=xxxxx))
    117796: SEARCH: (&(objectClass=organizationalPerson)(!(hsaProtectedPerson=PRES)))
         3: SEARCH: (&(objectClass=organizationalPerson)(!(personalIdentityNumber=xxxxx))(hsaPassportNumber=xxxxx))
      3616: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(hsaPassportNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
        11: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         4: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
      4817: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx)(!(hsaProtectedPerson=PRES)))
        11: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
        25: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         3: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
        22: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx))
       118: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx))(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx)(sn=xxxxx))(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         6: SEARCH: (&(objectClass=organizationalPerson)(givenName=xxxxx))
     26933: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(!(hsaProtectedPerson=PRES)))
        10: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
        17: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(sn=xxxxx)(!(hsaProtectedPerson=PRES)))
         4: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(sn=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         4: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         5: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         4: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(mail=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(mail=xxxxx)(givenName=xxxxx)(sn=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         5: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(mail=xxxxx)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(mail=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
        48: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         9: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx)(sn=xxxxx)(!(hsaProtectedPerson=PRES)))
        69: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx))
         3: SEARCH: (&(objectClass=organizationalPerson)(hsaIdentity=xxxxx))(!(hsaProtectedPerson=PRES)))
        92: SEARCH: (&(objectClass=organizationalPerson)(hsaPassportNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(hsaPassportNumber=xxxxx))
      2136: SEARCH: (&(objectClass=organizationalPerson)(hsaProtectedPerson=PRES))
        16: SEARCH: (&(objectClass=organizationalPerson)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(hsaTitle=xxxxx)(hsaPassportNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
       132: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(!(hsaProtectedPerson=PRES)))
         5: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(givenName=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(givenName=xxxxx)(sn=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(givenName=xxxxx)(sn=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(hsaPassportNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         1: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx)(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(mail=xxxxx))
     57272: SEARCH: (&(objectClass=organizationalPerson)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         2: SEARCH: (&(objectClass=organizationalPerson)(personalIdentityNumber=xxxxx)(hsaPassportNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
         8: SEARCH: (&(objectClass=organizationalPerson)(personalIdentityNumber=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
       157: SEARCH: (&(objectClass=organizationalPerson)(personalIdentityNumber=xxxxx))
      5196: SEARCH: (&(objectClass=organizationalPerson)(sn=xxxxx)(!(hsaProtectedPerson=PRES)))
         6: SEARCH: (&(objectClass=organizationalPerson)(sn=xxxxx)(hsaTitle=xxxxx)(!(hsaProtectedPerson=PRES)))
        11: SEARCH: (&(objectClass=organizationalPerson)(sn=xxxxx)(personalIdentityNumber=xxxxx)(!(hsaProtectedPerson=PRES)))
        17: SEARCH: (&(objectClass=organizationalPerson)(sn=xxxxx))
         3: SEARCH: (&(objectClass=organizationalPerson))
       300: SEARCH: (&(objectClass=organizationalRole)(cn=xxxxx))
       666: SEARCH: (&(objectClass=organizationalRole)(hsaIdentity=xxxxx))
         1: SEARCH: (&(objectClass=organizationalRole)(mail=xxxxx))
     75500: SEARCH: (&(objectClass=person)(hsaIdentity=xxxxx))
      1797: SEARCH: (&(objectClass=person)(personalIdentityNumber=xxxxx))
      2136: SEARCH: (&(|(objectClass=country)(objectClass=locality)(objectClass=organization)(objectClass=organizationalUnit))(|(&(createTimestamp>=xxxxx)(createTimestamp<=xxxxx))(&(modifyTimestamp>=xxxxx)(modifyTimestamp<=xxxxx))))
     11797: SEARCH: (&(|(objectClass=organization)(objectClass=organizationalUnit))(hsaIdentity=xxxxx))
    732865: SEARCH: (hsaIdentity=xxxxx)
   9124089: SEARCH: (objectClass=PRES)
      1078: SEARCH: (objectClass=organizationalPerson)
        19: SEARCH: (|(&(objectClass=organization)(o=xxxxx))(&(objectClass=organizationalUnit)(ou=xxxxx)))
        16: SEARCH: (|(&(objectClass=organization)(o=xxxxx))(&(objectClass=organizationalUnit)(ou=xxxxx))))
      2136: SEARCH: (|(objectClass=country)(objectClass=locality)(objectClass=organization)(objectClass=organizationalUnit))
    206352: SEARCH: (|(objectClass=organization)(objectClass=organizationalUnit)(objectClass=locality)(objectClass=country))
       815: UNBIND
      2258: UNKNOWN
Summa HSA-anrop: 12429619

---------------------------------------------------
---------------------------------------------------

Om den här filen

fil med alla anrop som SITHS_user gjort mot HSA Prod via Ldap under oktober nu i år (som sökts ut från våra skarpa Audit-loggar).
Alla sök-värden är maskade med xxxxx och varje operation summerad på en rad, men det ska förhoppningsvis kunna vara av viss hjälp för att kunna göra bedömning av vilken typ av operationer som SITHS gör.
 
Maskade värden kan även bestå av ”wildcards”-sökvillkor, som det skarpa exemplet nedan (där jag bara ersatt sökvärdena med ’abc’, ’def’ resp. ’ghi’):
 
----------------- OPERATION 000340 ----------------
  Create Time    :Mon Oct 15 10:03:17.443255 2018
  Start Time     :Mon Oct 15 10:03:17.443286 2018
  End Time       :Mon Oct 15 10:03:17.453146 2018
  OpUUID         :66a70c21-2f2b-421f-b3b1-a7f057f5f9fb
  DapBindId      :fd4b001f
  Concurrency    :2
  OpStackSize    :1
  OpFlow In/Out  :0/0
  Duration       :0.009860 sec
  User           :cn=siths_admin_user, o=users, dc=ExternalSystems, dc=Services, c=SE
  IP+Port+Sd     :[82.136.160.41]+14520+30
  Op-Name        :LDAP_Con4869982_Op8713
  Operation      :SEARCH
  Version        :3
  MessageID      :59073
  Base Obj       :c=se
  Scope          :subtree
  Filter         :(&(objectClass=organizationalPerson)(givenName=INI(abc))(sn=ANY(def)FIN(ghi))(!(hsaProtectedPerson=PRES)))
  Size Limit     :10000
  Time Limit     :60
  Deref Alias    :never
  Types Only     :no
  Req Attr #     :14
    Req Attr     :mail
    Req Attr     :endDate
    Req Attr     :givenName
    Req Attr     :hsaIdentity
    Req Attr     :hsaPassportNumber
    Req Attr     :hsaProtectedPerson
    Req Attr     :hsaTitle
    Req Attr     :middleName
    Req Attr     :objectClass
    Req Attr     :personalIdentityNumber
    Req Attr     :startDate
    Req Attr     :sn
    Req Attr     :title
    Req Attr     :userPrincipalName
  Found Entries  :0
  Bytes Received :355
  Bytes Returned :105
  Socket Mode    :ssl
  Cached Result  :no
  Abandoned      :no
  Result Code    :0 (success)
  Error Message  :Search succeeded. Found 0 Entries (0 Aliases), 0 Attributes, 0 Values. (ChainedResult=no)
 

