# language: sv
@ORGANISATION
@UPDATE
Egenskap: Gränssnittet ska erbjuda möjligheter att uppdatera organisationer i HSA.
  Testerna försöker lägga till, ta bort och uppdatera olika attribute på organisations
  objekt för att säkerställa att rätt attribute går att modifiera och andra förbjudna
  inte går att modifiera.
#KravID: HOEF-001
  Scenario: Som en användare behörig ska jag inte kunna ta bort obligatoriska attribut på organisation
    Givet att jag är en "användare behörig" att "uppdatera en organisation" under ett specifikt "län"
    Och det finns "organisationen Nordic MedTest" under ett "län"
    När jag "tar bort" "organisationens mail" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "organisationens postalAddress" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "organisationens telefonnummer" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "organisationens orgNummer" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "organisationens hsaIdentity" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "organisationens namn" från "organisationen"
    Så ska systemet svara med felet "not allowed on RDN"
    När jag "tar bort" "organisationens hsaHpt" från "organisationen"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" organisationen på rätt plats
    Och organisationen ska innehålla "alla Nordic MedTest attribute"
  @WIP
#KravID: HOEF-001
  Scenario: Som en användare behörig ska jag kunna ta bort ej obligatoriska attribut på organisation
    Givet att jag är en "användare behörig" att "uppdatera en organisation" under ett specifikt "län"
    Och det finns "organisationen Nordic MedTest" under ett "län"
    När jag "tar bort" "organisationens kommuntillhörighet" från "organisationen"
    Så ska systemet svara att operationen gått bra
    När jag "tar bort" "organisationens länskod" från "organisationen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" organisationen på rätt plats
    Och organisationen ska inte innehålla "kommuntillhörighet"
    Och organisationen ska inte innehålla "länskod"
#KravID: HOEF-001
  Scenario: Som en användare behörig ska jag kunna uppdatera attribut på organisation
    Givet att jag är en "användare behörig" att "uppdatera en organisation" under ett specifikt "län"
    Och det finns "organisationen Nordic MedTest" under ett "län"
    När jag "uppdaterar" "mail" på "organisationen"
    Så ska systemet svara att operationen gått bra
    När jag "uppdaterar" "orgNo" på "organisationen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" organisationen på rätt plats
    Och organisationen ska innehålla "mail"
    Och organisationen ska innehålla "orgNo"
#KravID: HOEF-001
  Scenario: Som en användare behörig ska jag kunna lägga till attribut på organisation
    Givet att jag är en "användare behörig" att "uppdatera en organisation" under ett specifikt "län"
    Och det finns "organisationen Nordic MedTest" under ett "län"
    När jag "lägger till" "extra telefonnummer" till "organisationen"
    Så ska systemet svara att operationen gått bra
    När jag "lägger till" "mobilnummer" till "organisationen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" organisationen på rätt plats
    Och enheten ska innehålla "extra telefonnummer"
    Och enheten ska innehålla "mobilnummer"
