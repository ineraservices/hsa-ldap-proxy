# language: sv
@WIP
Egenskap: Gränssnittet ska erbjuda möjligheter att lägga till organisation i HSA
  # HSA-124 
  Scenario: Som en administratör ska jag inte kunna lägga till ett nytt län
  Givet att jag är "användare" och roten SE finns skapad
    När jag försöker skapa ett län
    Så ska systemet svara vad som är fel
    Och ska jag hindras från att skapa ett län

    Scenario: Som administratör ska jag inte kunna lägga till ett nytt län på min organisation
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    När jag försöker skapa ett län på organisationen
    Så ska systemet svara vad som är fel
    Och ska jag hindras från att skapa ett län
   