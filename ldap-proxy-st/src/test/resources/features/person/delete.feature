# language: sv
@DELETE
@PERSON
Egenskap: Gränssnittet ska erbjuda möjligheter att ta bort person i HSA under förutsättningar att LDAP schema och verksamhetsregler godkänner att så får ske.
#KravId: SOTP-003
  Scenariomall: Som en användare behörig ska jag kunna ta bort en person under en organisation
    Givet att jag är en "användare behörig" att "ta bort en person" under en specifik "<överordnad>"
    Och det finns "en person med personnummer med alla obligatoriska attribut och objektklasser" under en "<överordnad>"
    När jag tar bort "en person med personnummer med alla obligatoriska attribut och objektklasser" under "<överordnad>"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" personen någonstans

   Exempel:
    | överordnad   |
    | organisation |
    | enhet        |

@NOTIMPLEMENTED
  #KravId: HANP-001
  Scenariomall: Som en användare behörig ska jag inte kunna ta bort mig själv
    Givet att jag är en "användare behörig" att "ta bort en person" under en specifik "<överordnad>"
    Och det finns "en person som är jag själv" under en "<överordnad>"
    När jag tar bort "en person som är jag själv" under "<överordnad>"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "alla obligatoriska attribut och objektklasser"
    Och personen ska innehålla "genererade attribut"

   Exempel:
    | överordnad   |
    | organisation |
    | enhet        |

@NOTIMPLEMENTED
  #KravId: SOTP-003
  Scenario: Som en användare utan behörighet ska jag inte kunna ta bort en person
    Givet att jag är en "användare obehörig" att "ta bort en person" under en specifik "organisation"
    Och det finns "en person med personnummer med alla obligatoriska attribut och objektklasser" under en "organisation"
    När jag tar bort "en person med personnummer med alla obligatoriska attribut och objektklasser" under "organisation"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "alla obligatoriska attribut och objektklasser"
    Och personen ska innehålla "genererade attribut"

