# language: sv


Egenskap: Gränssnittet ska erbjuda möjligheter att uppdatera personer i HSA.
  Testerna försöker lägga till, ta bort och uppdatera olika attribute på person
  objekt för att säkerställa att rätt attribute går att modifiera och andra förbjudna
  inte går att modifiera.

  @UPDATE @PERSON
  Scenario: Som en användare behörig ska jag inte kunna ta bort obligatoriska attribut på en person
    Givet att jag är en "användare behörig" att "uppdatera en person" under ett specifikt "län"
    Och det finns "personen med alla attribut" under ett "län"
    När jag "tar bort" "personens hsaIdentity" från "personen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "personens efternamn" från "personen"
    Så ska systemet svara med felet "object class violation"
    När jag "tar bort" "personens namn" från "personen"
    Så ska systemet svara med felet "not allowed on RDN"
    När jag "tar bort" "personens hela namn" från "personen"
    Så ska systemet svara med felet "object class violation"

  @UPDATE @PERSON
    #KravID: HANP-004
  Scenario: Som en användare behörig ska jag kunna uppdatera personens samordningsnummer till ett personnummer
    Givet att jag är en "användare behörig" att "uppdatera en person" under ett specifikt "län"
    Och det finns "person med samordningsnummer" under ett "län"
    När jag "uppdaterar" "personens samordningsnummer" på "personen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "personnummer"

  @UPDATE @PERSON
    #KravID: HANP-001
  Scenario: Som en användare behörig ska jag kunna uppdatera personens passuppgifter till person-id
    Givet att jag är en "användare behörig" att "uppdatera en person" under ett specifikt "län"
    Och det finns "person med passnummer" under ett län
    Och jag "tar bort" "personens passnummer" från personen
    Och jag "lägger till" "personens personnummer" till personen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "personnummer"

  @UPDATE @PERSON
    #KravID: HANP-005
  Scenario: Som en användare behörig ska jag kunna uppdatera personens HOSP-information
    Givet att jag är en "användare behörig" att "uppdatera en person" under ett specifikt "län"
    Och det finns "personen med HOSP info" under ett län
    När jag "uppdaterar" "förskrivningsrätt för barnmorskor" på "personen"
    Och jag "uppdaterar" "leg.yrkesgrupp och specialitet för läkare och tandläkare" på personen
    Och jag "uppdaterar" "förskrivarkod" på personen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "förskrivningsrätt för barnmorskor"
    Och personen ska innehålla "hsaSosTitleCodeSpeciality"
    Och personen ska innehålla "förskrivarkod"

  @UPDATE @PERSON
    #KravID: HANP-002
  Scenario: Som användare behörig ska jag kunna flytta en person mellan två platser inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "personen med alla attribut" under ett län
    När jag "flyttar" "personen med alla attribut" till "enhet med underordnade objekt"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen under "enhet med underordnade objekt"

  @UPDATE @PERSON
    #KravID: HANP-003
  Scenario: Som användare behörig ska jag kunna uppdatera namn och andra uppgifter från befolkningsregistret på en person
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "personen med alla attribut" under ett län
    Och jag "uppdaterar" "personnummer" på "personen"
    Så ska systemet svara att operationen gått bra
    Och jag "uppdaterar" "tilltalsnamn" på "personen"
    Så ska systemet svara att operationen gått bra
    Och jag "uppdaterar" "mellannamn" på "personen"
    Så ska systemet svara att operationen gått bra
    Och jag "uppdaterar" "efternamn" på "personen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "personnummer"
    Och personen ska innehålla "tilltalsnamn"
    Och personen ska innehålla "mellannamn"
    Och personen ska innehålla "efternamn"

  @UPDATE @PERSON
    #KravID: HANP-003
  Scenario: Som användare behörig ska jag kunna uppdatera CN när personen bytt namn
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "personen med alla attribut" under ett län
    Och jag uppdaterar CN på "personen med nytt namn"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "CN"


  @WIP
    #hsaConfidentialPerson är inte utvecklat när detta skrivs  av denna anledning har inte detta test kunnat provköras med med mer än hsaProtectedPerson
    #KravID: HANP-003
  Scenario: Som användare behörig ska jag kunna uppdatera en person till att bli skyddad person
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "icke skyddad person" under ett län
    Och jag "lägger till" "skyddad identitet" till "personen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "skyddad person"

  @UPDATE @PERSON
    #KravID: HANP-003
  Scenario: Som användare behörig ska jag kunna uppdatera en person med samordningsnummer till att vara styrkt och sakna svensk folkbokföringsadress
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "person med samordningsnummer" under ett län
    Och jag "lägger till" "styrkt samordningsnummer" till "personen"
    Och jag "lägger till" "saknar svensk folkbokföringsadress" till "personen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "styrkt samordningsnummer"
    Och personen ska innehålla "saknar svensk folkbokföringsadress"

  @UPDATE @PERSON
    #KravID: HANP-002
  Scenario: Som användare behörig ska jag inte kunna flytta en person till en enhet som har en person med samma namn trots att det är inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "person med samma namn" under ett län
    Och det finns "underenhet" under en "organisation"
    När jag "flyttar" "person med samma namn" till "underenhet"
    Så ska systemet svara med felet "entry already exists"
    Och ska jag "inte hitta" personen på rätt plats

  @UPDATE @PERSON
    #KravID: HANP-002
  Scenario: Som användare behörig ska jag  kunna flytta en person till en enhet som har en person med samma namn om jag lägger till en initial för personen och är inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en person" under en specifik "organisation"
    Och det finns "person med samma namn" under ett län
    Och det finns "underenhet" under en "organisation"
    När jag lägger till Initial och flyttar personen till "underenhet"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats