# language: sv

@CREATE @PERSON

Egenskap: Gränssnittet ska erbjuda möjligheter att lägga till personer i HSA
  Testerna försöker lägga till olika personer på olika platser i katalogen autentiserade
  som olika användare. Målet är att trigga alla möjliga sätt (positiva som negativa) som
  gränssnittet kan svara på, inte att ha utömmande tester för behörigheter eller validering.
#KravID: SOTP-001, SOTP-002
  Scenariomall: Som en administratör ska jag kunna skapa en person
    Givet att jag är en "<behöriganvändare>" att "skapa en person" under ett specifikt "<objekt>"
    När jag skapar "en <person> med alla obligatoriska attribut och objektklasser" under "<objekt>"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och personen ska innehålla "personens alla obligatoriska attribut och objektklasser"
    Och personen ska innehålla "genererade attribut"
    Och personen ska innehålla "ett genererat unikt ID"
    Exempel:
    |behöriganvändare  |objekt              |person                         |
    |huvudadmin        |organisation        |person med personnummer        |
    |centraladmin      |enhet               |person med samordningsnummer   |
    |personadmin       |organisation        |person med passnummer          |
#KravID: SOTP-001
  Scenario: Som användare ska jag kunna skapa person med alla attribut
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    Och jag skapar "en person med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats
    Och funktionen ska innehålla "allt som skickades in"
#KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa person utan obligatoriska objektklasser
   Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
   När jag skapar "en person utan obligatoriska objektklasser" under organisationen
   Så ska systemet svara med felet "object class violation"
   Och ska jag "inte hitta" personen någonstans
#KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa person utan obligatoriska attribut
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    När jag skapar "en person utan obligatoriska attribut" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" personen någonstans

#Stämmer nedan med tanke på mjuk validering?
  #KravID: SOTP-001
   Scenario: Som användare ska jag inte kunna skapa en person med en objektklass som inte är tillåten
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    När jag skapar "en person med en objektklass som inte är tillåten för person" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" personen någonstans
#KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa en person om platsen inte utpekas rätt
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    Och jag skapar "en person med syntaxfel i DN" under organisationen
    Så ska systemet svara med felet "invalid DN syntax"
    Och ska jag "inte hitta" personen någonstans

#Stämmer nedan med tanke på mjuk validering?
  #KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa en person med ett attribut som har ett värde med felaktig syntax
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    När jag skapar "en person med ett otillåtet värde för ett attribut" under organisationen
    Så ska systemet svara med felet "invalid attribute syntax"
    Och ska jag "inte hitta" personen någonstans
#KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa person på en organisation som inte finns
    Givet att jag är en "användare"
    När jag skapar "en person under icke existerande organisation"
    Så ska systemet svara med felet "no such object"
    Och ska jag "inte hitta" personen någonstans
#KravID: SOTP-001
  Scenario: Som användare ska jag inte kunna skapa person under ett personsobjekt
    Givet att jag är en "användare"
    När jag skapar "en person under ett personsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" personen någonstans
#KravID: SOTP-001
  Scenario: Som användare ska jag kunna skapa en person trots att attribut som inte är definierat används
    Givet att jag är en "användare behörig" att "skapa en person" under en specifik "organisation"
    När jag skapar "en person med ett attribut som inte är definierat" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" personen på rätt plats

  @NOTIMPLEMENTED
    #KravID: SOTP-001
  Scenario: Som användare utan behörighet till organisationen ska jag inte kunna skapa person
    Givet att jag är en "användare obehörig" att "skapa en person" under en specifik "organisation"
    När jag skapar "en person med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "inte hitta" personen någonstans
    #systemet svarar success då behörighetsmodulen inte finns än


