# language: sv
@CREATE @COMMISSION

Egenskap: Målet är att trigga alla möjliga sätt (positiva som negativa) som
  gränssnittet kan svara på, inte att ha utömmande tester för behörigheter eller validering.

  @WIP
  Scenario: Som en huvudadministratör ska jag kunna skapa ett medarbetaruppdrag
    Givet att jag är "huvudadministratör"
    Och att att jag har en "vårdenhet"
    När jag skapar ett medarbetaruppdrag under vårdenheten
    Så ska jag kunna hitta medarbetaruppdraget under vårdenheten

  @WIP
  Scenariomall: Som en huvudadministratör ska jag kunna skapa ett administrativt uppdrag
    Givet att jag är "huvudadministratör"
    Och att det finns ett behörighetsområde med en behörighetsområdesegenskap
    Och det finns en "<objekt>" som jag är har rättigheter till
    När jag skapar ett administrativt uppdrag med den behörighetsområdesegenskapen under "<objekt>"
    Så ska jag kunna hitta det administrativa uppdraget under "<objekt>"

    Exempel:
      | objekt       |
      | organisation |
      | enhet        |


