# language: sv
@DELETE
@FUNKTION
Egenskap: Gränssnittet ska erbjuda möjligheter att ta bort funktionsobjekt i HSA under förutsättningar att LDAP schema och verksamhetsregler godkänner att så får ske.
#KravID: HOEF-013
  Scenario: Som en användare behörig ska jag kunna ta bort en funktion under en organisation
    Givet att jag är en "användare behörig" att "ta bort en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag tar bort "Nordic Function" under "organisation"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" funktionen någonstans
#KravID: HOEF-013
  Scenario: Som en användare behörig ska jag kunna ta bort en funktion under en enhet
    Givet att jag är en "användare behörig" att "ta bort en funktion" under en specifik "enhet"
    Och det finns "Nordic Unit Function" under en "enhet"
    När jag tar bort "Nordic Unit Function" under "enhet"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" funktionen någonstans

@NOTIMPLEMENTED
  #KravID: HOEF-013
  Scenario: Som en användare utan behörighet ska jag inte kunna ta bort en funktion
    Givet att jag är en "användare obehörig" att "ta bort en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag tar bort "Nordic Function" under "organisation"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "alla obligatoriska attribut och objektklasser"
    Och funktionen ska innehålla "genererade attribut"

