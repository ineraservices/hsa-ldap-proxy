# language: sv
@UPDATE
@FUNKTION
Egenskap: Gränssnittet ska erbjuda möjligheter att modifiera funktioner i HSA
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna lägga till en mail på en funktion
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "lägger till" "mail" till "funktionen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna lägga till mail om mail redan finns
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "en funktion med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "lägger till" "ny mail" till "funktionen"
    Så ska systemet svara med felet "constraint violation"
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska inte innehålla "ny mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ta bort mail om mail inte finns
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "tar bort" "mail" från "funktionen"
    Så ska systemet svara med felet "no such attribute"
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna ta bort mail på en funktion
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "en funktion med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "tar bort" "mail" från "funktionen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna uppdatera mail på en funktion om mail finns innan
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "en funktion med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "uppdaterar" "mail" på "funktionen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna uppdatera mail på en funktion även om mail inte finns innan
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "uppdaterar" "mail" på "funktionen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "mail"

@NOTIMPLEMENTED
  #KravID: HOEF-010
  Scenario: Som en användare utan behörighet ska jag inte kunna lägga till mail på en funktion
    Givet att jag är en "användare obehörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "lägger till" "mail" till "funktionen"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ändra på HSAID
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "uppdaterar" "ny hsaID" på "funktionen"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "samma HSAID som tidigare"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna lägga till fler telefonnummer på en funktion
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "en funktion med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "lägger till" "extra telefonnummer" till "funktionen"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "extra telefonnummer"
#HOEF-004
  Scenario: Som användare behörig ska jag kunna flytta en funktion mellan två platser inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "destinationsenhet vid move" under en "organisation"
    När jag "flyttar" "Nordic Function" till "destinationsenhet vid move"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
#HOEF-004
  Scenario: Som användare behörig ska jag inte kunna flytta en funktion om det blir namnkonflikt
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Unit Function" under en "Nordic Unit"
    Och det finns "en andra funktion med namnet Nordic Unit Function" under en "organisation"
    När jag "flyttar" "en andra funktion med namnet Nordic Unit Function" till "Nordic Unit"
    Så ska systemet svara med felet "entry already exists"
    Och ska jag "hitta" funktionen på rätt plats
#HOEF-004
  Scenario: Som användare behörig ska jag inte kunna flytta en funktion till DN som inte existerar
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "flyttar" "Nordic Function" till "en enhet med felaktig DN"
    Så ska systemet svara med felet "no such object"
    Och ska jag "hitta" funktionen på rätt plats
#HOEF-005
  Scenario: Som användare behörig ska jag kunna ändra namn på en funktion
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    När jag "namnändrar" "Nordic Function" till "nytt funktionsnamn"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
#HOEF-005
  Scenario: Som användare behörig ska jag inte kunna ändra namn på en funktion om det blir namnkonflikt
    Givet att jag är en "användare behörig" att "uppdatera en funktion" under en specifik "organisation"
    Och det finns "Nordic Function" under en "organisation"
    Och det finns "en andra funktion med namnet Nordic Unit Function" under en "organisation"
    När jag "namnändrar" "en andra funktion med namnet Nordic Unit Function" till "Nordic Function"
    Så ska systemet svara med felet "entry already exists"
    Och ska jag "hitta" funktionen på rätt plats
