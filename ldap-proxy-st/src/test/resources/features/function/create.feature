# language: sv
@CREATE
@FUNKTION
Egenskap: Gränssnittet ska erbjuda möjligheter att lägga till funktioner i HSA.
  Testerna försöker lägga till olika funktioner på olika platser i katalogen autentiserade
  som olika användare. Målet är att trigga alla möjliga sätt (positiva som negativa) som
  gränssnittet kan svara på, inte att ha utömmande tester för behörigheter eller validering.

#Scenariot är inte helt fungerande ännu eftersom det inte går att ta bort funktion ännu
#Det felar i after scenariot
#KravId: HOEF-003
Scenariomall: Som en användare ska jag kunna skapa funktion
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "<överordnad>"
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser" under "<överordnad>"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "funktionens alla obligatoriska attribut och objektklasser"
    Och funktionen ska innehålla "genererade attribut"
    
    Exempel:
    | överordnad   |
    | organisation |
    #| enhet        |
   # | län          |
    #osäker på om man ska kunna skapa funktion på län

#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion utan obligatoriska objektklasser
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion utan obligatoriska objektklasser" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" funktionen någonstans

#Testet nedan resulterade i unwilling to perform när jag tog bort cn
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion utan obligatoriska attribut
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion utan obligatoriska attribut" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" funktionen någonstans

@NOTIMPLEMENTED
#KravId: HOEF-003
Scenario: Som användare utan behörighet till organisationen ska jag inte kunna skapa funktion
    Givet att jag är en "användare obehörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "inte hitta" funktionen någonstans
    #systemet svarar success då behörighetsmodulen inte finns än
#KravId: HOEF-003
Scenario: Som användare ska jag kunna skapa funktion med alla attribut
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    Och jag skapar "en funktion med alla obligatoriska och möjliga attribut och objektklasser" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    Och funktionen ska innehålla "allt som skickades in"
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa en funktion som redan finns
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara med felet "entry already exists"
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa en funktion med en objektklass som inte är tillåten
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion med en objektklass som inte är tillåten för funktion" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" funktionen någonstans
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa en funktion om platsen inte utpekas rätt
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    Och jag skapar "en funktion med felaktigt DN" under organisationen
    Så ska systemet svara med felet "invalid DN syntax"
    Och ska jag "inte hitta" funktionen någonstans
#KravId: HOEF-003
Scenario: Som användare ska jag kunna skapa en funktion trots att attribut som inte är definierat används
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion med ett attribut som inte är definierat" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" funktionen på rätt plats
#  Här borde vi säkerställa att avvikelsen loggas i LDAP-proxyn
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion på en organisation som inte finns
    Givet att jag är en "användare"
    När jag skapar "en funktion under icke existerande organisation"
    Så ska systemet svara med felet "no such object"
    Och ska jag "inte hitta" funktionen någonstans

@MP
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion direkt under ett landsobjekt
    Givet att jag är en "användare"
    När jag skapar "en funktion direkt under ett landsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans

@MP
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion direkt under ett länsobjekt
    Givet att jag är en "användare"
    När jag skapar "en funktion direkt under ett länsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans

@NOTIMPLEMENTED
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion på ett under ett personsobjekt
    Givet att jag är en "användare"
    När jag skapar "en funktion under ett personsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans

@WIP
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa en funktion med ett attribut som har ett värde med felaktig syntax
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "organisation"
    När jag skapar "en funktion med ett otillåtet värde för ett attribut" under organisationen
    Så ska systemet svara med felet "invalid attribute syntax"
    Och ska jag "inte hitta" funktionen någonstans

@WIP
#KravId: HOEF-003
Scenario: Som användare ska jag kunna skapa funktion med Lokala attribut
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "<överordnad>"
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser och giltiga lokala attribut" under "<överordnad>"
    Så ska systemet svara att operationen gått bra
    Och jag kan hitta funktionen på rätt plats
    Och de lokala attributen hittas på funktionen
    #systemet svarar success

@WIP
#KravId: HOEF-003
Scenario: Som användare ska jag inte kunna skapa funktion med Lokala attribut som är giltiga för en annan organisation
    Givet att jag är en "användare behörig" att "skapa en funktion" under en specifik "<överordnad>"
    När jag skapar "en funktion med alla obligatoriska attribut och objektklasser och lokala attribut giltig för en annan organisation " under "<överordnad>" 
    Så ska systemet svara vad som är fel
    Och funktionen ska inte skapas
    #Systemet svarar undefinedAttributeType????