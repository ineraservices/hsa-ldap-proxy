# language: sv
@DELETE @ENHET
Egenskap: Gränssnittet ska erbjuda möjligheter att ta bort enheter i HSA under förutsättningar att LDAP schema och verksamhetsregler godkänner att så får ske.
#KravID: HOEF-013
  Scenariomall: Som en användare ska jag kunna ta bort enhet
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "<överordnad>"
    När jag tar bort "enhet utan underordnade objekt" under "<överordnad>"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" enheten någonstans
# If the delete operation completes successfully and the entry is removed,
# then the server should return a “success” result.

    Exempel:
    | överordnad   |
    | organisation |
    | enhet        |
    #| län          |
#KravID: HOEF-013
@NOTIMPLEMENTED
  Scenariomall: Som en användare utan behörighet ska jag inte kunna ta bort enhet
    Givet att jag är en "användare obehörig" att "ta bort en enhet" under en specifik "<överordnad>"
    När jag tar bort "enhet utan underordnade objekt" under "<överordnad>"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" enheten på rätt plats
# No permission to delete should give response insufficient access

    Exempel:
    | överordnad   |
    | organisation |
    | enhet        |
    #| län          |

# Vårdgivare och Vårdenheter skall inte kunna tas bort via LDAP-proxy
#KravID: HOEF-014
  Scenario: Som en användare ska jag inte kunna ta bort vårdenhet
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "en enhet markerad som vårdenhet" under en "organisation"
    När jag tar bort "en enhet markerad som vårdenhet" under "organisation"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" enheten på rätt plats
    #Nedanstående rad är till enbart för att kunna städa databasen.
    När scenariot är klart behöver "vårdenhet" manuellt avmarkeras för att återställa
#KravID: HOEF-014
  Scenario: Som en användare ska jag inte kunna ta bort vårdgivare
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "en enhet markerad som vårdgivare" under en "organisation"
    När jag tar bort "en enhet markerad som vårdgivare" under "organisation"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" enheten på rätt plats
    #Nedanstående rad är till enbart för att kunna städa databasen.
    När scenariot är klart behöver "vårdgivare" manuellt avmarkeras för att återställa


# Enhet med underliggande objekt kan vara: administativt medarbetaruppdrag eller
# vårdmedarbetaruppdrag skall inte kunna tas bort via LDAP-proxy
@NOTIMPLEMENTED
  #KravID: HOEF-014
  Scenario: Som en användare ska jag inte kunna ta bort enhet med medarbetaruppdrag
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "medarbetaruppdrag" under en "enhet med underordnade objekt"
    När jag tar bort "enhet med underordnade objekt" under "organisation"
    Så ska systemet svara med felet "not allowed on non-leaf"
    Och ska jag "hitta" enheten på rätt plats
@NOTIMPLEMENTED
  #KravID: HOEF-014
  Scenario: Som en användare ska jag inte kunna ta bort enhet med administrtiva medarbetaruppdrag
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "administrativa medarbetaruppdrag" under en "enhet med underordnade objekt"
    När jag tar bort "enhet med underordnade objekt" under "organisation"
    Så ska systemet svara med felet "not allowed on non-leaf"
    Och ska jag "hitta" enheten på rätt plats

# Enhet med underliggande objekt kan vara: person, enhet, funktion
# skall inte kunna tas bort via LDAP-proxy
#KravID: HOEF-014
  Scenario: Som användare ska jag inte kunna ta bort enhet med underordnade enheter
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "en enhet underordnad annan enhet" under en "enhet med underordnade objekt"
    Och jag tar bort "enhet med underordnade objekt" under "organisation"
    Så ska systemet svara med felet "not allowed on non-leaf"
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-014
@NOTIMPLEMENTED
  Scenario: Som användare ska jag inte kunna ta bort enhet med underordnade person
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "person" under en "enhet med underordnade objekt"
    Och jag tar bort "enhet med underordnade objekt" under "organisation"
    Så ska systemet svara med felet "not allowed on non-leaf"
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-014
  Scenario: Som användare ska jag inte kunna ta bort enhet med underordnade funktion
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "funktion" under en "enhet med underordnade objekt"
    Och jag tar bort "enhet med underordnade objekt" under "organisation"
    Så ska systemet svara med felet "not allowed on non-leaf"
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-014
  Scenario: Som användare ska jag inte kunna ta bort enhet med felaktig dn
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och jag tar bort "en enhet med felaktig DN" under "organisation"
    Så ska systemet svara med felet "no such object"
# If the target entry does not exist, then the server should return a “noSuchObject” result.
#KravID: HOEF-014
  Scenario: Som användare ska jag inte kunna ta bort en enhet om platsen inte utpekas rätt
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och jag tar bort "en enhet med syntaxfel i DN" under organisationen
    Så ska systemet svara med felet "invalid DN syntax"

  Scenario: Som en användare ska jag kunna ta bort enhet med hiddenObject
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "enhet med hiddenObject" under en "organisation"
    När jag tar bort "enhet med hiddenObject" under "organisation"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" enheten någonstans

  Scenario: Som en användare ska jag kunna ta bort enhet med hsaFeignedDataObject
    Givet att jag är en "användare behörig" att "ta bort en enhet" under en specifik "organisation"
    Och det finns "enhet med hsaFeignedDataObject" under en "organisation"
    När jag tar bort "enhet med hsaFeignedDataObject" under "organisation"
    Så ska systemet svara att operationen gått bra
    Och ska jag "inte hitta" enheten någonstans
