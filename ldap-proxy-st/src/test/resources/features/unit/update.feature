# language: sv
@UPDATE
@ENHET
Egenskap: Gränssnittet ska erbjuda möjligheter att modifiera enheter i HSA
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna lägga till en mail på en enhet
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "lägger till" "mail" till "enheten"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna lägga till mail om mail redan finns
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "lägger till" "ny mail" till "enheten"
    Så ska systemet svara med felet "constraint violation"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska inte innehålla "ny mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ta bort mail om mail inte finns
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "tar bort" "mail" från "enheten"
    Så ska systemet svara med felet "no such attribute"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna ta bort mail på en enhet
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "tar bort" "mail" från "enheten"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna uppdatera mail på en enhet om mail finns innan
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "uppdaterar" "mail" på "enheten"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna uppdatera mail på en enhet även om mail inte finns innan
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "uppdaterar" "mail" på "enheten"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "mail"
#KravID: HOEF-006
  Scenario: Som en användare behörig ska jag kunna markera en enhet som vårdgivare
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "markerar" "enhet utan underordnade objekt" som "vårdgivare"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska vara "markerad som vårdgivare"

@WIP
  #KravID: HOEF-006
# Not working as the cleanup doesn't manage to remove the unit.
# Solve this by adding the unit in the database before test.
  Scenario: Som en användare behörig ska jag kunna avmarkera en enhet som vårdgivare
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet markerad som vårdgivare" under en "organisation"
    När jag "avmarkerar" "vårdgivare" från "en enhet markerad som vårdgivare"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska inte vara "markerad som vårdgivare"

@NOTIMPLEMENTED
  #KravID: HOEF-010
  Scenario: Som en användare utan behörighet ska jag inte kunna lägga till mail på en enhet
    Givet att jag är en "användare obehörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "lägger till" "mail" till "enheten"
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska inte innehålla "mail"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ändra på HSAID
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "uppdaterar" "ny hsaID" på "enheten"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "samma HSAID som tidigare"
#KravID: HOEF-010
  Scenario: Som en användare behörig ska jag kunna lägga till fler telefonnummer på en enhet
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under en "organisation"
    När jag "lägger till" "extra telefonnummer" till "enheten"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "extra telefonnummer"

  @NOTIMPLEMENTED
  #Indatakontroll behövs för att täcka detta fall.
    # KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ändra på organisationsnummer på vårdgivare
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet markerad som vårdgivare" under en "organisation"
    När jag "uppdaterar" "orgNo" på "enheten"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "samma orgNo som tidigare"
 # KravID: HOEF-010
  Scenario: Som en användare behörig ska jag inte kunna ta bort organisationsnummer på vårdgivare
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "en enhet markerad som vårdgivare" under en "organisation"
    När jag "tar bort" "orgNo på vårdgivare" på "enheten"
    Så ska systemet svara med felet "object class violation"
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "samma orgNo som tidigare"
    #Nedanstående rad är till enbart för att kunna städa databasen.
    När scenariot är klart behöver "vårdgivare" manuellt avmarkeras för att återställa
#KravID: HOEF-004
  Scenario: Som användare behörig ska jag kunna flytta en enhet mellan två platser inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "destinationsenhet vid move" under en "organisation"
    När jag "flyttar" "enhet utan underordnade objekt" till "destinationsenhet vid move"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-004
  Scenario: Som användare behörig ska jag inte kunna flytta en enhet om det blir namnkonflikt
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "underenhet" under en "enhet med underordnade objekt"
    Och det finns "en andra enhet med namnet underenhet" under en "organisation"
    När jag "flyttar" "en andra enhet med namnet underenhet" till "enhet med underordnade objekt"
    Så ska systemet svara med felet "entry already exists"
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-004
  Scenario: Som användare behörig ska jag inte kunna flytta en enhet till DN som inte existerar
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "flyttar" "enhet utan underordnade objekt" till "en enhet med felaktig DN"
    Så ska systemet svara med felet "no such object"
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-004
  Scenario: Som användare behörig ska jag kunna flytta ett delträd mellan två platser inom min organisation
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "destinationsenhet vid move" under en "organisation"
    När jag "flyttar" "enhet med underordnade objekt" till "destinationsenhet vid move"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-005
  Scenario: Som användare behörig ska jag kunna ändra namn på en enhet
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    När jag "namnändrar" "enhet utan underordnade objekt" till "nytt namn"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
#KravID: HOEF-005
  Scenario: Som användare behörig ska jag inte kunna ändra namn på en enhet om det blir namnkonflikt
    Givet att jag är en "användare behörig" att "uppdatera en enhet" under en specifik "organisation"
    Och det finns "enhet utan underordnade objekt" under en "organisation"
    Och det finns "enhet med underordnade objekt" under en "organisation"
    När jag "namnändrar" "enhet utan underordnade objekt" till "enhet med underordnade objekt"
    Så ska systemet svara med felet "entry already exists"
    Och ska jag "hitta" enheten på rätt plats

  @WIP
    #KravID: HOEF-008
  Scenariomall: Som användare ska jag kunna markera en enhet som felaktigt utpekad
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och Det finns en "enhet" under organisationen
    Så ska jag kunna peka ut "enhet" som felaktigt utpekad
    Och ska systemet svara att operationen gått bra
     
     Exempel:
    | enhet        |
    | vårdenhet    |
    | vårdgivare   |

  @WIP
    #KravID: HOEF-008
  Scenariomall: Som användare ska jag kunna arkivera en enhet
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och Det finns en "enhet" under organisationen som uppfyller kraven för arkivering
    Så ska jag kunna arkivera "enhet" 
    Och ska systemet svara att operationen gått bra
    Och ska jag hitta "enhet" som arkiverad
    
     Exempel:
    | enhet        |
    | vårdenhet    |
    | vårdgivare   |

