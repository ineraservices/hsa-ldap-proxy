# language: sv
@CREATE @ENHET
Egenskap: Gränssnittet ska erbjuda möjligheter att lägga till enheter i HSA.
  Testerna försöker lägga till olika enheter på olika platser i katalogen autentiserade
  som olika användare. Målet är att trigga alla möjliga sätt (positiva som negativa) som
  gränssnittet kan svara på, inte att ha utömmande tester för behörigheter eller validering.
#KravID: HOEF-002
  Scenariomall: Som en användare ska jag kunna skapa enhet
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "<överordnad>"
    När jag skapar "en enhet med alla obligatoriska attribut och objektklasser" under "<överordnad>"
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "enhetens alla obligatoriska attribut och objektklasser"
    Och enheten ska innehålla "genererade attribut"
    Och enheten ska innehålla "ett genererat unikt ID"

    Exempel:
    | överordnad   |
    | organisation |
    | enhet        |
 #   | län          |

  # Att testa att lägga till en enhet underordnad tre olika objekt har bedömts
  # vara tillräckligt i ett positivt test, så resterande tester i Egenskapen
  # testar endast att lägga till en enhet underordnad organisation.
  
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet utan obligatoriska objektklasser
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet utan obligatoriska objektklasser" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet utan obligatoriska attribut
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet utan obligatoriska attribut" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" enheten någonstans

@NOTIMPLEMENTED
  #KravID: HOEF-002
  Scenario: Som användare utan behörighet till organisationen ska jag inte kunna skapa enhet
    Givet att jag är en "användare obehörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara med felet "insufficient access"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag kunna skapa enhet med alla attribut
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med alla obligatoriska och möjliga attribut och objektklasser" under organisation
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "allt som skickades in"
    Och enheten ska innehålla "ett genererat unikt ID"
    Och enheten ska innehålla "genererade attribut"
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa en enhet som redan finns
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    När jag skapar "en enhet med alla obligatoriska attribut och objektklasser" under organisationen
    Så ska systemet svara med felet "entry already exists"
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa en enhet med en objektklass som inte är tillåten
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med en objektklass som inte är tillåten för enhet" under organisationen
    Så ska systemet svara med felet "object class violation"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa en enhet om platsen inte utpekas rätt
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    Och jag skapar "en enhet med syntaxfel i DN" under organisationen
    Så ska systemet svara med felet "invalid DN syntax"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag kunna skapa en enhet trots att attribut som inte är definierat används
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med ett attribut som inte är definierat" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
#  Här borde vi säkerställa att avvikelsen loggas i LDAP-proxyn
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet på en organisation som inte finns
    Givet att jag är en "användare"
    När jag skapar "en enhet under icke existerande organisation"
    Så ska systemet svara med felet "no such object"
    Och ska jag "inte hitta" enheten någonstans

@MP
  #KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet direkt under ett landsobjekt
    Givet att jag är en "användare"
    När jag skapar "en enhet direkt under ett landsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans

@MP
  #KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet direkt under ett länsobjekt
    Givet att jag är en "användare"
    När jag skapar "en enhet direkt under ett länsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans

@NOTIMPLEMENTED
  #KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa enhet på ett under ett personsobjekt
    Givet att jag är en "användare"
    När jag skapar "en enhet under ett personsobjekt"
    Så ska systemet svara med felet "naming violation"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag inte kunna skapa en enhet med ett attribut som har ett värde med felaktig syntax
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet med ett otillåtet värde för ett attribut" under organisationen
    Så ska systemet svara med felet "invalid attribute syntax"
    Och ska jag "inte hitta" enheten någonstans
#KravID: HOEF-002
  Scenario: Som användare ska jag kunna skapa enhet markerad som Vårdgivare
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet markerad som vårdgivare" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "allt som skickades in"
    #Nedanstående rad är till enbart för att kunna städa databasen.
    När scenariot är klart behöver "vårdgivare" manuellt avmarkeras för att återställa
#KravID: HOEF-002
  Scenario: Som användare ska jag kunna skapa enhet markerad som VårdEnhet
    Givet att jag är en "användare behörig" att "skapa en enhet" under en specifik "organisation"
    När jag skapar "en enhet markerad som vårdenhet" under organisationen
    Så ska systemet svara att operationen gått bra
    Och ska jag "hitta" enheten på rätt plats
    Och enheten ska innehålla "allt som skickades in"
    #Nedanstående rad är till enbart för att kunna städa databasen.
    När scenariot är klart behöver "vårdenhet" manuellt avmarkeras för att återställa

# Scenario när "katalogen" inte är tillgänglig
  #If the DN of the entry to add is not a naming context DN and the parent entry does not already exist, then it should fail with a “noSuchObject” result. If any of the ancestors of the target entry does exist, then the result may include a matched DN element with the DN of the most subordinate ancestor.

  #felhantering på denna sida: https://ldap.com/the-ldap-add-operation/

  #HSA-162

  @WIP
  Scenario: Som användare ska jag kunna skapa enhet med Lokala attribut
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och jag skapar en enhet med alla obligatoriska attribut och klasser och giltiga lokala 
    Så ska systemet svara att operationen gått bra
    Och jag kan hitta enheten på rätt plats
    Och de lokala attributen hittas på enheten
    #systemet svarar success
  @WIP
  Scenario: Som användare ska jag inte kunna skapa enhet med Lokala attribut som är giltiga för en annan organisation
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och jag skapar en enhet med alla obligatoriska attribut och objektklasser och lokala attribut tillhörande en annan organisation 
    Så ska systemet svara vad som är fel
    Och enheten ska inte skapas
    #Systemet svarar undefinedAttributeType????

  @WIP
  Scenario: Som användare ska jag inte kunna skapa enhet om en av attributen är felaktigt angivna
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och jag skapar en enhet med alla obligatoriska och möjliga attribut och objektklasser varav ett attribut är felaktigt angivet
    Så ska systemet svara vad som är fel
    Och enheten ska inte skapas
    #Systemet svarar undefinedAttributeType????

  @WIP
  Scenario: Som användare ska jag kunna skapa enhet som inte är Vårdgivare
    Givet att jag är "användare" och har behörighet till en specifik "organisation"
    Och jag skapar en enhet som inte har vårdgivartillhörighet
    Så ska systemet svara att operationen gått bra
    Och jag kan hitta enheten på rätt plats
   #systemet svarar success




