package se.inera.ldapclient;

import com.unboundid.ldap.sdk.*;
import se.inera.systemtest.BaseTest;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class LDAPTestClient {

    public SearchResult search(LDAPTestUser user, SearchRequest searchRequest) throws LDAPException {
        try (LDAPConnection connection = getConnection(user)) {
            return connection.search(searchRequest);
        }
    }

    private LDAPConnection getConnection(LDAPTestUser user) throws LDAPException {
        Properties props = new Properties();
        try {
            props.load(BaseTest.class.getResourceAsStream("test.properties"));
        } catch (IOException e) {
            fail("Unable to read test configuration: " + e.getMessage());
        }
        String host = System.getProperty("baseUri", props.getProperty("ldap-proxy.host"));
        int port = Integer.parseInt(System.getProperty("port", props.getProperty("ldap-proxy.port")));
        LDAPConnectionOptions options = new LDAPConnectionOptions();
        options.setResponseTimeoutMillis(4000);
        options.setConnectTimeoutMillis(4000);
        options.setAbandonOnTimeout(true);
        return new LDAPConnection(options, host, port, user.getBindDN(), user.getPassword());
    }

    public ResultCode add(LDAPTestUser user, Entry entry) throws LDAPException {
        try (LDAPConnection connection = getConnection(user)) {
            LDAPResult add = connection.add(entry);
            return add.getResultCode();
        }
    }

    public ResultCode remove(LDAPTestUser user, String dn) throws LDAPException {
        try (LDAPConnection connection = getConnection(user)) {
            LDAPResult result = connection.delete(dn);
            assertEquals("Could not result object with DN: '" + dn + "'.", ResultCode.SUCCESS, result.getResultCode());
            return result.getResultCode();
        }
    }

    public ResultCode modify(LDAPTestUser user, String dn, Modification[] mods) throws LDAPException {
        try (LDAPConnection connection = getConnection(user)) {
            LDAPResult result = connection.modify(dn, mods);
            assertEquals("Could not result object with DN: '" + dn + "'.", ResultCode.SUCCESS, result.getResultCode());
            return result.getResultCode();
        }
    }
    public ResultCode modify(LDAPTestUser user, String dn, List<Modification> mods) throws LDAPException {
         try (LDAPConnection connection = getConnection(user)) {
            LDAPResult result = connection.modify(dn, mods);
            assertEquals("Could not result object with DN: '" + dn + "'.", ResultCode.SUCCESS, result.getResultCode());
            return result.getResultCode();
        }
    }

    public ResultCode modifyDN(LDAPTestUser user, String dn, String newRDN, String newParentDN) throws LDAPException {
        ModifyDNRequest modifyDNRequest = new ModifyDNRequest(dn, newRDN, true, newParentDN);

        try (LDAPConnection connection = getConnection(user)) {
            LDAPResult result = connection.modifyDN(modifyDNRequest);
            assertEquals("Could not result object with new DN: '" + newParentDN + "'.", ResultCode.SUCCESS, result.getResultCode());
            return result.getResultCode();
        }
    }
}
