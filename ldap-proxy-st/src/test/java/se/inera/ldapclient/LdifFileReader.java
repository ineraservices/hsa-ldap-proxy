package se.inera.ldapclient;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldif.LDIFChangeRecord;
import com.unboundid.ldif.LDIFReader;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LdifFileReader {
    private static final boolean DEFAULT_ADD = true;

    /**
     * Load content of LDIF file to a list of LDIFChangeRecords
     */
    public static List<LDIFChangeRecord> getLdifChangeRecords(String ldifFileName) throws Exception {
        InputStream resourceAsStream = LdifFileReader.class.getClassLoader().getResourceAsStream(ldifFileName);
        if (resourceAsStream == null) {
            throw new FileNotFoundException("Should be able to load " + ldifFileName);
        }
        LDIFReader r = new LDIFReader(resourceAsStream);
        LDIFChangeRecord readEntry = null;
        List<LDIFChangeRecord> readEntries = new ArrayList<>();
        while ((readEntry = r.readChangeRecord(DEFAULT_ADD)) != null) {
            readEntries.add(readEntry);
        }
        resourceAsStream.close();
        return readEntries;
    }


    /**
     * Load content of LDIF file to a list of LDIFChangeRecords
     */
    public static List<Entry> getLdifEntries(String ldifFileName) throws Exception {
        InputStream resourceAsStream = LdifFileReader.class.getClassLoader().getResourceAsStream(ldifFileName);
        if (resourceAsStream == null) {
            throw new FileNotFoundException("Should be able to load " + ldifFileName);
        }
        LDIFReader r = new LDIFReader(resourceAsStream);
        Entry readEntry = null;
        List<Entry> readEntries = new ArrayList<>();
        while ((readEntry = r.readEntry()) != null) {
            readEntries.add(readEntry);
        }
        resourceAsStream.close();
        return readEntries;
    }
}
