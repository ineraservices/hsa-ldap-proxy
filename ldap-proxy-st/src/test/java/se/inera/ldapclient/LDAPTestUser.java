package se.inera.ldapclient;

public class LDAPTestUser {
    private String bindDN;
    private String password;

    public LDAPTestUser(String bindDN, String password) {
        this.bindDN = bindDN;
        this.password = password;
    }

    public String getBindDN() {
        return bindDN;
    }

    public void setBindDN(String bindDN) {
        this.bindDN = bindDN;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
