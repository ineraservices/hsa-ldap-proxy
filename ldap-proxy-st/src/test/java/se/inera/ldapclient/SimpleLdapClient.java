package se.inera.ldapclient;

import com.unboundid.ldap.sdk.*;
import com.unboundid.ldif.LDIFChangeRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SimpleLdapClient {

    public SimpleLdapClient(String name, String host, int port, String username, String password) throws LDAPException {
        ldapConnection = new LDAPConnection(host,  port, username, password);
        ldapConnection.setConnectionName(name);

        System.out.println("created connection: " + name);
    }

    private LDAPConnection ldapConnection;

    /**
     * Load an LDIF file and process it to an ldap connection
     */
    public List<LDAPResult> loadLdifFiles(String ldifFileName) throws Exception {
        List<LDAPResult> results = new ArrayList<>();
        for (LDIFChangeRecord ldifChangeRecord : LdifFileReader.getLdifChangeRecords(ldifFileName)) {
           /* SearchResultEntry entry = ldapConnection.getEntry(ldifChangeRecord.getDN());
            if(entry !=null)
              //ldapConnection.delete(entry.getDN()); // added this in place of deleteRecursive at BaseTest
                System.out.println("entry already exists: " + entry.getDN());
            */
            LDAPResult result = ldifChangeRecord.processChange(ldapConnection);
            results.add(result);
            System.out.println("loaded data to: " + ldapConnection.getConnectionName() + " dn: " + ldifChangeRecord.getDN());
        }

        return results;
    }

    public void close() {
        System.out.println("closed connection: " + ldapConnection.getConnectionName());
        ldapConnection.closeWithoutUnbind();
        ldapConnection = null;
    }

    public SearchResult search(SearchRequest request) throws LDAPSearchException {
        if(!ldapConnection.isConnected()) {
            try {
                ldapConnection.reconnect();
            } catch (LDAPException e) {
                e.printStackTrace();
            }

        }
        return ldapConnection.search(request);
    }
    
    public Entry getEntry(String dn) throws LDAPException {
        if(!ldapConnection.isConnected()) {
            try {
                ldapConnection.reconnect();
            } catch (LDAPException e) {
                e.printStackTrace();
            }
            
        }
        return ldapConnection.getEntry(dn);
    }

    public LDAPResult add(String dn, Collection<Attribute> attributes) throws LDAPException {
        return ldapConnection.add(dn, attributes);
    }

    public LDAPResult modify(ModifyRequest request) throws LDAPException {
        return ldapConnection.modify(request);
    }
    
    public LDAPResult modifyDn(ModifyDNRequest request) throws LDAPException {
        return ldapConnection.modifyDN(request);
    }

    public void deleteRecursive(String baseDn) throws LDAPException {
        SearchRequest request = new SearchRequest(baseDn, SearchScope.ONE, "(objectClass=*)");

        SearchResult result = search(request);
        for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
                deleteRecursive(searchResultEntry.getDN());
        }
        if (!baseDn.startsWith("c=") && !(baseDn.startsWith("l=")) && !(baseDn.startsWith("o="))) {
            ldapConnection.delete(baseDn);
            System.out.println("deleted from: " + ldapConnection.getConnectionName() + " dn: " + baseDn);
        }
    }
}