package se.inera.acceptancetest.model;

import com.unboundid.ldap.sdk.Attribute;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.fail;

public class Entry {
    private String name;
    private String dn;
    private List<String> classes = new ArrayList<>();
    private Map<String, String> attributes = new HashMap<>();

    public Entry withAttribute(String key, String value) {
        attributes.putIfAbsent(key, value);
        return this;
    }

    public Entry withClass(String className) {
        classes.add(className);
        return this;
    }

    public String getDn() {
        return dn;
    }

    public String getParentDn() {
        return dn.substring(dn.indexOf(','));
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public List<String> getClasses() {
        return classes;
    }

    public String getName() {
        return name;
    }

    public List<Attribute> getLdapAttributes() {

        return Stream.concat(
                classes.stream()
                    .map(className -> new Attribute("objectClass", className)),
                attributes.keySet().stream()
                    .map(this::toAttribute)
            )
            .collect(Collectors.toList());
    }

    private Attribute toAttribute(String key) {
        String value = attributes.get(key);
        final String BASE64_PREFIX = "base64:";
        if (value.startsWith(BASE64_PREFIX)) {
            String substring = value.substring(BASE64_PREFIX.length());
            try {
                return new Attribute(key, Base64.getDecoder().decode(substring));
            } catch (IllegalArgumentException e) {
                fail("Base64 decoding failed for key: '" + key + "'");
                return null;
            }
        } else {
            return new Attribute(key, value);
        }
    }

    public String createFilter(String attribute) {
        return "(" + attribute + "=" + attributes.get(attribute) + ")";
    }

    @Override
    public String toString() {
        return "Entry [name=" + name + ", dn=" + dn + ", classes=" + classes + ", attributes=" + attributes + "]";
    }
    
    
}
