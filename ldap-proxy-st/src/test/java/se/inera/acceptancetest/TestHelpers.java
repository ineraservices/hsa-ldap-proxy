package se.inera.acceptancetest;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.SearchResultEntry;
import se.inera.acceptancetest.model.Entry;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestHelpers {
	private static final String BASE64_PREFIX = "base64:";
    private static final String IS_BINARY_POSTFIX = ";binary";
    private static final String USER_CERT = "usercertificate";
    private static final String USER_PASSWORD = "userpassword";
    private static final String HSA_JPEG_LOGO = "hsajpeglogotype";
    private static final String JPEG_PHOTO = "jpegphoto";
    private static final String UUID_PATTERN = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";
    private static final String MAIL_PATTERN = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";

    public static void assertHasAttributes(SearchResultEntry entry, String... attributes) {
        for (String attribute : attributes) {
            if (!entry.hasAttribute(attribute)) {
                System.out.println("Entry does not contain attribute '" + attribute + "'. It contains these:");
                entry.getAttributes().forEach(System.out::println);
                fail("Entry does not contain attribute '" + attribute + "'.");
            }
            assertTrue(entry.hasAttribute(attribute));
        }
    }

    public static boolean isUUID(String uuid) {
        return uuid.matches(UUID_PATTERN);
    }

    public static boolean isMail(String mail) {
        return mail.matches(MAIL_PATTERN);
    }

    public static void assertEqualAttributes(SearchResultEntry actualResult, Entry expectedResult) {
        Map<String, String> expectedAttributes = expectedResult.getAttributes();
        ArrayList<Attribute> actualAttributes = new ArrayList<>(actualResult.getAttributes());

        expectedAttributes.keySet().forEach(key -> {
            Optional<String> actual = actualAttributes.stream()
                .filter(it -> it.getName().equals(key))
                .map(Attribute::getValue)
                .findFirst();

            if (actual.isPresent()) {
                String expected = expectedAttributes.get(key);
                if (isBinary(key)) {
                    if(expected.startsWith(BASE64_PREFIX)) {
                        expected = expected.substring(BASE64_PREFIX.length());
                    }
                    String convactual = base64Encode(actualResult.getAttribute(key).getValueByteArray());
                    assertEquals("Attribute " + key, expected, convactual);
                } else {
                    if (expected.startsWith(BASE64_PREFIX)) {
                        expected = new String(base64Decode(expected), StandardCharsets.UTF_8);
                    }
                    assertEquals("Attribute " + key, expected, actual.get());
                }
            } else {
                fail("Missing attribute '" + key + "'.");
            }
        });
    }

	private static byte[] base64Decode(String s) {
		return Base64.getDecoder().decode(s.substring(BASE64_PREFIX.length()));
	}

    private static String base64Encode(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

	private static boolean isBinary(String attributeName) {
        if (attributeName == null || attributeName.trim().isEmpty()) {
            return false;
        }
        if (attributeName.toLowerCase().endsWith(IS_BINARY_POSTFIX)) {
            return true;
        }
        switch (attributeName.toLowerCase()) {
            case USER_CERT:
            case USER_PASSWORD:
            case HSA_JPEG_LOGO:
            case JPEG_PHOTO:
                return true;
            default:
                return false;
        }
    }
}
