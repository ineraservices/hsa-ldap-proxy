package se.inera.acceptancetest;

import com.unboundid.ldap.sdk.*;
import se.inera.acceptancetest.model.Property;
import se.inera.acceptancetest.model.Entry;
import se.inera.acceptancetest.model.User;
import se.inera.ldapclient.LDAPTestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class TestApi {
    private LDAPTestClient client;
    private World world;

    private SearchResult result;
    private Map<String, User> users;
    private Map<String, Entry> entries;
    private Map<String, Property> properties;
    private LDAPException exception;

    public TestApi(LDAPTestClient client, World world) {
        this.client = client;
        this.world = world;
    }


    /**
     * Will search for the object at the exact location specified by the DN and nothing else.
     * @param baseDN The location at which to search
     * @return A single search result entry matching the given DN.
     */
    public SearchResultEntry searchByDN(String baseDN) {
        try {
            result = client.search(world.getUser(), new SearchRequest(baseDN, SearchScope.BASE, "(objectClass=*)"));
        } catch (LDAPException e) {
            if(e.getResultCode() != ResultCode.NO_SUCH_OBJECT) {
                exception = e;
            }
        }
        if (result != null && result.getEntryCount() >= 1) {
            return result.getSearchEntries().get(0);
        }
        return null;
    }

    /**
     * Will perform a complete search beginning at the root of the tree spanning all subordinates to any depth.
     * @param filter The filter to be used.
     * @return The result of the search
     */
    public SearchResult searchFromRoot(String filter) {
        try {
            result = client.search(world.getUser(), new SearchRequest("c=SE", SearchScope.SUB, filter, "+", "*"));
        } catch (LDAPException e) {
            exception = e;
        }
        return result;
    }

    public SearchResultEntry getLastSearchEntry() {
        if (result.getEntryCount() != 1) {
            fail("Last search did not return exactly one entry (" + result.getEntryCount() + ").");
        }
        return result.getSearchEntries().get(0);
    }

    public void loadTestData(Map<String, User> users, Map<String, Entry> entries, Map<String, Property> properties) {
        this.users = users;
        this.entries = entries;
        this.properties = properties;
    }

    public Entry getEntryData(String entryName) {
        Entry entry = entries.get(entryName);
        if (entry == null) {
            throw new IllegalArgumentException("The Entry '" + entryName + "' does not exist in the test data.");
        }
        return entry;
    }

    public Property getPropertyData(String propertyName) {
        Property property = properties.get(propertyName);
        if (property == null) {
            throw new IllegalArgumentException("The Entry '" + propertyName + "' does not exist in the test data.");
        }
        return property;
    }

    public User getUserData(String userName) {
        User user = users.get(userName);
        if (user == null) {
            throw new IllegalArgumentException("The username '" + userName + "' does not exist in the test data.");
        }
        return user;
    }

    public ResultCode add(Entry entry) {
        try {
            return client.add(world.getUser(), new com.unboundid.ldap.sdk.Entry(entry.getDn(), entry.getLdapAttributes()));
        } catch (LDAPException e) {
            exception = e;
        }
        return null;
    }

    public ResultCode remove(Entry entry) {
        try {
            return client.remove(world.getUser(), entry.getDn());
        } catch (LDAPException e) {
            exception = e;
        }
        return null;
    }

    public ResultCode modifyAdd(Entry entry, Property property) {
        ResultCode modResultCode;
        Modification mods[] = new Modification[property.getClasses().size() + property.getAttributes().size()];
        int modIdx = 0;

        //Prepare list of class additions
        if (!property.getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < property.getClasses().size(); classIndx++) {
                mods[modIdx] = new Modification(ModificationType.ADD, "objectClass", property.getClasses().get(classIndx));
                modIdx++;
            }
        }
        //Add list of attribute additions
        if (!property.getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(property.getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(property.getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                mods[modIdx] = new Modification(ModificationType.ADD, attributeList.get(attIdx), attributeValueList.get(attIdx));
                modIdx++;
            }
        }

        try {
            modResultCode = client.modify(world.getUser(), entry.getDn(), mods);
        } catch (LDAPException e) {
            modResultCode = e.getResultCode();
            exception = e;
        }
        return modResultCode;
    }

    public ResultCode modifyDelete(Entry entry, Property property) {
        ResultCode modResultCode;
        Modification mods[] = new Modification[property.getClasses().size() + property.getAttributes().size()];
        int modIdx = 0;

        //Prepare list of class delete
        if (!property.getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < property.getClasses().size(); classIndx++) {
                mods[modIdx] = new Modification(ModificationType.DELETE, "objectClass", property.getClasses().get(classIndx));
                modIdx++;
            }
        }
        //Add list of attribute delete
        if (!property.getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(property.getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(property.getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if(attributeValueList.get(attIdx) == null) {
                    mods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx));
                }
                else {
                    mods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx), attributeValueList.get(attIdx));
                }
                modIdx++;
            }
        }
        try {
            modResultCode = client.modify(world.getUser(), entry.getDn(), mods);
        } catch (LDAPException e) {
            modResultCode = e.getResultCode();
            exception = e;
        }
        return modResultCode;
    }

    public ResultCode modifyReplace(Entry entry, Property property) {
        ResultCode modResultCode;
        List<Modification> mods = new ArrayList<>();
        int modIdx = 0;

        //Prepare list of class additions
        if (!property.getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < property.getClasses().size(); classIndx++) {
                mods.add(new Modification(ModificationType.ADD, "objectClass", property.getClasses().get(classIndx)));
                modIdx++;
            }
        }
        //Add list of attribute replacements
        if (!property.getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(property.getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(property.getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if(entry.getAttributes().containsKey(attributeList.get(attIdx))) {
                    mods.add(new Modification(ModificationType.DELETE, attributeList.get(attIdx), entry.getAttributes().get(attributeList.get(attIdx))));
                }
                mods.add(new Modification(ModificationType.ADD, attributeList.get(attIdx), attributeValueList.get(attIdx)));
                modIdx++;
            }
        }

        try {
            modResultCode = client.modify(world.getUser(), entry.getDn(), mods);
        } catch (LDAPException e) {
            modResultCode = e.getResultCode();
            exception = e;
        }
        return modResultCode;
    }

    public ResultCode modifyRevert(Entry entry, Modification mods[]) {
        ResultCode modResultCode;
        try {
            modResultCode = client.modify(world.getUser(), entry.getDn(), mods);
        } catch (LDAPException e) {
            modResultCode = e.getResultCode();
            exception = e;
        }
        return modResultCode;
    }

    public ResultCode modifyDN(String RDN, String parentDN, String newRDN, String newParentDN) {
        ResultCode modDNResult;
        //Construct the full DN for entry to be moved/renamed
        String entryDN = RDN + parentDN;
        try {
            modDNResult = client.modifyDN(world.getUser(), entryDN, newRDN, newParentDN);
        } catch (LDAPException e) {
            modDNResult = e.getResultCode();
            exception = e;
        }
        return modDNResult;
    }

    public void assertNoExceptionCaught() {
        LDAPException currentException = getAndClearException();
        if (currentException != null) {
            System.out.println("Unexpected Exception caught:");
            System.out.println(currentException.getExceptionMessage());
        }
        assertNull(currentException);
    }

    public LDAPException getAndClearException() {
        LDAPException e = exception;
        exception = null;
        return e;
    }
}