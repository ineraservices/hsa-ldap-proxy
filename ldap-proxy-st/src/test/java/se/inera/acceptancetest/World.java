package se.inera.acceptancetest;

import com.unboundid.ldap.sdk.Modification;
import se.inera.acceptancetest.model.Entry;
import se.inera.acceptancetest.model.Property;
import se.inera.ldapclient.LDAPTestUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The World class is used to save data within a single Scenario-run.
 * The state is wiped clean before each scenario by Cucumber.
 */
public class World {
    private LDAPTestUser user;
    private Entry LDAPTestEntry;
    private Entry BeforeTestEntry;
    private boolean isEntryPreparedBefore;
    private boolean isEntryAdded;
    private boolean isEntryRemoved;
    private boolean isEntryMoved;
    private boolean isEntryRenamed;
    private boolean isEntryUnModified;
    private String  afterMoveParentDN;
    private String  afterRenameRDN;
    private List<Modification[]> modsList = new ArrayList<>();
    private Integer numModReverts;

    public World() {
        this.numModReverts = 0;
        this.isEntryUnModified = false;
    }

    /**
     * User
     */
    public LDAPTestUser getUser() {return user; }

    public void setUser(LDAPTestUser user) {this.user = user; }

    /**
     * Property
     */
    public void pushRevertMods(Modification Mods[]) {
        if(numModReverts >= 0) {
            modsList.add(numModReverts++,Mods);
        }
    }
    public void pushRevertMods(List<Modification> Mods) {
        if(numModReverts >= 0) {
            Modification[] modArray = (Modification[]) Mods.toArray(new Modification[Mods.size()]);
            modsList.add(numModReverts++,modArray);
        }
    }

    public Modification[] popRevertMods() {
        if(numModReverts > 0) {
            numModReverts--;
            return modsList.get(numModReverts);
        }
        return null;
    }

    public Integer getNumRevertMods() { return numModReverts; }

    /**
     * Entry
     */

    public void storeEntryBeforeTest(Entry entry){ this.BeforeTestEntry = entry;}

    public Entry getEntryFromBeforeTest(){return this.BeforeTestEntry;}

    public boolean isEntryMoved() {return isEntryMoved;}

    public void setEntryMoved(boolean entryMoved) {
        isEntryMoved = entryMoved;
    }

    public boolean isEntryRenamed() {return isEntryRenamed;}

    public void setEntryRenamed(boolean entryRenamed) {
        isEntryRenamed = entryRenamed;
    }

    public void setEntryParentDNafterMove(String parentDN){ this.afterMoveParentDN = parentDN; }

    public String getEntryParentDNafterMove(){return this.afterMoveParentDN; }

    public void setEntryRDNafterRename(String newRDN){ this.afterRenameRDN = newRDN; }

    public String getEntryRDNafterRename(){return this.afterRenameRDN; }

    public void setTestEntry(Entry entry){this.LDAPTestEntry = entry; }

    public Entry getTestEntry() {return this.LDAPTestEntry; }

    public boolean isEntryPreparedBefore() {
        return isEntryPreparedBefore;
    }

    public void setEntryPreparedBefore(boolean entryPreparedBefore) {
        isEntryPreparedBefore = entryPreparedBefore;
    }
    public boolean getEntryPreparedBefore() {
        return isEntryPreparedBefore;
    }
    public boolean isEntryAdded() {
        return isEntryAdded;
    }

    public void setEntryAdded(boolean entryAdded) {
        isEntryAdded = entryAdded;
    }

    public boolean isEntryRemoved() {
        return isEntryRemoved;
    }

    public void setEntryRemoved(boolean entryRemoved) {
        isEntryRemoved = entryRemoved;
    }

    public boolean getEntryUnModified() { return isEntryUnModified;
    }

    public void setEntryUnModified(boolean entryNotChanged) {   isEntryUnModified = entryNotChanged;
    }
}
