package se.inera.acceptancetest.stepdefs;

import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.ResultCode;
import cucumber.api.java.sv.När;
import cucumber.api.java.sv.Och;
import se.inera.acceptancetest.TestApi;
import se.inera.acceptancetest.World;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.fail;

public class Update {
    private final World world;
    private TestApi api;

    public Update(TestApi api, World world) {
        this.api = api;
        this.world = world;
    }

    @När("^jag \"markerar\" \"([^\"]*)\" som \"([^\"]*)\"$")
    public void jagmarkerar(String entryName, String property) {
        Modification revertmods[] = new Modification[api.getPropertyData(property).getClasses().size() + api.getPropertyData(property).getAttributes().size()];
        int modIdx = 0;
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "markerar" -> ADD
 */
        world.setTestEntry(api.getEntryData(entryName));

        //Prepare list of class to be reverted after test
        if (!api.getPropertyData(property).getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < api.getPropertyData(property).getClasses().size(); classIndx++) {
                revertmods[modIdx] = new Modification(ModificationType.DELETE, "objectClass", api.getPropertyData(property).getClasses().get(classIndx));
                modIdx++;
            }
        }
        //Add list of attribute to be reverted after test
        if (!api.getPropertyData(property).getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(api.getPropertyData(property).getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(api.getPropertyData(property).getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if(world.getTestEntry().getAttributes().get(attributeList.get(attIdx)) == null) {
                    revertmods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx));
                }
                else {
                    revertmods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx), attributeValueList.get(attIdx));
                }
                modIdx++;
            }
        }
        if(ResultCode.SUCCESS.equals(api.modifyAdd(api.getEntryData(entryName),api.getPropertyData(property)))) {
            world.pushRevertMods(revertmods);
        }
    }

    @När("^jag \"lägger till\" \"([^\"]*)\"(?: till \"?(?:[^\"]*)\"?)?$")
    public void jaglaggertill(String property) {
        Modification revertmods[] = new Modification[api.getPropertyData(property).getClasses().size() + api.getPropertyData(property).getAttributes().size()];
        int modIdx = 0;
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "lägger till" -> ADD
 */
        //Prepare list of class to be reverted after test
        if (!api.getPropertyData(property).getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < api.getPropertyData(property).getClasses().size(); classIndx++) {
                revertmods[modIdx] = new Modification(ModificationType.DELETE, "objectClass", api.getPropertyData(property).getClasses().get(classIndx));
                modIdx++;
            }
        }
        //Add list of attribute to be reverted after test
        if (!api.getPropertyData(property).getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(api.getPropertyData(property).getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(api.getPropertyData(property).getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if(world.getTestEntry().getAttributes().get(attributeList.get(attIdx)) == null) {
                    revertmods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx));
                }
                else {
                    revertmods[modIdx] = new Modification(ModificationType.DELETE, attributeList.get(attIdx), attributeValueList.get(attIdx));
                }
                modIdx++;
            }
        }
        if(ResultCode.SUCCESS.equals(api.modifyAdd(world.getTestEntry(),api.getPropertyData(property)))) {
            world.pushRevertMods(revertmods);
        }
    }

    @När("^jag \"(?:avmarkerar|tar bort)\" \"([^\"]*)\"(?: (?:som|från|på) \"?(?:[^\"]*)\"?)?$")
    public void jagavmarkerar(String property) {
        Modification revertmods[] = new Modification[api.getPropertyData(property).getClasses().size() + api.getPropertyData(property).getAttributes().size()];
        int modIdx = 0;
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "avmarkerar" -> DELETE
        "tar bort" -> DELETE
 */
        //Prepare list of class to be reverted after test
        if (!api.getPropertyData(property).getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < api.getPropertyData(property).getClasses().size(); classIndx++) {
                revertmods[modIdx] = new Modification(ModificationType.ADD, "objectClass", api.getPropertyData(property).getClasses().get(classIndx));
                modIdx++;
            }
        }
        //Add list of attribute to be reverted after test
        if (!api.getPropertyData(property).getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(api.getPropertyData(property).getAttributes().keySet());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if (world.getTestEntry().getAttributes().get(attributeList.get(attIdx)) != null) {
                    revertmods[modIdx++] = new Modification(ModificationType.ADD, attributeList.get(attIdx), world.getTestEntry().getAttributes().get(attributeList.get(attIdx)));
                }
            }
        }
        if(ResultCode.SUCCESS.equals(api.modifyDelete(world.getTestEntry(),api.getPropertyData(property)))) {
            world.pushRevertMods(revertmods);
        }
    }
    @När("^jag \"(?:uppdaterar|ersätter)\" \"([^\"]*)\"(?: på \"?(?:[^\"]*)\"?)?$")
    public void jaguppdaterar(String property) {
      //  Modification revertmods[] = new Modification[api.getPropertyData(property).getClasses().size() + api.getPropertyData(property).getAttributes().size()];
        List<Modification> revertmods = new ArrayList<>();
        int modIdx = 0;
       /* Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "uppdaterar" -> DELETE (old), ADD (new)
        "ersätter" -> DELETE (old), ADD (new)
*/

        //Prepare list of class to be reverted after test
        if (!api.getPropertyData(property).getClasses().isEmpty()) {
            for (int classIndx = 0; classIndx < api.getPropertyData(property).getClasses().size(); classIndx++) {
                revertmods.add(new Modification(ModificationType.DELETE, "objectClass", api.getPropertyData(property).getClasses().get(classIndx)));
                modIdx++;
            }
        }
        //Add list of attribute to be reverted after test
        if (!api.getPropertyData(property).getAttributes().isEmpty()) {
            ArrayList<String> attributeList = new ArrayList<>(api.getPropertyData(property).getAttributes().keySet());
            ArrayList<String> attributeValueList = new ArrayList<>(api.getPropertyData(property).getAttributes().values());

            for (int attIdx = 0;attIdx < attributeList.size(); attIdx++) {
                if(world.getTestEntry().getAttributes().get(attributeList.get(attIdx)) == null) {
                    revertmods.add(new Modification(ModificationType.DELETE, attributeList.get(attIdx)));
                }
                else {
                    Collection<String> values = api.getPropertyData(property).getAttributes().values();
                    String[] toBeDeletedValues = (String[]) values.toArray(new String[values.size()]);
                    revertmods.add(new Modification(ModificationType.DELETE, attributeList.get(attIdx), toBeDeletedValues));
                    revertmods.add(new Modification(ModificationType.ADD, attributeList.get(attIdx), world.getTestEntry().getAttributes().get(attributeList.get(attIdx))));
                }
                modIdx++;
            }
        }

        if(ResultCode.SUCCESS.equals(api.modifyReplace(world.getTestEntry(),api.getPropertyData(property)))) {
            world.pushRevertMods(revertmods);
        }
    }
    @När("^jag uppdaterar CN på \"([^\"]*)\"$")
    public void jaguppdaterarCN( String newDestEntry) {

        String entryName = "personen med alla attribut";
        String RDN = api.getEntryData(entryName).getDn().replace(api.getEntryData(newDestEntry).getParentDn(),"");
        String parentDN = api.getEntryData(entryName).getParentDn();
        String newRDN = api.getEntryData(newDestEntry).getDn().replace(api.getEntryData(newDestEntry).getParentDn(),"");
        String newParentDN = api.getEntryData(newDestEntry).getParentDn().substring(1); //Remove comma

        if(ResultCode.SUCCESS.equals(api.modifyDN(RDN, parentDN, newRDN, newParentDN))) {

            world.storeEntryBeforeTest(api.getEntryData(entryName));
            world.setEntryRDNafterRename(newRDN);
            world.setEntryRenamed(true);

        }
    }
    @När("jag lägger till Initial och flyttar personen till \"([^\"]*)\"$")
    public void jagLäggerTillInitialOchFlyttarPersonenTill(String newDestEntry1) {

        String entryName = "person med samma namn";
        String newDestEntry= "personen med initial";
        String RDN = api.getEntryData(entryName).getDn().replace(api.getEntryData(entryName).getParentDn(),"");
        String parentDN = api.getEntryData(entryName).getParentDn();
        String newRDN = api.getEntryData(newDestEntry).getDn().replace(api.getEntryData(newDestEntry).getParentDn(),"");
        String newParentDN = api.getEntryData(newDestEntry).getParentDn();
        if(ResultCode.SUCCESS.equals(api.remove(api.getEntryData(entryName)))){
            api.assertNoExceptionCaught();


            if(ResultCode.SUCCESS.equals(api.add(api.getEntryData(newDestEntry)))) {//api.modifyDN(RDN, parentDN, newRDN, newParentDN))) {
                if(world.isEntryPreparedBefore()){
                    world.setTestEntry(api.getEntryData(newDestEntry));
                    world.setEntryRDNafterRename(newRDN);
                    world.setEntryAdded(true);
                }
                else {
                    world.storeEntryBeforeTest(api.getEntryData(entryName));
                    world.setTestEntry(api.getEntryData(newDestEntry));
                    world.setEntryRDNafterRename(newRDN);
                    world.setEntryAdded(true);
                }
            }

        }
    }

    @När("^jag \"(?:räknar upp|räknar ner)\" \"([^\"]*)\" som \"([^\"]*)\"$")
    public void jagraknar(String entryName, String property) {
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "räknar upp" -> INCREMENT
        "räknar ner" -> INCREMENT
 */
        fail("LDAP ModificationType INCREMENT not supported by HSA");
    }

    @När("^jag \"flyttar\" \"([^\"]*)\" till \"([^\"]*)\"$")
    public void jagflyttar(String entryName, String newDestEntry) {
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "flyttar" -> modifyDN

 */
        String RDN = api.getEntryData(entryName).getDn().replace(api.getEntryData(entryName).getParentDn(),"");
        String parentDN = api.getEntryData(entryName).getParentDn();
        String newRDN = api.getEntryData(entryName).getDn().replace(api.getEntryData(entryName).getParentDn(),"");
        String newParentDN = api.getEntryData(newDestEntry).getDn();

        if(ResultCode.SUCCESS.equals(api.modifyDN(RDN, parentDN, newRDN, newParentDN))) {

            world.storeEntryBeforeTest(api.getEntryData(entryName));
            //.getParentDn always returns with a comma first -add
            world.setEntryParentDNafterMove("," + api.getEntryData(newDestEntry).getDn());
            world.setEntryMoved(true);
        }
        else if(world.getEntryPreparedBefore()){
                world.setEntryUnModified(true);
                world.storeEntryBeforeTest(api.getEntryData(entryName));
        }
    }

    @När("^jag \"namnändrar\" \"([^\"]*)\" till \"([^\"]*)\"$")
    public void jagnamnandrar(String entryName, String newDestEntry) {
/*      Uttryck mappning mot ModifyTypes som vi har tänkt använda i scenarior är:
        "namnändrar" -> modifyDN
 */
        String RDN = api.getEntryData(entryName).getDn().replace(api.getEntryData(entryName).getParentDn(),"");
        String parentDN = api.getEntryData(entryName).getParentDn();
        String newRDN = api.getEntryData(newDestEntry).getDn().replace(api.getEntryData(newDestEntry).getParentDn(),"");
        String newParentDN = api.getEntryData(newDestEntry).getParentDn().substring(1); //Remove comma

        if(ResultCode.SUCCESS.equals(api.modifyDN(RDN, parentDN, newRDN, newParentDN))) {
            world.storeEntryBeforeTest(api.getEntryData(entryName));
            //.getParentDn always returns with a comma first -add
            world.setEntryRDNafterRename(newRDN);
            world.setEntryRenamed(true);
        }
    }


}

