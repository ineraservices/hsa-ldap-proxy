package se.inera.acceptancetest.stepdefs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.unboundid.ldap.sdk.*;
import cucumber.api.java.After;
import cucumber.api.java.sv.Givet;
import cucumber.api.java.sv.När;
import cucumber.api.java.sv.Och;
import cucumber.api.java.sv.Så;
import se.inera.acceptancetest.TestApi;
import se.inera.acceptancetest.World;
import se.inera.acceptancetest.model.Entry;
import se.inera.acceptancetest.model.Property;
import se.inera.acceptancetest.model.User;
import se.inera.ldapclient.LDAPTestUser;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toMap;
import static org.junit.Assert.*;
import static se.inera.acceptancetest.TestHelpers.*;

public class Common {
    private final TestApi api;
    private World world;
    private Map<String, Consumer<SearchResultEntry>> objectAssertions = new HashMap<>();

    public Common(TestApi api, World world) {
        this.api = api;
        this.world = world;
        loadTestData();
        objectAssertions.put("alla Nordic MedTest attribute", actual -> {
            assertEquals(world.getTestEntry().getAttributes().get("o"), actual.getAttributeValue("o"));
            assertEquals(world.getTestEntry().getAttributes().get("hsaIdentity"), actual.getAttributeValue("hsaIdentity"));
            assertEquals(world.getTestEntry().getAttributes().get("countyCode"), actual.getAttributeValue("countyCode"));
            assertEquals(world.getTestEntry().getAttributes().get("hsaHpt"), actual.getAttributeValue("hsaHpt"));
            assertEquals(world.getTestEntry().getAttributes().get("mail"), actual.getAttributeValue("mail"));
            assertEquals(world.getTestEntry().getAttributes().get("municipalityCode"), actual.getAttributeValue("municipalityCode"));
            assertEquals(world.getTestEntry().getAttributes().get("orgNo"), actual.getAttributeValue("orgNo"));
            assertEquals(world.getTestEntry().getAttributes().get("postalAddress"), actual.getAttributeValue("postalAddress"));
            assertEquals(world.getTestEntry().getAttributes().get("telephoneNumber"), actual.getAttributeValue("telephoneNumber"));
        });

        objectAssertions.put("enhetens alla obligatoriska attribut och objektklasser", actual -> {
            //assertEquals("organizationalUnit", actual.getAttributeValue("objectClass"));
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalUnit"));
            assertEquals(world.getTestEntry().getAttributes().get("ou"), actual.getAttributeValue("ou"));
            assertTrue(actual.hasAttribute("hsaIdentity"));
        });
        objectAssertions.put("funktionens alla obligatoriska attribut och objektklasser", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalRole"));
            assertEquals(world.getTestEntry().getAttributes().get("cn"), actual.getAttributeValue("cn"));
            assertTrue(actual.hasAttribute("hsaIdentity"));
        });
        objectAssertions.put("personens alla obligatoriska attribut och objektklasser", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertEquals(world.getTestEntry().getAttributes().get("cn"), actual.getAttributeValue("cn"));
            assertTrue(actual.hasAttribute("hsaIdentity"));
            assertTrue(actual.hasAttribute("sn"));
            assertTrue(actual.hasAttribute("fullName"));
        });
        objectAssertions.put("personnummer", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("personalIdentityNumber"));

        });
        objectAssertions.put("tilltalsnamn", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("givenName"));

        });
        objectAssertions.put("mellannamn", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("middleName"));

        });
        objectAssertions.put("efternamn", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("sn"));

        });
        objectAssertions.put("CN", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("cn"));

        });
        objectAssertions.put("förskrivningsrätt för barnmorskor", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("hsaSosNursePrescriptionRight"));

        });
        objectAssertions.put("skyddad person", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("hsaConfidentialPerson"));
            assertTrue(actual.hasAttribute("hsaProtectedPerson"));
        });
        objectAssertions.put("styrkt samordningsnummer", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("hsaVerifiedIdentityNumber"));
        });
        objectAssertions.put("förskrivarkod", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("personalPrescriptionCode"));

        });
        objectAssertions.put("saknar svensk folkbokföringsadress", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("hsaEmigratedPerson"));
        });
        objectAssertions.put("hsaSosTitleCodeSpeciality", actual -> {
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("organizationalPerson"));
            assertTrue(actual.hasAttribute("hsaSosTitleCodeSpeciality"));

        });

        objectAssertions.put("genererade attribut", actual -> assertHasAttributes(actual, "createTimestamp", "creatorsName", "numSubordinates", "numAllSubordinates"));
        objectAssertions.put("allt som skickades in", actual -> assertEqualAttributes(actual, world.getTestEntry()));
        objectAssertions.put("ett genererat unikt ID", actual -> {
            assertTrue("Has the UUID attribute", actual.hasAttribute("dirxEntryUUID"));
            assertTrue("Value is a correct UUID", isUUID(actual.getAttribute("dirxEntryUUID").getValue()));
        });
        objectAssertions.put("markerad som vårdgivare", actual ->
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("hsaHealthCareProvider")));
        objectAssertions.put("inte markerad som vårdgivare", actual ->
                assertFalse(Arrays.asList(actual.getObjectClassValues()).contains("hsaHealthCareProvider")));
        objectAssertions.put("markerad som vårdenhet", actual ->
            assertTrue(Arrays.asList(actual.getObjectClassValues()).contains("hsaHealthCareUnit")));
        objectAssertions.put("mail", actual -> {
            assertTrue("Has the mail attribute",actual.hasAttribute("mail"));
            assertTrue("with correct syntax", isMail(actual.getAttribute("mail").getValue()));
        });
        objectAssertions.put("mobilnummer", actual ->
                assertEquals(actual.getAttributeValue("mobile"),api.getPropertyData("mobilnummer").getAttributes().get("mobile")));
        objectAssertions.put("orgNo", actual ->
            assertEquals(actual.getAttributeValue("orgNo"),api.getPropertyData("orgNo").getAttributes().get("orgNo")));
        objectAssertions.put("inte mail", actual ->
                assertFalse("Has no mail attribute",(actual.hasAttribute("mail"))));
        objectAssertions.put("inte länskod", actual ->
                assertFalse("Has no countyCode attribute",(actual.hasAttribute("countyCode"))));
        objectAssertions.put("inte kommuntillhörighet", actual ->
                assertFalse("Has no municipalityCode attribute",(actual.hasAttribute("municipalityCode"))));
        objectAssertions.put("ny mail", actual -> {
            assertTrue("Has the mail attribute",actual.hasAttribute("mail"));
            assertTrue("with correct syntax", isMail(actual.getAttribute("mail").getValue()));
            assertEquals(actual.getAttributeValue("mail"),api.getPropertyData("ny mail").getAttributes().get("mail"));
        });
        objectAssertions.put("inte ny mail", actual -> {
            if (world.getTestEntry().getAttributes().get("mail") == null) {
                assertNotSame(actual.getAttributeValue("mail"),api.getPropertyData("ny mail").getAttributes().get("mail"));
            }
            else {
                assertEquals(world.getTestEntry().getAttributes().get("mail"), actual.getAttributeValue("mail"));
            }
        });
        objectAssertions.put("samma HSAID som tidigare", actual ->
                assertEquals(world.getTestEntry().getAttributes().get("hsaIdentity"), actual.getAttributeValue("hsaIdentity")));
        objectAssertions.put("extra telefonnummer", actual ->
                assertTrue(Arrays.toString(actual.getAttributeValues("telephoneNumber")).contains(api.getPropertyData("extra telefonnummer").getAttributes().get("telephoneNumber"))));
        objectAssertions.put("samma orgNo som tidigare", actual ->
                assertEquals(world.getTestEntry().getAttributes().get("orgNo"), actual.getAttributeValue("orgNo")));
    }

    private void loadTestData() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            File usersFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/users.yml");
            Map<String, User> users = Arrays.stream(mapper.readValue(usersFile, User[].class))
                .collect(toMap(User::getName, u -> u));
            File entriesFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/entries.yml");
            Map<String, Entry> entries = Arrays.stream(mapper.readValue(entriesFile, Entry[].class))
                .collect(toMap(Entry::getName, u -> u));
            File propertiesFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/properties.yml");
            Map<String, Property> properties = Arrays.stream(mapper.readValue(propertiesFile, Property[].class))
                .collect(toMap(Property::getName, u -> u));
            api.loadTestData(users, entries, properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Givet("^att jag är en \"([^\"]*)\" att \"([^\"]*)\" under (?:en specifik|ett specifikt) \"([^\"]*)\"$")
    public void attJagArBehorighet(String userName, String actionType, String entryType) {
        User user = api.getUserData(userName);
        Entry entry = api.getEntryData(entryType);

        world.setUser(new LDAPTestUser(user.getUsername(), user.getPassword()));
        world.setTestEntry(entry);
        SearchResultEntry result = api.searchByDN(entry.getDn());
        assertNotNull("Entry: '" + entryType + "' did not exist in the LDAP proxy.", result);
        api.assertNoExceptionCaught();
    }

    @Och("^det finns \"([^\"]*)\"(?: under (?:en|ett) \"?(?:[^\"]*)\"?)?$")
    public void detfinnsen(String entryName) {

        world.setTestEntry(api.getEntryData(entryName));
        if (world.getTestEntry() == null) {
            throw new IllegalArgumentException("The Entry '" + entryName + "' does not exist in the test data.");
        }
        SearchResultEntry result = api.searchByDN(world.getTestEntry().getDn());
        if (result != null) {
            if (!(world.getTestEntry().getDn().equals(result.getDN()))) {
                if (ResultCode.SUCCESS.equals(api.add(world.getTestEntry()))) {
                   world.setEntryPreparedBefore(true);
                } else {
                    fail("Not able to add required preloaded entry: " + entryName);
                }
            }
        }
    }

    @Givet("^att jag är en \"([^\"]*)\"$")
    public void attJagAr(String userName) {
        User user = api.getUserData(userName);
        world.setUser(new LDAPTestUser(user.getUsername(), user.getPassword()));
    }

    @Så("^ska systemet svara att operationen gått bra$")
    public void skaSystemetSvaraAttOperationenGattBra() {
        api.assertNoExceptionCaught();
    }
    @Så("^ska jag \"(hitta|inte hitta)\" (?:enheten|funktionen|personen|organisationen) (?:någonstans|på rätt plats)$")
    public void jagKanHittaObjektet(String exists) {
        Entry entry = world.getTestEntry();
        SearchResult result = api.searchFromRoot(entry.createFilter("hsaIdentity"));

        if (exists.equals("hitta")) {
            assertNotNull(result);
            assertEquals(1, result.getEntryCount());
            if (world.isEntryMoved()) {
                assertEquals(world.getEntryParentDNafterMove(), "," + result.getSearchEntries().get(0).getDN());
            } else if (world.isEntryRenamed()) {
                assertTrue(result.getSearchEntries().get(0).getDN().contains(world.getEntryRDNafterRename()));
            } else {
                assertEquals(world.getTestEntry().getDn(), result.getSearchEntries().get(0).getDN());
            }
        } else if (exists.equals("inte hitta")) {
            assertEquals(0, result.getEntryCount());
        }
    }

    @Så("^ska jag \"(hitta|inte hitta)\" personen under \"([^\"]+)\"$")
    public void jagKanHittaObjektet(String exists,String destName) throws LDAPException {
        Entry entry = world.getTestEntry();
        Entry destEntry = api.getEntryData(destName);
        SearchResult result = api.searchFromRoot(entry.createFilter("hsaIdentity"));
        if (exists.equals("hitta")) {
            assertNotNull(result);
            assertEquals(1, result.getEntryCount());

                assertEquals(destEntry.getDn(),  result.getSearchEntries().get(0).getParentDNString());

        } else if (exists.equals("inte hitta")) {
            assertEquals(0, result.getEntryCount());
        }
    }
    @Så("^ska systemet svara med felet \"([^\"]+)\"$")
    public void systemetSvararMedFel(String error) {
        LDAPException exception = api.getAndClearException();
        assertNotNull("The operation should result in an exception.", exception);
        assertEquals(error, exception.getResultCode().getName());
    }

    @Och("^(?:enheten|funktionen|organisationen|personen) ska (?:vara|innehålla) \"([^\"]*)\"$")
    public void enhetenSkaVaraInnehalla(String key) {
        Consumer<SearchResultEntry> assertion = objectAssertions.get(key);
        assertNotNull("Assertion '" + key + "' not found.", assertion);
        assertion.accept(api.getLastSearchEntry());
    }

    @Och("^(?:enheten|funktionen|organisationen|personen) ska inte (?:vara|innehålla) \"([^\"]*)\"$")
    public void enhetenSkaInteVaraInnehalla(String key) {
        Consumer<SearchResultEntry> assertion = objectAssertions.get("inte " + key);
        assertNotNull("Assertion inte '" + key + "' not found.", assertion);
        assertion.accept(api.getLastSearchEntry());
    }

    @När("^scenariot är klart behöver \"([^\"]*)\" manuellt avmarkeras för att återställa$")
    public void stadareftertest(String property) {
        assertTrue(ResultCode.SUCCESS.equals(api.modifyDelete(world.getTestEntry(),api.getPropertyData(property))));
    }

    @After
    public void cleanUp() {
        /*
          Object Moved or renamed must be reverted first
         */
        if (world.isEntryMoved()) {
            String RDN = world.getEntryFromBeforeTest().getDn().replace(world.getEntryFromBeforeTest().getParentDn(),"");
            String parentDN = world.getEntryParentDNafterMove();
            String newRDN = world.getEntryFromBeforeTest().getDn().replace(world.getEntryFromBeforeTest().getParentDn(),"");
            //Remove comma to get correct parentDN
            String newParentDN = world.getEntryFromBeforeTest().getParentDn().substring(1);

            api.modifyDN(RDN, parentDN, newRDN, newParentDN);
            api.assertNoExceptionCaught();

        }
        else if (world.isEntryRenamed()) {
            String RDN = world.getEntryRDNafterRename();
            String parentDN = world.getEntryFromBeforeTest().getParentDn();
            String newRDN = world.getEntryFromBeforeTest().getDn().replace(world.getEntryFromBeforeTest().getParentDn(),"");
            //Remove comma to get correct parentDN
            String newParentDN = world.getEntryFromBeforeTest().getParentDn().substring(1);
            api.modifyDN(RDN, parentDN, newRDN, newParentDN);
            api.assertNoExceptionCaught();
        }
        /*
          Property add, delete, replace -cleanup
         */
        while(world.getNumRevertMods() > 0) {
            api.modifyRevert(world.getTestEntry(), world.popRevertMods());
        }
        /*
          Entry cleanup
         */
        if (world.isEntryPreparedBefore() && world.getEntryUnModified()) {
            api.remove(world.getEntryFromBeforeTest());
            api.assertNoExceptionCaught();
        }
        else if (world.isEntryRemoved() && !world.isEntryPreparedBefore()) {
            api.add(world.getTestEntry());
            api.assertNoExceptionCaught();

        }
        else if (world.isEntryPreparedBefore() && !world.isEntryRemoved()) {

            api.remove(world.getTestEntry());
            api.assertNoExceptionCaught();
        }
        else if (world.isEntryAdded()) {
            api.remove(world.getTestEntry());
            api.assertNoExceptionCaught();
        }


    }
}
