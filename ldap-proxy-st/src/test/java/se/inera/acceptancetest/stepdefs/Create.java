package se.inera.acceptancetest.stepdefs;

import com.unboundid.ldap.sdk.ResultCode;
import cucumber.api.java.sv.När;
import se.inera.acceptancetest.TestApi;
import se.inera.acceptancetest.World;

public class Create {
    private TestApi api;
    private World world;

    public Create(TestApi api, World world) {
        this.api = api;
        this.world = world;
    }

    @När("^jag skapar \"([^\"]*)\"(?: under \"?(?:[^\"]*)\"?)?$")
    public void jagSkapar(String entryName) {
        world.setTestEntry(api.getEntryData(entryName));
        if (world.getTestEntry() == null) {
            throw new IllegalArgumentException("The Entry '" + entryName + "' does not exist in the test data.");
        }
        if (ResultCode.SUCCESS.equals(api.add(world.getTestEntry()))) {
            world.setEntryAdded(true);
        }
    }
}
