package se.inera.acceptancetest.stepdefs.function;

import com.unboundid.ldap.sdk.SearchResultEntry;
import cucumber.api.java.sv.Och;

import se.inera.acceptancetest.TestApi;
import se.inera.acceptancetest.World;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static se.inera.acceptancetest.TestHelpers.assertEqualAttributes;
import static se.inera.acceptancetest.TestHelpers.assertHasAttributes;
import static se.inera.acceptancetest.TestHelpers.isUUID;

public class CreateFunction {

    private TestApi api;
    private String[] theClasses;
    private Map<String, Consumer<SearchResultEntry>> objectAssertions = new HashMap<>();


    public CreateFunction(TestApi api, World world) {
        this.api = api;
        theClasses =  new String[3];
        String[] theExpected = {"organizationalRole","top","HSARoleExtension"};

        // objectAssertions contains tests that should be done on a
        // Search Result given a property name from the feature file.
        objectAssertions.put("alla obligatoriska attribut och objektklasser", actual -> {
            theClasses = actual.getAttributeValues("objectClass");
            assertArrayEquals(theExpected,theClasses);

            assertEquals(world.getTestEntry().getAttributes().get("cn"), actual.getAttributeValue("cn"));
            assertTrue(actual.hasAttribute("hsaIdentity"));
        });
        objectAssertions.put("genererade attribut", actual -> assertHasAttributes(actual, "createTimestamp", "creatorsName", "numSubordinates", "numAllSubordinates"));
        objectAssertions.put("allt som skickades in", actual -> assertEqualAttributes(actual, world.getTestEntry()));
        objectAssertions.put("ett genererat unikt ID", actual -> {
            assertTrue("Has the UUID attribute", actual.hasAttribute("dirxEntryUUID"));
            assertTrue("Value is a correct UUID", isUUID(actual.getAttribute("dirxEntryUUID").getValue()));
        });
    }
}
