package se.inera.acceptancetest.stepdefs;

import com.unboundid.ldap.sdk.ResultCode;
import cucumber.api.java.sv.När;
import se.inera.acceptancetest.TestApi;
import se.inera.acceptancetest.World;

public class Delete {
    private final World world;
    private TestApi api;

    public Delete(TestApi api, World world) {
        this.api = api;
        this.world = world;
    }

    @När("^jag tar bort \"([^\"]*)\"(?: under \"?(?:[^\"]*)\"?)?$")
    public void jagTarBort(String entryName) {
        if (api.getEntryData(entryName) == null) {
            throw new IllegalArgumentException("The Entry '" + entryName + "' does not exist in the test data.");
        }
        if (ResultCode.SUCCESS.equals(api.remove(api.getEntryData(entryName)))) {
            world.setEntryRemoved(true);
            world.setTestEntry(api.getEntryData(entryName));
        }
    }
}
