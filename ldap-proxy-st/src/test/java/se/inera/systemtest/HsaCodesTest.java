package se.inera.systemtest;

import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchScope;
import junit.framework.TestCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import se.inera.ldapclient.SimpleLdapClient;

class HsaCodesTest extends BaseTest {

    @Test
    void getCodeSystemCodes() throws Exception {
        SearchRequest request = new SearchRequest("dc=koder,dc=Services,c=SE", SearchScope.SUB, "(objectClass=*)");
        SimpleLdapClient simpleLdapClient = getLdapClients().get("ldap-proxy-client");
        SearchResult result = simpleLdapClient.search(request);
        TestCase.assertTrue(result.getEntryCount() > 0);
        result.getSearchEntries().forEach(searchResultEntry ->
                TestCase.assertNotNull("attribute should not be null: " + searchResultEntry.toLDIFString(),searchResultEntry.getAttribute("hsaCodeEntry")));
    }
}
