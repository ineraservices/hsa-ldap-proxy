package se.inera.systemtest;

import com.unboundid.ldap.sdk.*;
import junit.framework.TestCase;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import se.inera.ldapclient.SimpleLdapClient;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

class SearchObjectTest {
    /**
     * a realistic test would be to test searches pending role and assert a certain
     * role not able to read data not associated with role
     */
    @DisplayName("BASE search Country")
    @Test
    @Tag("SEARCH")
    void searchCountry() throws Exception {
        SearchRequest request = new SearchRequest("c=SE", SearchScope.BASE, "(objectClass=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        System.out.println(result.getSearchEntries());
        assertEquals(1, result.getEntryCount());
    }

    @DisplayName("BASE search function")
    @Test
    @Tag("SEARCH")
    void searchFunction() throws Exception {
        SearchRequest request = new SearchRequest("cn=Nordic Unit Function,ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.BASE, "(objectClass=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        System.out.println(result.getSearchEntries());
        assertEquals(1, result.getEntryCount());
    }
    
    @DisplayName("BASE search employee")
    @Test
    @Tag("SEARCH")
    void searchEmployee() throws Exception {
        SearchRequest request = new SearchRequest("cn=Anna Fia Inera,o=Nordic Organization Maximum,l=Värmlands län,c=SE", SearchScope.BASE, "(objectClass=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        System.out.println(result.getSearchEntries());
        assertEquals(1, result.getEntryCount());
        assertTrue(result.getSearchEntries().get(0).hasObjectClass("HSAPersonExtension"));
    }
    
    @DisplayName("SUB search filter")
    @Test
    @Tag("SEARCH")
    void searchSUBTreeWithFilter() throws Exception {
        SearchRequest request = new SearchRequest("c=SE", SearchScope.SUB, "(o=Nordic MedTest)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        System.out.println(result.getSearchEntries());
        assertAll("SUB search filter", () -> assertEquals(1, result.getEntryCount()),
                      () -> assertEquals("l=Värmlands län,c=SE",result.getSearchEntries().get(0).getParentDN().toString() ));

    }
    @DisplayName("SUB search filter consists of hsaIdentity")
    @Test
    @Tag("SEARCH")
    void searchSUBTreeFilterByHSAIdentity() throws Exception {
        SearchRequest request = new SearchRequest("l=Värmlands län,c=SE", SearchScope.SUB, "(hsaIdentity=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        System.out.println(result.getSearchEntries());


        boolean firstObjectFound= false;
        boolean secondObjectFound = false;
        if(result.getEntryCount()!= 0){
            for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
                if(searchResultEntry.toLDIFString().contains("TSTNMT2321003456-1002"))
                {
                    System.out.println(searchResultEntry.toLDIFString());
                    firstObjectFound = true;
                    assertEquals("TSTNMT2321003456-1002", searchResultEntry.getAttributeValue("hsaIdentity"));

                }
                if(searchResultEntry.toLDIFString().contains("TSTNMT2321003456-1003"))
                {
                    System.out.println(searchResultEntry.toLDIFString());
                    secondObjectFound = true;
                    assertEquals("TSTNMT2321003456-1003", searchResultEntry.getAttributeValue("hsaIdentity"));

                }
            }
        }
        assertTrue("Object not found in subtree search", firstObjectFound);
        assertTrue("Object not found in subtree search", secondObjectFound);

    }

    @DisplayName("Sub-tree search and filter by name")
    @Test
    @Tag("SEARCH")
    void searchOrganizationByName() throws Exception {
        SearchRequest request = new SearchRequest("l=Värmlands län,c=SE", SearchScope.SUB, "(o=Nordic MedTest)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        assertEquals(1, result.getEntryCount());
        assertEquals("Nordic MedTest",result.getSearchEntries().get(0).getAttributeValue("o") );
    }

    @DisplayName("Base search for specific organization*")
    @Test
    @Tag("SEARCH")
    void searchOrganizationByHSAIDandStar() throws Exception {
        SearchRequest request = new SearchRequest("o=Svensk Organisation, c=SE", SearchScope.BASE, "(hsaIdentity=TSTNMT2321003456-000*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);

        //Only one hit expected
        assertEquals(1, result.getEntryCount());
        assertEquals("Svensk Organisation",result.getSearchEntries().get(0).getAttributeValue("o"));
    }

    @DisplayName("Sub-tree search containing all")
    @Test
    @Tag("SEARCH")
    void searchSubSverige() throws Exception {
        SearchRequest oneRequest = new SearchRequest("c=SE", SearchScope.SUB, "(objectClass=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(oneRequest);
        boolean objectFound = false;
        if(result.getEntryCount()!= 0){
            for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
                if(searchResultEntry.toLDIFString().contains("TEST999999994-0011"))
                {
                    System.out.println(searchResultEntry.toLDIFString());
                    objectFound = true;
                    assertEquals("TEST999999994-0011", searchResultEntry.getAttributeValue("hsaIdentity"));
                    break;
                }
            }
        }
        assertTrue("Object not found in subtree search", objectFound);
        assertTrue( result.getEntryCount()> 30);//would be better with another solution
    }

    @DisplayName("Sub-tree search for all OrganizationUnit")
    @Test
    @Tag("SEARCH")
    void searchSubSverigeUnits() throws Exception {
        SearchRequest oneRequest = new SearchRequest("c=SE", SearchScope.SUB, "(objectClass=organizationalUnit)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(oneRequest);

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());

        List<String> expectedDN = Stream.of("ou=Svensk enhet,o=Svensk Organisation,c=SE",
                "ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=Nordic Unit4,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=underenhet,ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                .collect(Collectors.toList());
        assertEquals(7, result.getEntryCount());
        assertTrue(collectedDN.containsAll(expectedDN));

        List<String> expectedObjectClasses = Stream.of("organizationalUnit",
                "top",
                "HSAOrganizationExtension")
                .collect(Collectors.toList());


        for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
            assertTrue(Arrays.stream(searchResultEntry.getAttributeValues("objectClass"))
                                      .collect(Collectors.toList())
                                      .containsAll(expectedObjectClasses));
        }
    }
    @DisplayName("Sub-tree search , sizeLimit")
    @Test
    @Tag("SEARCH")
    void searchSubSverigeUnitsSizeLimit() throws Exception {
        SearchRequest oneRequest = new SearchRequest("c=SE", SearchScope.SUB, "(objectClass=organizationalUnit)");
        int sizeLimit = 3;
        oneRequest.setSizeLimit(sizeLimit );
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        Throwable exception = assertThrows(LDAPSearchException.class, () -> ldapProxyClient.search(oneRequest));
        LDAPSearchException ex = (LDAPSearchException) exception;
        
        assertEquals(ex.getResultCode(), ResultCode.SIZE_LIMIT_EXCEEDED);
        List<SearchResultEntry> searchEntries = ex.getSearchEntries();
        assertEquals(sizeLimit, searchEntries.size());

        List<String> expectedObjectClasses = Stream.of("organizationalUnit",
                                                        "top",
                                                        "HSAOrganizationExtension")
                                                   .collect(Collectors.toList());

        for (SearchResultEntry searchResultEntry : searchEntries) {
            assertTrue(Arrays.stream(searchResultEntry.getAttributeValues("objectClass"))
                    .collect(Collectors.toList())
                    .containsAll(expectedObjectClasses));
        }
    }

    @DisplayName("One-level search for all kind of objects")
    @Test
    @Tag("SEARCH")
    void searchOneLevelNordicMedTestAll() throws Exception {
        SearchRequest request = new SearchRequest("o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.ONE, "(objectClass=*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        assertNotSame(0, result.getEntryCount());

        List<String> expectedDN = Stream.of("cn=Nordic Function,o=Nordic MedTest,l=Värmlands län,c=SE",
                                            "ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE",
                                            "ou=Nordic Unit4,o=Nordic MedTest,l=Värmlands län,c=SE",
                                            "ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                                            "ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                                  .collect(Collectors.toList());

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());
        
        List<String> notExpectedDN = Stream.of("ou=underenhet,ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                                    .collect(Collectors.toList());

        assertAll("One-level search for all kind of objects", () -> assertEquals(5, result.getEntryCount()),
                            () -> assertTrue("Should contain all expected DNs", collectedDN.containsAll(expectedDN)),
                            () -> assertFalse("Should not contain this unexpected DN", collectedDN.containsAll(notExpectedDN)));
    }

    @DisplayName("One-level search for function object")
    @Test
    @Tag("SEARCH")
    void searchOneLevelNordicMedTestFunction() throws Exception {
        SearchRequest request = new SearchRequest("o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.ONE, "(objectClass=organizationalRole)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);

        //Only one function expected
        assertAll("One-level search for function object",
                () -> assertEquals(1, result.getEntryCount()),
                () -> assertEquals(result.getSearchEntries().get(0).getAttributeValue("hsaIdentity"),"TSTNMT2321003456-1003"),
                () -> assertTrue(result.getSearchEntries().get(0).hasObjectClass("HSARoleExtension")),
                () -> assertTrue(result.getSearchEntries().get(0).hasObjectClass("organizationalRole")),
                () -> assertEquals(result.getSearchEntries().get(0).getDN(),"cn=Nordic Function,o=Nordic MedTest,l=Värmlands län,c=SE"));
    }

    @DisplayName("Sub-level search testing filter OR AND Wildcard")
    @Test
    @Tag("SEARCH")
    void searchSubLevelORNordicMedTestAll() throws Exception {
        SearchRequest request = new SearchRequest("o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.SUB, "(&(hsaIdentity=T*)(|(objectClass=organizationalUnit)(objectClass=organization)))");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        assertNotSame(0, result.getEntryCount());

        List<String> expectedDN = Stream.of("o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=underenhet,ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                .collect(Collectors.toList());

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());
        //collectedDN.forEach(System.out::println);

        assertTrue(collectedDN.containsAll(expectedDN));

    }

//THIS TEST WILL FAIL because locality is county in orgAPI
    @DisplayName("Sub-level filter out country or locality using OR")
    @Test
    @Tag("SEARCH")
    void searchCountryOrCountyDN() throws Exception {
        SearchRequest request = new SearchRequest("c=SE", SearchScope.SUB, "(|(objectClass=locality)(objectClass=country))");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        assertNotSame(0, result.getEntryCount());
//                System.out.println("This TEST will FAIL until locality is implemented for LDAP-proxy");
        List<String> expectedDN = Stream.of("l=Värmlands län,c=SE",
                "l=Stockholms län,c=SE",
                "l=Uppsala län,c=SE",
                "l=Sörmlands län,c=SE",
                "l=Östergötlands län,c=SE",
                "l=Jönköpings län,c=SE",
                "l=Kronobergs län,c=SE",
                "l=Kalmar län,c=SE",
                "l=Gotlands län,c=SE",
                "l=Blekinge län,c=SE",
                "l=Skåne län,c=SE",
                "l=Hallands län,c=SE",
                "l=Västra Götalands län,c=SE",
                "l=Värmlands län,c=SE",
                "l=Örebro län,c=SE",
                "l=Västmanlands län,c=SE",
                "l=Dalarnas län,c=SE",
                "l=Gävleborgs län,c=SE",
                "l=Västernorrlands län,c=SE",
                "l=Jämtlands län,c=SE",
                "l=Västerbottens län,c=SE",
                "l=Norrbottens län,c=SE",
                "c=SE")
               .collect(Collectors.toList());

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());
       assertTrue(collectedDN.containsAll(expectedDN));


    }
    @DisplayName("Sub-tree search for all OrganizationUnit, only under NordicMedtest")
    @Test
    @Tag("SEARCH")
    void searchSubNordicMedtestUnits() throws Exception {
        SearchRequest oneRequest = new SearchRequest("o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.SUB, "(objectClass=organizationalUnit)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(oneRequest);

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());

        List<String> expectedDN = Stream.of("ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=underenhet,ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                .collect(Collectors.toList());

        assertTrue(collectedDN.containsAll(expectedDN));

        List<String> expectedObjectClasses = Stream.of("organizationalUnit",
                "top",
                "HSAOrganizationExtension")
                .collect(Collectors.toList());

        List<String> notExpectedDN = Stream.of("ou=Svensk enhet,o=Svensk Organisation,c=SE")
               .collect(Collectors.toList());
        assertFalse(collectedDN.containsAll(notExpectedDN));

        for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
            assertTrue(Arrays.stream(searchResultEntry.getAttributeValues("objectClass"))
                    .collect(Collectors.toList())
                    .containsAll(expectedObjectClasses));
        }
    }
    @DisplayName("Sub-tree search for all OrganizationUnit, only under NordicMedtest")
    @Test
    @Tag("SEARCH")
     void searchSubNordicMedtestUnitsWithoutNordicMedtest() throws Exception {
        SearchRequest oneRequest = new SearchRequest("o=Nordic MedTest,l=Värmlands län,c=SE", SearchScope.SUB, "(objectClass=organizationalUnit)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(oneRequest);

        List<String> collectedDN = result.getSearchEntries().stream()
                .map(Entry::getDN)
                .collect(Collectors.toList());

        List<String> expectedDN = Stream.of("ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=underenhet,ou=enhet med underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE",
                "ou=enhet utan underordnade objekt,o=Nordic MedTest,l=Värmlands län,c=SE")
                .collect(Collectors.toList());

        assertTrue(collectedDN.containsAll(expectedDN));

        List<String> expectedObjectClasses = Stream.of("organizationalUnit",
                "top",
                "HSAOrganizationExtension")
                .collect(Collectors.toList());

        List<String> notExpectedDN = Stream.of("ou=Svensk enhet,o=Svensk Organisation,c=SE",
                "o=Nordic MedTest,l=Värmlands län,c=SE")
                .collect(Collectors.toList());
        collectedDN.stream()
                .filter(notExpectedDN::contains)
                .forEach(dn -> fail(dn + " should not be included in the results."));


        for (SearchResultEntry searchResultEntry : result.getSearchEntries()) {
            assertTrue(Arrays.stream(searchResultEntry.getAttributeValues("objectClass"))
                    .collect(Collectors.toList())
                    .containsAll(expectedObjectClasses));
        }
    }
    @DisplayName("Sub-level search testing messeges no such object")
    @Test
    @Tag("SEARCH")
    void searchSubLevelNoSuchObject() throws Exception {
        SearchRequest oneRequest = new SearchRequest("o=Nordic MedTes,l=Värmlands län,c=SE", SearchScope.SUB, "(&(hsaIdentity=S*)(|(objectClass=organizationalUnit)(objectClass=organization)))");
        String exceptionString = "";
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult searchR = null;

        try {
            searchR = ldapProxyClient.search(oneRequest);
             } catch (LDAPException e) {
                exceptionString   = e.toString();
            }
        System.out.println(exceptionString);
        assertTrue(exceptionString.contains("no such object"));
        assertNull(searchR);

    }

    @DisplayName("Sub-level search testing messeges invalid DN syntax")
    @Test
    @Tag("SEARCH")
    void searchSubLevelInvalidDNSyntax() throws Exception {
        SearchRequest oneRequest = new SearchRequest("Nordic MedTes,Värmlands län,SE", SearchScope.SUB, "(&(hsaIdentity=S*)(|(objectClass=organizationalUnit)(objectClass=organization)))");
        String theExceptionString = "";
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult searchR = null;
        try {
            searchR = ldapProxyClient.search(oneRequest);

            } catch (LDAPException e) {
            theExceptionString = e.toString();
        }
        System.out.println(theExceptionString);
        assertTrue(theExceptionString.contains("invalid DN syntax"));
        assertNull(searchR);
    }
}
