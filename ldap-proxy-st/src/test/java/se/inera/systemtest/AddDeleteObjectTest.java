package se.inera.systemtest;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.ResultCode;

import se.inera.ldapclient.SimpleLdapClient;

public class AddDeleteObjectTest {
    
    static int counter = 1;
    @DisplayName("Add Employee")
    @Tag("SEARCH")
    @ParameterizedTest
    @ValueSource(strings = {"o=Svensk Organisation,c=SE",
                            "ou=Svensk enhet,o=Svensk Organisation,c=SE",
                            "o=Nordic MedTest,l=Värmlands län,c=SE",
                            "ou=Nordic Unit,o=Nordic MedTest,l=Värmlands län,c=SE"})
    void addDeleteEmployee(String dn) throws Exception {
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        // Create employee
        Entry testEmployee = getTestEmployee(dn);
        LDAPResult result = ldapProxyClient.add(testEmployee.getDN(), testEmployee.getAttributes());
        System.out.println(result.getResultCode());
        assertEquals(ResultCode.SUCCESS, result.getResultCode());
        
        // Verify employee created
        Entry entry = ldapProxyClient.getEntry(testEmployee.getDN());
        assertAll("Create Person", 
                            () -> assertNotNull(entry),
                            () -> assertEquals(testEmployee.getAttributeValue("hsaIdentity"), entry.getAttributeValue("hsaIdentity")),
                            () -> assertEquals(testEmployee.getAttributeValue("fullName"), entry.getAttributeValue("fullName")),
                            () -> assertEquals(testEmployee.getAttributeValue("middleName"), entry.getAttributeValue("middleName")),
                            () -> assertEquals(testEmployee.getAttributeValue("sn"), entry.getAttributeValue("sn")),
                            () -> assertEquals(testEmployee.getAttributeValue("givenName"), entry.getAttributeValue("givenName"))
                );
        
        
        // Delete employee
        ldapProxyClient.deleteRecursive(testEmployee.getDN());
        Entry deletedEntry = ldapProxyClient.getEntry(testEmployee.getDN());
        assertNull(deletedEntry);
    }

    
    Entry getTestEmployee(String parentDn) {
        Entry entry = new Entry("cn=Test Ldap Person,"+parentDn);
        entry.addAttribute("objectClass", "HSAPersonExtension","InetOrgPerson","organizationalPerson","person","pkiUser","top");
        entry.addAttribute("sn", "Person");
        entry.addAttribute("cn", "Test Ldap Person");
        entry.addAttribute("givenName", "Test");
        entry.addAttribute("middleName", "Ldap");
        entry.addAttribute("fullName", "Ldap Person, Test");
        entry.addAttribute("hsaIdentity", "TEST_LDAP_EMP_000"+counter++);
        return entry;
    }
}
