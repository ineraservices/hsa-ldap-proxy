package se.inera.systemtest;

import com.unboundid.ldap.sdk.LDAPException;
import se.inera.ldapclient.SimpleLdapClient;

import java.io.IOException;
import java.util.Properties;

class LdapClientFactory {
    static SimpleLdapClient create() throws LDAPException, IOException {
        Properties props = new Properties();
        props.load(BaseTest.class.getResourceAsStream("test.properties"));

        String baseUri = System.getProperty("baseUri", props.getProperty("ldap-proxy.host"));
        int port = Integer.parseInt(System.getProperty("port", props.getProperty("ldap-proxy.port")));
        System.out.println("Creating client to server: " + baseUri + ":" + port);
        return new SimpleLdapClient(
            "ldap-proxy-client",
            baseUri,
            port,
            props.getProperty("ldap-proxy.username"),
            props.getProperty("ldap-proxy.password"));

    }
}
