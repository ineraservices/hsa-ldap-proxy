package se.inera.systemtest;

import com.unboundid.ldap.sdk.*;
import junit.framework.TestCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import se.inera.ldapclient.SimpleLdapClient;

import static com.unboundid.ldap.sdk.ModificationType.ADD;
import static com.unboundid.ldap.sdk.ModificationType.DELETE;
import static com.unboundid.ldap.sdk.ModificationType.INCREMENT;
import static com.unboundid.ldap.sdk.ModificationType.REPLACE;
import static com.unboundid.ldap.sdk.ResultCode.NOT_SUPPORTED;
import static com.unboundid.ldap.sdk.ResultCode.SUCCESS;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

class ModifyObjectTest extends BaseTest {

    private static final String description = "description";

    @DisplayName("Modify Employee")
    @Test
    @Tag("MODIFY")
    void modifyEmployee() throws Exception {
        
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        String employeeDn = "cn=Anna Fia Inera,o=Nordic Organization Maximum,l=Värmlands län,c=SE";
        Entry employeeEntry = ldapProxyClient.getEntry(employeeDn);

        //Only one hit expected
        assertNotNull(employeeEntry);
        assertEquals("Anna Fia Inera",employeeEntry.getAttributeValue("fullName"));
        
        List<Modification> mods = new ArrayList<>();
        //Modify Description
        if ( employeeEntry.hasAttribute(description)) {
            mods.add( new Modification(DELETE,description, employeeEntry.getAttributeValue(description)));
        }
        mods.add(new Modification(ADD,description, "Test värde"));
        LDAPResult modResult = ldapProxyClient.modify(new ModifyRequest(employeeDn,mods));
        assertEquals(SUCCESS,modResult.getResultCode());
        
        // Verify desciption
        Entry employeeEntryAfterModification = ldapProxyClient.getEntry(employeeDn);
        assertEquals("Test värde" ,employeeEntryAfterModification.getAttributeValue("description"));
        
        // DELETE description
        
        ModifyRequest modifyRequest = new ModifyRequest(employeeDn,new Modification(DELETE,description));
        ldapProxyClient.modify(modifyRequest);
        
        //Verify delete description
        Entry employeeEntryAfterDeleteDescription = ldapProxyClient.getEntry(employeeDn);
        assertFalse(employeeEntryAfterDeleteDescription.hasAttribute("description"));
    }
        
    @DisplayName("Modify modtype INCREMENET")
    @Test
    @Tag("MODIFY")
    void modifyOrgAttributeWithModtypeIncrement() throws Exception {

        SearchRequest request = new SearchRequest("o=Svensk Organisation, c=SE", SearchScope.BASE, "(hsaIdentity=TSTNMT2321003456-000*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);

        //Only one hit expected
        TestCase.assertEquals(1, result.getEntryCount());
        SearchResultEntry searchResultEntry = result.getSearchEntries().get(0);
        TestCase.assertEquals("Svensk Organisation",searchResultEntry.getAttributeValue("o"));
        List<Modification> mods = new ArrayList<>();
        if ( searchResultEntry.hasAttribute(description)) {
            mods.add( new Modification(DELETE,description, searchResultEntry.getAttributeValue(description)));
        }
        mods.add(new Modification(ADD,description, "0"));
        ModifyRequest modifyRequest = new ModifyRequest("o=Svensk Organisation, c=SE",mods);
        LDAPResult modResult = ldapProxyClient.modify(modifyRequest);
        TestCase.assertEquals(SUCCESS,modResult.getResultCode());

        modifyRequest = new ModifyRequest("o=Svensk Organisation, c=SE",new Modification(INCREMENT,description,"1"));
        try {
            ldapProxyClient.modify(modifyRequest);
        } catch (LDAPException e) {
            TestCase.assertEquals(NOT_SUPPORTED,e.getResultCode());
        }
    }
    
    @DisplayName("Modify modtype REPLACE")
    @Test
    @Tag("MODIFY")
    void modifyOrgAttributeWithModtypeReplace() throws Exception {
        
        SearchRequest request = new SearchRequest("o=Svensk Organisation, c=SE", SearchScope.BASE, "(hsaIdentity=TSTNMT2321003456-000*)");
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        SearchResult result = ldapProxyClient.search(request);
        
        //Only one hit expected
        TestCase.assertEquals(1, result.getEntryCount());
        SearchResultEntry searchResultEntry = result.getSearchEntries().get(0);
        TestCase.assertEquals("Svensk Organisation",searchResultEntry.getAttributeValue("o"));
        ModifyRequest modifyRequest = new ModifyRequest("o=Svensk Organisation, c=SE",new Modification(REPLACE,description,"1"));
        try {
            ldapProxyClient.modify(modifyRequest);
        } catch (LDAPException e) {
            TestCase.assertEquals(NOT_SUPPORTED,e.getResultCode());
        }
    }


    @Disabled
    @Test
    /**
     * what should happen when trying to add/modify non defined attribute?
     */
    void modifyUnexistingAttributeShouldFail() throws Exception {
        ldapLoadLdifBaseData();
        assertThrows(LDAPException.class, () -> ldapLoadLdifFiles("testdata/failing_modify_org_structure.ldif"));
    }

    @Disabled
    @Test
    void modifyOrganization() throws Exception {
        ldapLoadLdifBaseData();

        SearchRequest request = new SearchRequest("c=SE", SearchScope.SUB, "(street=Hästgatan 12 b)");

        for (SimpleLdapClient simpleLdapClient : getLdapClients().values()) {
            SearchResult result = simpleLdapClient.search(request);
            TestCase.assertEquals(0, result.getEntryCount());
        }

        ldapLoadLdifFiles("testdata/modify_org_structure.ldif");

        request = new SearchRequest("c=SE", SearchScope.SUB, "(street=Hästgatan 12 b)");
        for (SimpleLdapClient simpleLdapClient : getLdapClients().values()) {
            SearchResult result = simpleLdapClient.search(request);
            TestCase.assertEquals(1, result.getEntryCount());
        }
    }
}
