package se.inera.systemtest;

import com.unboundid.ldap.sdk.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import se.inera.ldapclient.LdifFileReader;
import se.inera.ldapclient.SimpleLdapClient;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
class OrganisationUnitTest extends BaseTest {

    @Test
    @DisplayName("Adding a HealtCareProvider Unit with orgNo to an existing " +
            "Organization should result in response code 0 \"OK\"")
    void addHealthCareProviderUnitSuccess() throws Exception {
        SimpleLdapClient simpleLdapClient = getLdapClients().get("ldap-proxy-client");
        List<LDAPResult> results = simpleLdapClient
                .loadLdifFiles("ldif/newWorkingHealthCareProviderUnit.ldif");
        assertEquals(1, results.size());
        assertEquals(0, results.get(0).getResultCode().intValue());
        simpleLdapClient.modify(new ModifyRequest("ou=Mocked Up Unit,o=Svensk Organisation,c=SE", new Modification(ModificationType.DELETE, "ObjectClass", "hsaHealthCareProvider")));
        simpleLdapClient.deleteRecursive("ou=Mocked Up Unit,o=Svensk Organisation,c=SE");
    }

    @Test
    @DisplayName("Adding a HealtCareProvider Unit without orgNo to an existing " +
            "Organization should result in response code 65  \"object class violation\"")
    void addHealthCareProviderUnitFailure(){

        LDAPException exception = assertThrows(LDAPException.class, () -> {
            getLdapClients().get("ldap-proxy-client")
                    .loadLdifFiles("ldif/newFailingHealthCareProviderUnit.ldif");
        });
        assertEquals(65, exception.getResultCode().intValue());
    }


    @Disabled
    @Test
    /**
     * this test fails on images and other fields
     */
    void createOrganisationUnits() throws Exception {
        // add ldif file through proxy
        ldapLoadLdifBaseData();

        // assert using file
        List<Entry> entries = LdifFileReader.getLdifEntries(LDIF_BASE_DATA);

        for (Entry entry : entries) {
            SearchRequest request = new SearchRequest(entry.getDN(), SearchScope.BASE, "(objectClass=*)");
            assertCreateOrganizationUnit(entry, request);
        }

        for (Entry entry : entries) {
            SearchRequest request = new SearchRequest("c=SE", SearchScope.SUB, "(hsaIdentity=" + entry.getAttributeValue("hsaIdentity") + ")");
            assertCreateOrganizationUnit(entry, request);
        }
    }

    private void assertCreateOrganizationUnit(Entry entry, SearchRequest request) throws LDAPSearchException {
        for (SimpleLdapClient simpleLdapClient : getLdapClients().values()) {
            SearchResult result = simpleLdapClient.search(request);
            int expectedNumberOfSearchResults = 1;
            assertEquals(expectedNumberOfSearchResults, result.getEntryCount());
            for (Entry searchResultEntry : result.getSearchEntries()) {
                assertEntryEquals(entry, searchResultEntry);
            }
        }
    }

    private void assertEntryEquals(Entry entry, Entry searchResultEntry) {
        assertEquals(entry.getDN(), searchResultEntry.getDN());

        for (Attribute a : entry.getAttributes()) {
            // we do not expect change type
            if (a.getName().equals("changetype"))
                return;
            assertTrue(searchResultEntry.getAttributes().contains(a));
        }
    }
}
