package se.inera.systemtest;

import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.ModifyDNRequest;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchScope;
import junit.framework.TestCase;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import se.inera.ldapclient.SimpleLdapClient;

class MoveObjectTest extends BaseTest {
    
    static int counter = 1;
    @DisplayName("Move Employee")
    @Tag("SEARCH")
    @Test
    void addMoveRenameDeleteEmployee() throws Exception {
        String dn = "o=Svensk Organisation,c=SE";
        String newDn = "ou=Svensk enhet,o=Svensk Organisation,c=SE";
        SimpleLdapClient ldapProxyClient = LdapClientFactory.create();
        // Create employee
        Entry testEmployee = getTestEmployee(dn);
        LDAPResult result = ldapProxyClient.add(testEmployee.getDN(), testEmployee.getAttributes());
        System.out.println(result.getResultCode());
        assertEquals(ResultCode.SUCCESS, result.getResultCode());
        
        // Verify employee created
        Entry entry = ldapProxyClient.getEntry(testEmployee.getDN());
        assertNotNull(entry);
        assertEquals(testEmployee.getAttributeValue("hsaIdentity"), entry.getAttributeValue("hsaIdentity"));
        assertEquals(testEmployee.getAttributeValue("fullName"), entry.getAttributeValue("fullName"));
        assertEquals(testEmployee.getAttributeValue("middleName"), entry.getAttributeValue("middleName"));
        assertEquals(testEmployee.getAttributeValue("sn"), entry.getAttributeValue("sn"));
        assertEquals(testEmployee.getAttributeValue("givenName"), entry.getAttributeValue("givenName"));
        
        //Move
        LDAPResult modifyDnResult = ldapProxyClient.modifyDn(new ModifyDNRequest("cn=Test Ldap Person,"+dn, "cn=Test Ldap Person1", true, newDn));
        assertEquals(ResultCode.SUCCESS, modifyDnResult.getResultCode());
        
        //Verify new location
        Entry entryAfterMove = ldapProxyClient.getEntry("cn=Test Ldap Person1,"+newDn);
        assertNotNull(entryAfterMove);
        assertEquals(testEmployee.getAttributeValue("hsaIdentity"), entryAfterMove.getAttributeValue("hsaIdentity"));
        assertEquals(testEmployee.getAttributeValue("fullName"), entryAfterMove.getAttributeValue("fullName"));
        assertEquals(testEmployee.getAttributeValue("middleName"), entryAfterMove.getAttributeValue("middleName"));
        assertEquals(testEmployee.getAttributeValue("sn"), entryAfterMove.getAttributeValue("sn"));
        assertEquals(testEmployee.getAttributeValue("givenName"), entryAfterMove.getAttributeValue("givenName"));
        
     // Delete employee
        ldapProxyClient.deleteRecursive(entryAfterMove.getDN());
        Entry deletedEntry = ldapProxyClient.getEntry(entryAfterMove.getDN());
        assertNull(deletedEntry);
    }

    Entry getTestEmployee(String parentDn) {
        Entry entry = new Entry("cn=Test Ldap Person,"+parentDn);
        entry.addAttribute("objectClass", "HSAPersonExtension","InetOrgPerson","organizationalPerson","person","pkiUser","top");
        entry.addAttribute("sn", "Person");
        entry.addAttribute("cn", "Test Ldap Person");
        entry.addAttribute("givenName", "Test");
        entry.addAttribute("middleName", "Ldap");
        entry.addAttribute("fullName", "Ldap Person, Test");
        entry.addAttribute("hsaIdentity", "TEST_LDAP_EMP_000"+counter++);
        return entry;
    }
}
