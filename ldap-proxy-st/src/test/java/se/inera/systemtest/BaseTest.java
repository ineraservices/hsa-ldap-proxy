package se.inera.systemtest;

import com.unboundid.ldap.sdk.LDAPResult;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import se.inera.ldapclient.SimpleLdapClient;

import java.util.*;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BaseTest {

    final static String LDIF_BASE_DATA = "testdata/create_organization_units.ldif";
    private static Map<String, SimpleLdapClient> ldapClients = new HashMap<>(2);
    private static final Logger LOG = Logger.getLogger(BaseTest.class.getName());

    @BeforeAll
    public static void before() throws Exception {
        Properties props = new Properties();
        props.load(BaseTest.class.getResourceAsStream("test.properties"));
        String baseUri = System.getProperty("baseUri", props.getProperty("ldap-proxy.host"));
        int port = Integer.parseInt(System.getProperty("port", props.getProperty("ldap-proxy.port")));
        LOG.info("Running tests on " + baseUri + ":" + port);
        SimpleLdapClient ldapProxyClient = new SimpleLdapClient(
            "ldap-proxy-client",
            baseUri,
            port,
            props.getProperty("ldap-proxy.username"),
            props.getProperty("ldap-proxy.password"));

        ldapClients.put("ldap-proxy-client", ldapProxyClient);
    }

    @AfterAll
    static void afterAll() {
        closeLdapConnections();
    }

    Map<String, SimpleLdapClient> getLdapClients() {
        return ldapClients;
    }


    void ldapLoadLdifFiles(String ldifBaseData) throws Exception {
        List<List<LDAPResult>> resultsList = new ArrayList<>();
        for (Map.Entry<String, SimpleLdapClient> simpleLdapClient : ldapClients.entrySet()) {
            List<LDAPResult> results = simpleLdapClient.getValue().loadLdifFiles(ldifBaseData);
            resultsList.add(results);
        }

        for (int i = 0; i < resultsList.size(); i++) {
            for (int j = i + 1; j < resultsList.size(); j++) {
                assertLDAPResultEquals(resultsList.get(i), resultsList.get(j));
            }
        }

    }

    void ldapLoadLdifBaseData() throws Exception {
        ldapLoadLdifFiles(LDIF_BASE_DATA);
    }

    private void assertLDAPResultEquals(List<LDAPResult> ldapResults, List<LDAPResult> ldapResults1) {
        if (ldapResults.size() != ldapResults1.size()) {
            throw new AssertionFailedError("Expected same size: " + ldapResults + " and " + ldapResults1);
        }

        for (int i = 0; i < ldapResults.size(); i++) {
            assertEquals(ldapResults.get(i).getDiagnosticMessage(), ldapResults1.get(i).getDiagnosticMessage());
            assertEquals(ldapResults.get(i).getMatchedDN(), ldapResults1.get(i).getMatchedDN());
            assertEquals(ldapResults.get(i).getReferralURLs(), ldapResults1.get(i).getReferralURLs());
            assertEquals(ldapResults.get(i).getResultCode(), ldapResults1.get(i).getResultCode());
        }
    }

    private static void closeLdapConnections() {
        System.out.println("closing connections");
        for (SimpleLdapClient simpleLdapClient : ldapClients.values()) {
            simpleLdapClient.close();
        }
    }
}
