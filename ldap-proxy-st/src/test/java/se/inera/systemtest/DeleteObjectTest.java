package se.inera.systemtest;

import com.unboundid.ldap.sdk.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import se.inera.ldapclient.LdifFileReader;
import se.inera.ldapclient.SimpleLdapClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DeleteObjectTest extends BaseTest {

    @Disabled
    @Test
    void deleteOrganization() throws Exception {
        ldapLoadLdifBaseData();
        ldapLoadLdifFiles("testdata/delete_org_structure.ldif");

        // assert using files
        List<Entry> originalEntries = LdifFileReader.getLdifEntries(LDIF_BASE_DATA);
        List<Entry> deletedEntries = LdifFileReader.getLdifEntries("testdata/delete_org_structure.ldif");

        SearchRequest request = new SearchRequest("c=SE", SearchScope.SUB, "(|(o=*)(ou=*))");
        for (SimpleLdapClient simpleLdapClient : getLdapClients().values()) {
            SearchResult result = simpleLdapClient.search(request);
            // assert correct number of entires in search
            assertEquals(originalEntries.size() - deletedEntries.size(), result.getEntryCount());

            // assert search result doesn't contain deleted entry
            for (Entry searchResultEntry : result.getSearchEntries()) {
                for (Entry deletedEntry : deletedEntries) {
                    assertNotEquals(deletedEntry.getDN(), searchResultEntry.getDN());
                }
            }
        }
    }

    @Disabled
    @Test
    void failDeleteCounty() throws Exception {
        ldapLoadLdifBaseData();
        assertThrows(LDAPException.class, () -> ldapLoadLdifFiles("testdata/failing_delete_org_structure.ldif"));
    }
}
