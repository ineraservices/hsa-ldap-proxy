System tests for ldap proxy


- You have to have an inmemoryldapserver and an ldap-proxy running to run the tests

### Execute cucumber tests

Run cucumber tests including starting the target system
```sh
$ gradlew cucumber -PDOCKER_COMPOSE
```

Run cucumber tests without starting the target system
```sh
$ gradlew cucumber
```

Run cucumber tests defined by a specific @tag
```sh
$ gradlew cucumber -Ptag=@MyTest
```

Run cucumber tests defined by combinations of @tag's
```sh
$ gradlew cucumber -Ptag="@MyTest or @TagTest"
```
### end of cucumber tests

